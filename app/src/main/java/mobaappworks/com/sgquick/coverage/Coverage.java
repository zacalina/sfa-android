package mobaappworks.com.sgquick.coverage;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TypefaceSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.mobaappworks.R;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import mobaappworks.com.sgquick.Constant;
import mobaappworks.com.sgquick.CustomDrawerAdapter;
import mobaappworks.com.sgquick.Util;
import mobaappworks.com.sgquick.entity.AccountEntity;
import mobaappworks.com.sgquick.entity.CoveragePlanEntity;

public class Coverage extends AppCompatActivity {

    private CoveragePagerAdapter mCoveragePagerAdapter;
    private ViewPager mViewPager;
    private PagerTabStrip pagerTabStrip;
    private ActionBarDrawerToggle mDrawerToggle;
    private String USER;
    private String EMAIL;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private DrawerLayout Drawer;
    private String mTitle = "";
    private Realm realm;
    private TelephonyManager telephonyManager;
    private MyPhoneStateListener pslistener;
    private Integer signalStrengthDisplay;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Hide the title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homescreen);

        context = this;

        CustomDrawerAdapter.context = getApplicationContext();

        USER = Util.getUsername();
        EMAIL = Util.getEmail();

        // actionbar styling
        SpannableString s = new SpannableString("Coverage");
        s.setSpan(new TypefaceSpan(Constant.FONT_FSELLIOT_REG), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        mTitle = "COVERAGE";
        getSupportActionBar().setTitle(s);

        mRecyclerView = (RecyclerView) findViewById(R.id.RecyclerView);
        mRecyclerView.setHasFixedSize(true);

        mAdapter = new CustomDrawerAdapter(Constant.TITLES, Constant.ICONS,USER,EMAIL);
        mRecyclerView.setAdapter(mAdapter);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        //
            Bundle extras = getIntent().getExtras();

            String value = extras.getString("syncStatus");
            //Toast.makeText(context, value, Toast.LENGTH_LONG).show();

        if (extras != null) {
            if(value != null) {
                if (value.equals("loadComplete")) {
                    msgLoadComplete();
                } else if (value.equals("syncComplete")) {
                    msgSyncComplete();
                } else if (value.equals("drawerIntent")) {
                    Log.d("Kamote Log  ", "Intent from Drawer");
                }
                else {
                    msgSyncError();
                }
            }
            else{
                //Toast.makeText(context, "Always Appears", Toast.LENGTH_LONG).show();
                Log.d("Kamote Log  ", "DATA LOADED");
            }
        }
        else
        {
            Log.d("Kamote Log : ", "An error has occured");
        }



        //value

        Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout);

        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                Drawer,
                R.string.drawer_open,
                R.string.drawer_close) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
                getWindow().setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
                );
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
                getWindow().setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
                );
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                getWindow().setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
                );
            }
        };

        Drawer.setDrawerListener(mDrawerToggle);

        mDrawerToggle.setDrawerIndicatorEnabled(true);

        mCoveragePagerAdapter = new CoveragePagerAdapter(getSupportFragmentManager(), this);

        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mCoveragePagerAdapter);

        pagerTabStrip = (PagerTabStrip) findViewById(R.id.pager_title_strip);
        pagerTabStrip.setDrawFullUnderline(true);
        pagerTabStrip.setTabIndicatorColor(Color.rgb(232, 99, 160));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        realm = Realm.getDefaultInstance();

//        List<String> availableTelco = Util.getAvailableTelco(context);

        try {
            pslistener = new MyPhoneStateListener();
            telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            telephonyManager.listen(pslistener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class MyPhoneStateListener extends PhoneStateListener {
        int signal;
        @Override
        public void onSignalStrengthsChanged(SignalStrength signalStrength) {
            super.onSignalStrengthsChanged(signalStrength);
            signal = signalStrength.getGsmSignalStrength();
            Util.setDisplaySignal(signal);
            // (2 * gsmSignalStrength) - 113 -> dBm
        }
    }

    public void msgSyncComplete(){

        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        alertDialog.setMessage("SYNC COMPLETE");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        //alertDialog.setTitle("INFO");
        alertDialog.show();
    }


    public void msgLoadComplete(){

        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        alertDialog.setMessage("LOAD COMPLETE");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        //alertDialog.setTitle("INFO");
        alertDialog.show();
    }

    public void msgSyncError(){

        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        alertDialog.setMessage("An ERROR has occured during SYNC, please RE-SYNC");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.setTitle("ERROR");
        alertDialog.show();
    }





}