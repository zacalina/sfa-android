package mobaappworks.com.sgquick.coverage;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mobaappworks.R;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import mobaappworks.com.sgquick.Constant;
import mobaappworks.com.sgquick.FontChangeCrawler;
import mobaappworks.com.sgquick.Util;
import mobaappworks.com.sgquick.accounts.AccountsAddPipeline;
import mobaappworks.com.sgquick.entity.AccountEntity;
import mobaappworks.com.sgquick.entity.CoveragePlanEntity;

public class CoverageFragmentMap extends Fragment implements LocationListener {

    private GoogleMap mMap;
    private View rootView;
    private Context context;
    private Double latitude;
    private Double longitude;
    private Location location;
    private LocationManager locationManager;
    private Boolean isGPSEnabled;
    private Boolean isNetworkEnabled;
    private Integer type;
    private Realm realm;
    private RealmResults<AccountEntity> accounts;
    private Calendar selectedCalendar;
    private Map<String, Integer> ids;


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        FontChangeCrawler fontChanger = new FontChangeCrawler(this.context.getAssets(), Constant.FONT_FSELLIOT_REG);
//        fontChanger.replaceFonts((ViewGroup) this.getView());


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(rootView == null)
            rootView = inflater.inflate(R.layout.activity_coverage_map, container, false);

        Bundle args = getArguments();
        type = args.getInt(Constant.ACCOUNT_LIST_TYPE);

        context = rootView.getContext();
        accounts = getAccounts(type);
        return rootView;
    }

    public void refreshMap(Calendar selectedCalendar){
        this.selectedCalendar = selectedCalendar;
        mMap.clear();
        accounts = getAccounts(type,
                selectedCalendar.get(Calendar.MONTH),
                selectedCalendar.get(Calendar.YEAR),
                selectedCalendar.get(Calendar.DAY_OF_MONTH));
        getLocation();
        setUpMap();
    }


    @Override
    public void onResume() {
        super.onResume();
        setUpMapIfNeeded();
        if(selectedCalendar == null){
            selectedCalendar = Calendar.getInstance();
        }

        refreshMap(selectedCalendar);
    }

    public RealmResults<AccountEntity> getAccounts(int type){
        return getAccounts(type ,-1,-1,-1);
    }


    public RealmResults<AccountEntity> getAccounts(int type, int month, int year, int day){
        realm = Realm.getDefaultInstance();
        Calendar calendar = Calendar.getInstance();
        Calendar fromCal;
        Calendar toCal;

        switch (type){
            case 0:
                //DAILY
                fromCal = Util.getCalendarWithValues(month,year,day,0,0,0,0);
                toCal = Util.getCalendarWithValues(month,year,day,23,59,59,99);
                break;
            case 1:
                //MONTHLY
                fromCal = Util.getCalendarWithValues(month,year, calendar.getMinimum(Calendar.DAY_OF_MONTH), 0,0,0,0);
                toCal = Util.getCalendarWithValues(month,year, calendar.getMaximum(Calendar.DAY_OF_MONTH), 23,59,59,99);
                break;
            default:
                fromCal = Util.getCalendarWithValues(-1,-1,-1,0,0,0,0);
                toCal = Util.getCalendarWithValues(-1,-1,-1,0,0,0,0);
                break;
        }

        Date fromDate = fromCal.getTime();
        Date toDate = toCal.getTime();

        boolean or = false;

        RealmQuery<AccountEntity> query = realm.where(AccountEntity.class);
        RealmResults<CoveragePlanEntity> coverageMonth = Util.getCoverageMonth(fromDate.getTime(), toDate.getTime());

        if (coverageMonth.size() < 1)
            return null;

        for (CoveragePlanEntity cpe : coverageMonth) {
            if (or) {
                query.or().equalTo("accountId", Integer.valueOf(cpe.getAccountIdFk()));
            } else {
                query.equalTo("accountId", Integer.valueOf(cpe.getAccountIdFk()));
                or = true;
            }
        }

        return query.findAll();
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            SupportMapFragment sfm = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.coverage_map);
            mMap = sfm.getMap();
//            // Check if we were successful in obtaining the map.
//            if (mMap != null) {
//                setUpMap();
//            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {

        ids = new HashMap<>();
        if (accounts != null)
            for (AccountEntity account : accounts) {

                ids.put(account.getAccountName(), account.getAccountId());

                if(!"".equals(account.getLatitude()) && !"".equals(account.getLongitude())) {
                    LatLng latlng = new LatLng(Double.valueOf(account.getLatitude()), Double.valueOf(account.getLongitude()));
                    mMap.addMarker(new MarkerOptions()
                            .position(latlng)
                            .title(account.getAccountName())
                            .snippet(account.getAccountAddress()));
                }
            }

        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
//                .title("ME")


        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(14.617476, 121.053131), 11.0f));

        mMap.setOnInfoWindowClickListener(
                new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {
                        Intent nextScreen = new Intent(context, AccountsAddPipeline.class);
                        nextScreen.putExtra(Constant.ID, ids.get(marker.getTitle()));
//                        nextScreen.putExtra("eventId", "" + eventId);
                        startActivity(nextScreen);
                    }
                }
        );
    }

    private GoogleMap.OnMyLocationChangeListener myLocationChangeListener = new GoogleMap.OnMyLocationChangeListener() {
        @Override
        public void onMyLocationChange(Location location) {
            LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());
            mMap.addMarker(new MarkerOptions().position(loc));
        }
    };

    public Location getLocation() {
        try {
            if (latitude == null) {
                latitude = 0D;
            }

            if (longitude == null) {
                longitude = 0D;
            }

            locationManager = (LocationManager) context
                    .getSystemService(Context.LOCATION_SERVICE);

            // getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
            } else {
//                this.canGetLocation = true;
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            5000,
                            10,
                            this);
                    Log.d("Network", "Network Enabled");
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                5000,
                                10,
                                this);
                        Log.d("GPS", "GPS Enabled");
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return location;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        locationManager.removeUpdates(this);
    }

    @Override
    public void onLocationChanged(Location location) {
        String lng = "Longitude: " + location.getLongitude();
        longitude = location.getLongitude();
        Log.v("MAP VIEW", lng);
        String lat = "Latitude: " + location.getLatitude();
        latitude = location.getLatitude();
        Log.v("MAP VIEW", lat);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
