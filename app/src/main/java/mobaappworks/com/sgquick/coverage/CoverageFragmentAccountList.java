package mobaappworks.com.sgquick.coverage;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.mobaappworks.R;

import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import mobaappworks.com.sgquick.Constant;
import mobaappworks.com.sgquick.FontChangeCrawler;
import mobaappworks.com.sgquick.Util;
import mobaappworks.com.sgquick.accounts.Accounts;
import mobaappworks.com.sgquick.accounts.AccountsList;
import mobaappworks.com.sgquick.accounts.AccountsListAdapter;
import mobaappworks.com.sgquick.entity.AccountEntity;
import mobaappworks.com.sgquick.entity.CoveragePlanEntity;

public class CoverageFragmentAccountList extends Fragment implements View.OnClickListener {
    public ListView listView;
    public EditText etDatePicker;
    public ProgressBar progressBar;
    private Context context;
    private int type;
    private Realm realm;
    private RealmResults<AccountEntity> accountEntities;
    SearchView sv;
    EditText inputSearch;

    private int mYear;
    private int mDay;
    private int mMonth;
    private ViewPager vp;
    private CoveragePagerAdapter adapter;
    private SparseArray<Fragment> registeredFragments;

    private Calendar selectedCalendar;


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        FontChangeCrawler fontChanger = new FontChangeCrawler(this.context.getAssets(), Constant.FONT_FSELLIOT_REG);
        fontChanger.replaceFonts((ViewGroup) this.getView());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.activity_coverage_account_list, container, false);
        Bundle args = getArguments();
        type = args.getInt(Constant.ACCOUNT_LIST_TYPE);
        listView = (ListView) rootView.findViewById(R.id.listview);
        listView.setOnItemClickListener(onClickListener());
        etDatePicker = (EditText) rootView.findViewById(R.id.pick_date);
        etDatePicker.setOnClickListener(this);
        context = rootView.getContext();
        realm = Realm.getDefaultInstance();
        vp = (ViewPager) getActivity().findViewById(R.id.pager);
        adapter = (CoveragePagerAdapter) vp.getAdapter();
        registeredFragments = adapter.getRegisteredFragments();
        // get the current date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        accountEntities = getAccounts(type);

        listView.setAdapter(new AccountsListAdapter(context, accountEntities, false));
        if (accountEntities == null) {
            progressBar = (ProgressBar) rootView.findViewById(R.id.coverageProgressBar);
            progressBar.setVisibility(View.GONE);

            inputSearch = (EditText) rootView.findViewById(R.id.inputSearch);
            inputSearch.setVisibility(View.VISIBLE);
        } else if (listView.getCount() == 0) {
            progressBar = (ProgressBar) rootView.findViewById(R.id.coverageProgressBar);
            progressBar.setVisibility(View.VISIBLE);

            inputSearch = (EditText) rootView.findViewById(R.id.inputSearch);
            inputSearch.setVisibility(View.GONE);
        } else if (listView.getCount() > 0) {
            progressBar = (ProgressBar) rootView.findViewById(R.id.coverageProgressBar);
            progressBar.setVisibility(View.GONE);

            inputSearch = (EditText) rootView.findViewById(R.id.inputSearch);
            inputSearch.setVisibility(View.VISIBLE);
        } else {
            progressBar = (ProgressBar) rootView.findViewById(R.id.coverageProgressBar);
            progressBar.setVisibility(View.VISIBLE);

            inputSearch = (EditText) rootView.findViewById(R.id.inputSearch);
            inputSearch.setVisibility(View.GONE);
        }

        inputSearch = (EditText) rootView.findViewById(R.id.inputSearch);
        inputSearch.setOnClickListener(myOnClickListener);

        return rootView;


    }

    public RealmResults<AccountEntity> getAccounts(int type) {
        return getAccounts(type, -1, -1, -1);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (selectedCalendar != null) {
            accountEntities = getAccounts(
                    type,
                    selectedCalendar.get(Calendar.MONTH),
                    selectedCalendar.get(Calendar.YEAR),
                    selectedCalendar.get(Calendar.DAY_OF_MONTH));
            listView.setAdapter(new AccountsListAdapter(context, accountEntities, false));

            mYear = selectedCalendar.get(Calendar.YEAR);
            mMonth = selectedCalendar.get(Calendar.MONTH);
            mDay = selectedCalendar.get(Calendar.DAY_OF_MONTH);
        }
    }

    public RealmResults<AccountEntity> getAccounts(int type, int month, int year, int day) {
        realm = Realm.getDefaultInstance();
        Calendar calendar = Calendar.getInstance();

        Calendar fromCal;
        Calendar toCal;

        switch (type) {
            case 0:
                //DAILY
                fromCal = Util.getCalendarWithValues(month, year, day, 0, 0, 0, 0);
                toCal = Util.getCalendarWithValues(month, year, day, 23, 59, 59, 99);
                etDatePicker.setText(Constant.SDF_DATE.format(fromCal.getTime()));
                break;
            case 1:
                //MONTHLY
                fromCal = Util.getCalendarWithValues(month, year, calendar.getMinimum(Calendar.DAY_OF_MONTH), 0, 0, 0, 0);
                toCal = Util.getCalendarWithValues(month, year, calendar.getMaximum(Calendar.DAY_OF_MONTH), 23, 59, 59, 99);
                etDatePicker.setText(Constant.SDF_MONTH.format(fromCal.getTime()));
                break;
            default:
                fromCal = Util.getCalendarWithValues(-1, -1, -1, 0, 0, 0, 0);
                toCal = Util.getCalendarWithValues(-1, -1, -1, 0, 0, 0, 0);
                break;
        }

        Date fromDate = fromCal.getTime();
        Date toDate = toCal.getTime();

        boolean or = false;

        RealmQuery<AccountEntity> query = realm.where(AccountEntity.class);
        RealmResults<CoveragePlanEntity> coverageMonth = Util.getCoverageMonth(fromDate.getTime(), toDate.getTime());

        if (coverageMonth.size() < 1)
            return null;

        for (CoveragePlanEntity cpe : coverageMonth) {
            if (or) {
                query.or().equalTo("accountId", Integer.valueOf(cpe.getAccountIdFk()));
            } else {
                query.equalTo("accountId", Integer.valueOf(cpe.getAccountIdFk()));
                or = true;
            }
        }

        return query.findAll();
    }


    public AdapterView.OnItemClickListener onClickListener() {
        return new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Integer tag = (Integer) view.getTag();
                TextView tv = (TextView) view.findViewById(R.id.firstLine);
                String name = tv.getText().toString();

                Intent intent = new Intent(context, Accounts.class);
                intent.putExtra(Constant.ID, tag);
                intent.putExtra(Constant.ACCOUNT_NAME, name);

                context.startActivity(intent);
            }

        };
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.pick_date:
                switch (type) {
                    case 0:
                        //DAILY
                        showDialog(0).show();
                        break;
                    case 1:
                        //MONTHLY
                        showDialog(1).show();
                        break;
                }

                break;
        }

    }

    private DatePickerDialog.OnDateSetListener mDateSetListener =
            new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker view, int year,
                                      int monthOfYear, int dayOfMonth) {
                    mYear = year;
                    mMonth = monthOfYear;
                    mDay = dayOfMonth;

                    Calendar calendar = Calendar.getInstance();
                    calendar.set(Calendar.YEAR, year);
                    calendar.set(Calendar.MONTH, mMonth);
                    calendar.set(Calendar.DAY_OF_MONTH, mDay);
                    CoverageFragmentMap coverageFragmentMap;
                    selectedCalendar = calendar;

                    switch (type) {
                        case 0:
                            //DAILY
                            coverageFragmentMap = (CoverageFragmentMap) registeredFragments.get(1);
                            etDatePicker.setText(Constant.SDF_DATE.format(calendar.getTime()));
                            coverageFragmentMap.refreshMap(selectedCalendar);
                            break;
                        case 1:
                            //MONTHLY
                            coverageFragmentMap = (CoverageFragmentMap) registeredFragments.get(3);
                            etDatePicker.setText(Constant.SDF_MONTH.format(calendar.getTime()));
                            coverageFragmentMap.refreshMap(selectedCalendar);
                            break;
                    }


                    accountEntities = getAccounts(type, mMonth, mYear, mDay);
                    listView.setAdapter(new AccountsListAdapter(context, accountEntities, false));
                }
            };


    protected Dialog showDialog(int id) {
        switch (id) {
            case 0:
                return new DatePickerDialog(context, 3,
                        mDateSetListener,
                        mYear, mMonth, mDay);
            case 1:
                return createDialogWithoutDateField();

        }
        return null;
    }

    private DatePickerDialog createDialogWithoutDateField() {
        DatePickerDialog dpd = new DatePickerDialog(context, 3, mDateSetListener, mYear, mMonth, mDay);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            DatePicker datePicker = dpd.getDatePicker();
            int daySpinnerId = Resources.getSystem().getIdentifier("day", "id", "android");
            if (daySpinnerId != 0) {
                View daySpinner = datePicker.findViewById(daySpinnerId);
                if (daySpinner != null) {
                    daySpinner.setVisibility(View.GONE);
                }
            }
        }
        return dpd;
    }

    final View.OnClickListener myOnClickListener = new View.OnClickListener() {
        public void onClick(final View v) {
            //Toast.makeText(context, "Button1 clicked.", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getActivity(), AccountsList.class);
            startActivity(intent);

        }
    };
}