package mobaappworks.com.sgquick.coverage;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import java.util.Calendar;

import mobaappworks.com.sgquick.Constant;

public class CoveragePagerAdapter extends FragmentPagerAdapter {
    Context mContext;
    SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();
    public CoveragePagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragmentDailyMap    = new CoverageFragmentMap();
        Fragment fragmentDailyList   = new CoverageFragmentAccountList();
        Fragment fragmentMonthlyMap  = new CoverageFragmentMap();
        Fragment fragmentMonthlyList = new CoverageFragmentAccountList();
        Bundle args = new Bundle();

        switch (position){
//            fragment = new CoverageFragmentGraph();
            case 0:
                args.putInt(Constant.ACCOUNT_LIST_TYPE,0);
                fragmentDailyList.setArguments(args);
                return fragmentDailyList;
            case 1:
                args.putInt(Constant.ACCOUNT_LIST_TYPE,0);
                fragmentDailyMap.setArguments(args);
                return fragmentDailyMap;
            case 2:
                args.putInt(Constant.ACCOUNT_LIST_TYPE,1);
                fragmentMonthlyList.setArguments(args);
                return fragmentMonthlyList;
            case 3:
                args.putInt(Constant.ACCOUNT_LIST_TYPE,1);
                fragmentMonthlyMap.setArguments(args);
                return fragmentMonthlyMap;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment item = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, item);
        return item;
    }


    public SparseArray<Fragment> getRegisteredFragments() {
        return registeredFragments;
    }

    public void setRegisteredFragments(SparseArray<Fragment> registeredFragments) {
        this.registeredFragments = registeredFragments;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        switch (position){
//            return "Coverage";
            case 0:
                return "Daily Coverage "; //(" + Constant.SDF_DATE.format(today.getTime()) + ")";
            case 1:
                return "Daily Map";
            case 2:
                return "Monthly Coverage"; // (" + Constant.SDF_MONTH.format(today.getTime()) + ")";
            case 3:
                return "Monthly Map";
            default:
                return "Error";
        }
    }
}