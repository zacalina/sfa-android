package mobaappworks.com.sgquick.entity;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ldeguzman on 9/11/15.
 */
public class CheckInOutEntity extends RealmObject {

    @PrimaryKey
    private int id;
    private String accountId;
    private String checkinLng;
    private String checkinLat;
    private String checkoutLng;
    private String checkoutLat;
    private Date checkinDate;
    private Date checkoutDate;
    private String checkinNote;
    private String checkoutNote;

    public CheckInOutEntity() {
    }

    public CheckInOutEntity(int id, String accountId, String checkinLng, String checkinLat, String checkoutLng,
                            String checkoutLat, Date checkinDate, Date checkoutDate, String checkinNote, String checkoutNote) {
        this.id = id;
        this.accountId = accountId;
        this.checkinLng = checkinLng;
        this.checkinLat = checkinLat;
        this.checkoutLng = checkoutLng;
        this.checkoutLat = checkoutLat;
        this.checkinDate = checkinDate;
        this.checkoutDate = checkoutDate;
        this.checkinNote = checkinNote;
        this.checkoutNote = checkoutNote;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getCheckinLng() {
        return checkinLng;
    }

    public void setCheckinLng(String checkinLng) {
        this.checkinLng = checkinLng;
    }

    public String getCheckinLat() {
        return checkinLat;
    }

    public void setCheckinLat(String checkinLat) {
        this.checkinLat = checkinLat;
    }

    public String getCheckoutLng() {
        return checkoutLng;
    }

    public void setCheckoutLng(String checkoutLng) {
        this.checkoutLng = checkoutLng;
    }

    public String getCheckoutLat() {
        return checkoutLat;
    }

    public void setCheckoutLat(String checkoutLat) {
        this.checkoutLat = checkoutLat;
    }

    public Date getCheckinDate() {
        return checkinDate;
    }

    public void setCheckinDate(Date checkinDate) {
        this.checkinDate = checkinDate;
    }

    public Date getCheckoutDate() {
        return checkoutDate;
    }

    public void setCheckoutDate(Date checkoutDate) {
        this.checkoutDate = checkoutDate;
    }

    public String getCheckinNote() {
        return checkinNote;
    }

    public void setCheckinNote(String checkinNote) {
        this.checkinNote = checkinNote;
    }

    public String getCheckoutNote() {
        return checkoutNote;
    }

    public void setCheckoutNote(String checkoutNote) {
        this.checkoutNote = checkoutNote;
    }
}
