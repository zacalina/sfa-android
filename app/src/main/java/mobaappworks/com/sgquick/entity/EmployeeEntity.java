package mobaappworks.com.sgquick.entity;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ldeguzman on 9/1/15.
 */
public class EmployeeEntity extends RealmObject {

    @PrimaryKey
    private int id;
    private String employeeId;
    private String firstName;
    private String middleName;
    private String lastName;
    private String birthdate;
    private String department;
    private String email;
    private String homeAddress;
    private String mobileNumber;
    private String role;
    private String regionIdFk;
    private String salesmanWirelessId;
    private String salesmanWirelineId;
    private String telNumber;
    private String workGroup;
    private String status;
    private String dateHired;
    private String effectStartDate;
    private String effectveEndDate;
    private String segmentIdFk;
    private String cityTownIdFk;
    private String province;
    private String salesRegionIdFk;
    private String lastUpdateDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getSalesmanWirelessId() {
        return salesmanWirelessId;
    }

    public void setSalesmanWirelessId(String salesmanWirelessId) {
        this.salesmanWirelessId = salesmanWirelessId;
    }

    public String getSalesmanWirelineId() {
        return salesmanWirelineId;
    }

    public void setSalesmanWirelineId(String salesmanWirelineId) {
        this.salesmanWirelineId = salesmanWirelineId;
    }

    public String getTelNumber() {
        return telNumber;
    }

    public void setTelNumber(String telNumber) {
        this.telNumber = telNumber;
    }

    public String getWorkGroup() {
        return workGroup;
    }

    public void setWorkGroup(String workGroup) {
        this.workGroup = workGroup;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDateHired() {
        return dateHired;
    }

    public void setDateHired(String dateHired) {
        this.dateHired = dateHired;
    }

    public String getEffectStartDate() {
        return effectStartDate;
    }

    public void setEffectStartDate(String effectStartDate) {
        this.effectStartDate = effectStartDate;
    }

    public String getEffectveEndDate() {
        return effectveEndDate;
    }

    public void setEffectveEndDate(String effectveEndDate) {
        this.effectveEndDate = effectveEndDate;
    }

    public String getSegmentIdFk() {
        return segmentIdFk;
    }

    public void setSegmentIdFk(String segmentIdFk) {
        this.segmentIdFk = segmentIdFk;
    }

    public String getRegionIdFk() {
        return regionIdFk;
    }

    public void setRegionIdFk(String regionIdFk) {
        this.regionIdFk = regionIdFk;
    }

    public String getCityTownIdFk() {
        return cityTownIdFk;
    }

    public void setCityTownIdFk(String cityTownIdFk) {
        this.cityTownIdFk = cityTownIdFk;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getSalesRegionIdFk() {
        return salesRegionIdFk;
    }

    public void setSalesRegionIdFk(String salesRegionIdFk) {
        this.salesRegionIdFk = salesRegionIdFk;
    }

    public String getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(String lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }
}
