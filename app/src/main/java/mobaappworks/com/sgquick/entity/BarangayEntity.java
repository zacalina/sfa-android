package mobaappworks.com.sgquick.entity;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Migs on 9/16/2015.
 */
public class BarangayEntity extends RealmObject {

    @PrimaryKey
    private int id;
    private String name;
    private String description;
    private int cityTownIdFk;
    private String postalCode;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCityTownIdFk() {
        return cityTownIdFk;
    }

    public void setCityTownIdFk(int cityTownIdFk) {
        this.cityTownIdFk = cityTownIdFk;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }
}
