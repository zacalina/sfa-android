package mobaappworks.com.sgquick.entity;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ldeguzman on 9/15/15.
 */
public class CoveragePlanEntity extends RealmObject{

    @PrimaryKey
    private int id;
    private String accountIdFk;
    private long scheduledDateTime;
    private String status;
    private String employeeId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAccountIdFk() {
        return accountIdFk;
    }

    public void setAccountIdFk(String accountIdFk) {
        this.accountIdFk = accountIdFk;
    }

    public long getScheduledDateTime() {
        return scheduledDateTime;
    }

    public void setScheduledDateTime(long scheduledDateTime) {
        this.scheduledDateTime = scheduledDateTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }
}
