package mobaappworks.com.sgquick.entity;

import android.accounts.Account;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ldeguzman on 9/2/15.
 */
public class CrossSellUpSellEntity extends RealmObject {

    @PrimaryKey
    private int id;
    private String recommendation;
    private AccountEntity accountId;
    private ProductTypeEntity productTypeId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRecommendation() {
        return recommendation;
    }

    public void setRecommendation(String recommendation) {
        this.recommendation = recommendation;
    }

    public AccountEntity getAccountId() {
        return accountId;
    }

    public void setAccountId(AccountEntity accountId) {
        this.accountId = accountId;
    }

    public ProductTypeEntity getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(ProductTypeEntity productTypeId) {
        this.productTypeId = productTypeId;
    }
}
