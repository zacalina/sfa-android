package mobaappworks.com.sgquick.entity;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ldeguzman on 9/1/15.
 */
public class AccountDetailsEntity extends RealmObject {


    @PrimaryKey
    private int id;
    private int avgMonthlyAcquiBilled;
    private int avgMonthlyAcquiLines;
    private int avgMonthlyAcquiMSF;
    private int dataPostpaidMSF;
    private int dataPostpaidLines;
    private int dataPostpaidBilled;
    private int fixedBbPostpaidMSF;
    private int fixedBbPostpaidLines;
    private int fixedBbPostpaidBilled;
    private int mtPostpaidMSF;
    private int mtPostpaidLines;
    private int mtPostpaidBilled;
    private int nomadicPostpaidMSF;
    private int nomadicPostpaidLines;
    private int nomadicPostpaidBilled;
    private AccountEntity accountId;
    private int solutionsMSF;
    private int solutionsBilled;
    private int solutionsLines;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAvgMonthlyAcquiBilled() {
        return avgMonthlyAcquiBilled;
    }

    public void setAvgMonthlyAcquiBilled(int avgMonthlyAcquiBilled) {
        this.avgMonthlyAcquiBilled = avgMonthlyAcquiBilled;
    }

    public int getAvgMonthlyAcquiLines() {
        return avgMonthlyAcquiLines;
    }

    public void setAvgMonthlyAcquiLines(int avgMonthlyAcquiLines) {
        this.avgMonthlyAcquiLines = avgMonthlyAcquiLines;
    }

    public int getAvgMonthlyAcquiMSF() {
        return avgMonthlyAcquiMSF;
    }

    public void setAvgMonthlyAcquiMSF(int avgMonthlyAcquiMSF) {
        this.avgMonthlyAcquiMSF = avgMonthlyAcquiMSF;
    }

    public int getDataPostpaidMSF() {
        return dataPostpaidMSF;
    }

    public void setDataPostpaidMSF(int dataPostpaidMSF) {
        this.dataPostpaidMSF = dataPostpaidMSF;
    }

    public int getDataPostpaidLines() {
        return dataPostpaidLines;
    }

    public void setDataPostpaidLines(int dataPostpaidLines) {
        this.dataPostpaidLines = dataPostpaidLines;
    }

    public int getDataPostpaidBilled() {
        return dataPostpaidBilled;
    }

    public void setDataPostpaidBilled(int dataPostpaidBilled) {
        this.dataPostpaidBilled = dataPostpaidBilled;
    }

    public int getFixedBbPostpaidMSF() {
        return fixedBbPostpaidMSF;
    }

    public void setFixedBbPostpaidMSF(int fixedBbPostpaidMSF) {
        this.fixedBbPostpaidMSF = fixedBbPostpaidMSF;
    }

    public int getFixedBbPostpaidLines() {
        return fixedBbPostpaidLines;
    }

    public void setFixedBbPostpaidLines(int fixedBbPostpaidLines) {
        this.fixedBbPostpaidLines = fixedBbPostpaidLines;
    }

    public int getFixedBbPostpaidBilled() {
        return fixedBbPostpaidBilled;
    }

    public void setFixedBbPostpaidBilled(int fixedBbPostpaidBilled) {
        this.fixedBbPostpaidBilled = fixedBbPostpaidBilled;
    }

    public int getMtPostpaidMSF() {
        return mtPostpaidMSF;
    }

    public void setMtPostpaidMSF(int mtPostpaidMSF) {
        this.mtPostpaidMSF = mtPostpaidMSF;
    }

    public int getMtPostpaidLines() {
        return mtPostpaidLines;
    }

    public void setMtPostpaidLines(int mtPostpaidLines) {
        this.mtPostpaidLines = mtPostpaidLines;
    }

    public int getMtPostpaidBilled() {
        return mtPostpaidBilled;
    }

    public void setMtPostpaidBilled(int mtPostpaidBilled) {
        this.mtPostpaidBilled = mtPostpaidBilled;
    }

    public int getNomadicPostpaidMSF() {
        return nomadicPostpaidMSF;
    }

    public void setNomadicPostpaidMSF(int nomadicPostpaidMSF) {
        this.nomadicPostpaidMSF = nomadicPostpaidMSF;
    }

    public int getNomadicPostpaidLines() {
        return nomadicPostpaidLines;
    }

    public void setNomadicPostpaidLines(int nomadicPostpaidLines) {
        this.nomadicPostpaidLines = nomadicPostpaidLines;
    }

    public int getNomadicPostpaidBilled() {
        return nomadicPostpaidBilled;
    }

    public void setNomadicPostpaidBilled(int nomadicPostpaidBilled) {
        this.nomadicPostpaidBilled = nomadicPostpaidBilled;
    }

    public AccountEntity getAccountId() {
        return accountId;
    }

    public void setAccountId(AccountEntity accountId) {
        this.accountId = accountId;
    }

    public int getSolutionsMSF() {
        return solutionsMSF;
    }

    public void setSolutionsMSF(int solutionsMSF) {
        this.solutionsMSF = solutionsMSF;
    }

    public int getSolutionsBilled() {
        return solutionsBilled;
    }

    public void setSolutionsBilled(int solutionsBilled) {
        this.solutionsBilled = solutionsBilled;
    }

    public int getSolutionsLines() {
        return solutionsLines;
    }

    public void setSolutionsLines(int solutionsLines) {
        this.solutionsLines = solutionsLines;
    }
}
