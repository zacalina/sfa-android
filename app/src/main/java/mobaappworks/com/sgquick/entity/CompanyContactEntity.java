package mobaappworks.com.sgquick.entity;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ldeguzman on 9/1/15.
 */
public class CompanyContactEntity extends RealmObject {

    @PrimaryKey
    private int id;
    private String name;
    private String email;
    private String contactNumber;
    private String contactCountryCode;
    private String contactExtension;
    private int companyId;
    private String contactAreaCode;
    private String birthDate;
    private String facebookId;
    private String twitterId;
    private String instagramId;
    private CompanyContactRoleEntity companyContactRoleId;
    private AccountEntity accountId;
    private String preference;
    private String interests;

    public CompanyContactEntity() {}

    public CompanyContactEntity(int id, String name, String email, String contactNumber,
                                int companyId, String birthDate, CompanyContactRoleEntity companyContactRoleId,
                                AccountEntity accountId) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.contactNumber = contactNumber;
        this.companyId = companyId;
        this.birthDate = birthDate;
        this.companyContactRoleId = companyContactRoleId;
        this.accountId = accountId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getContactCountryCode() {
        return contactCountryCode;
    }

    public void setContactCountryCode(String contactCountryCode) {
        this.contactCountryCode = contactCountryCode;
    }

    public String getContactExtension() {
        return contactExtension;
    }

    public void setContactExtension(String contactExtension) {
        this.contactExtension = contactExtension;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public String getContactAreaCode() {
        return contactAreaCode;
    }

    public void setContactAreaCode(String contactAreaCode) {
        this.contactAreaCode = contactAreaCode;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getTwitterId() {
        return twitterId;
    }

    public void setTwitterId(String twitterId) {
        this.twitterId = twitterId;
    }

    public String getInstagramId() {
        return instagramId;
    }

    public void setInstagramId(String instagramId) {
        this.instagramId = instagramId;
    }

    public CompanyContactRoleEntity getCompanyContactRoleId() {
        return companyContactRoleId;
    }

    public void setCompanyContactRoleId(CompanyContactRoleEntity companyContactRoleId) {
        this.companyContactRoleId = companyContactRoleId;
    }

    public AccountEntity getAccountId() {
        return accountId;
    }

    public void setAccountId(AccountEntity accountId) {
        this.accountId = accountId;
    }

    public String getPreference() {
        return preference;
    }

    public void setPreference(String preference) {
        this.preference = preference;
    }

    public String getInterests() {
        return interests;
    }

    public void setInterests(String interests) {
        this.interests = interests;
    }
}
