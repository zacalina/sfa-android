package mobaappworks.com.sgquick.entity;

import android.support.annotation.PluralsRes;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ldeguzman on 9/1/15.
 */
public class CityTownEntity extends RealmObject {

    @PrimaryKey
    private int id;
    private String name;
    private String shortCode;
    private int provinceIdFk;
    private int areaCode;

    public CityTownEntity() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortCode() {
        return shortCode;
    }

    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }

    public int getProvinceIdFk() {
        return provinceIdFk;
    }

    public void setProvinceIdFk(int provinceIdFk) {
        this.provinceIdFk = provinceIdFk;
    }

    public int getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(int areaCode) {
        this.areaCode = areaCode;
    }
}

