package mobaappworks.com.sgquick.entity;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ldeguzman on 9/1/15.
 */
public class RegionEntity extends RealmObject{

    @PrimaryKey
    private int id;
    private String shortCode;
    private String name;
    private String regionNumber;

    public RegionEntity() {
    }

    public RegionEntity(int id, String shortCode, String name, String regionNumber) {
        this.id = id;
        this.shortCode = shortCode;
        this.name = name;
        this.regionNumber = regionNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getShortCode() {
        return shortCode;
    }

    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegionNumber() {
        return regionNumber;
    }

    public void setRegionNumber(String regionNumber) {
        this.regionNumber = regionNumber;
    }
}
