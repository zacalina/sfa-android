package mobaappworks.com.sgquick.entity;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ldeguzman on 9/11/15.
 */
public class CheckOutEntity extends RealmObject {

    @PrimaryKey
    private int id;
    private AccountEntity accountEntity;
    private long lng;
    private long lat;
    private String date;

    public CheckOutEntity() {
    }

    public CheckOutEntity(int id, AccountEntity accountEntity, long lng, long lat, String date) {
        this.id = id;
        this.accountEntity = accountEntity;
        this.lng = lng;
        this.lat = lat;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public AccountEntity getAccountEntity() {
        return accountEntity;
    }

    public void setAccountEntity(AccountEntity accountEntity) {
        this.accountEntity = accountEntity;
    }

    public long getLng() {
        return lng;
    }

    public void setLng(long lng) {
        this.lng = lng;
    }

    public long getLat() {
        return lat;
    }

    public void setLat(long lat) {
        this.lat = lat;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
