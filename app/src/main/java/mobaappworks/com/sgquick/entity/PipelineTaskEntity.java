package mobaappworks.com.sgquick.entity;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ldeguzman on 9/1/15.
 */
public class PipelineTaskEntity extends RealmObject {

    @PrimaryKey
    private int id;
    private String pipelineTaskName;
    private Date dueDate;
    private String status;
    private Date startDate;
    private Date endDate;
    private PipelineEntity pipelineId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPipelineTaskName() {
        return pipelineTaskName;
    }

    public void setPipelineTaskName(String pipelineTaskName) {
        this.pipelineTaskName = pipelineTaskName;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public PipelineEntity getPipelineId() {
        return pipelineId;
    }

    public void setPipelineId(PipelineEntity pipelineId) {
        this.pipelineId = pipelineId;
    }
}
