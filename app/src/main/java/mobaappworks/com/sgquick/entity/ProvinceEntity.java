package mobaappworks.com.sgquick.entity;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ldeguzman on 9/1/15.
 */
public class ProvinceEntity extends RealmObject {

    @PrimaryKey
    private int id;
    private String name;
    private String shortCode;
    private int regionIdFk;

    public ProvinceEntity() {
    }

    public ProvinceEntity(int id, String name, String shortCode, int regionIdFk) {
        this.id = id;
        this.name = name;
        this.shortCode = shortCode;
        this.regionIdFk = regionIdFk;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortCode() {
        return shortCode;
    }

    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }

    public int getRegionIdFk() {
        return regionIdFk;
    }

    public void setRegionIdFk(int regionIdFk) {
        this.regionIdFk = regionIdFk;
    }
}
