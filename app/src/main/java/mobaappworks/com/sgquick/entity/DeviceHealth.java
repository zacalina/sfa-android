package mobaappworks.com.sgquick.entity;

import io.realm.RealmObject;

/**
 * Created by Migs on 9/20/2015.
 */
public class DeviceHealth extends RealmObject {

    private int id;
    private String signalStrength;
    private String batteryLife;
    private int hasInternet;
    private String transactionType;
    private String storageLevel;
    private String captureDate;
    private String employeeIdFk;
    private String imei;
    private String osVersion;
    private String deviceName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSignalStrength() {
        return signalStrength;
    }

    public void setSignalStrength(String signalStrength) {
        this.signalStrength = signalStrength;
    }

    public String getBatteryLife() {
        return batteryLife;
    }

    public void setBatteryLife(String batteryLife) {
        this.batteryLife = batteryLife;
    }

    public int getHasInternet() {
        return hasInternet;
    }

    public void setHasInternet(int hasInternet) {
        this.hasInternet = hasInternet;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getStorageLevel() {
        return storageLevel;
    }

    public void setStorageLevel(String storageLevel) {
        this.storageLevel = storageLevel;
    }

    public String getCaptureDate() {
        return captureDate;
    }

    public void setCaptureDate(String captureDate) {
        this.captureDate = captureDate;
    }

    public String getEmployeeIdFk() {
        return employeeIdFk;
    }

    public void setEmployeeIdFk(String employeeIdFk) {
        this.employeeIdFk = employeeIdFk;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }
}
