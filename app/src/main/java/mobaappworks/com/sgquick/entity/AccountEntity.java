package mobaappworks.com.sgquick.entity;

import java.util.Date;
import java.util.List;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ldeguzman on 9/1/15.
 */
public class AccountEntity extends RealmObject {

    @PrimaryKey
    private int id;
    private int accountId;
    private String accountName;
    private String registeredAccountName;
    private String accountAddress;
    private String unitNumber;
    private String dateAssigned;
    private String floorNumber;
    private String lowestFloorOccupied;
    private String highestFloorOccupied;
    private String buildingName;
    private String streetNumber;
    private String streetName;
    private int barangayIdFk;
    private int cityTownIdFk;
    private int provinceIdFk;
    private int regionIdFk;
    private int salesRegionIdFk;
    private String secRanking;

    private String gbuTagidFk;
    private int industryTypeIdFk;
    private String accountClassIdFk;
    private String tier;
    private String originalName;
    private String longitude;
    private String latitude;
    private String anniversary;
    private String numberOfEmployees;
    private String dateOfLastVisit;

    private String employeeId;
    private int segmentIdFk;
    private String cleanedName;
    private boolean isTapped;
    private String contactNumber;
    private String zipCode;
    private boolean isBayan;
    private Date acCheckoutDate;

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public int getSegmentIdFk() {
        return segmentIdFk;
    }

    public void setSegmentIdFk(int segmentIdFk) {
        this.segmentIdFk = segmentIdFk;
    }

    public String getCleanedName() {
        return cleanedName;
    }

    public void setCleanedName(String cleanedName) {
        this.cleanedName = cleanedName;
    }

    public boolean isTapped() {
        return isTapped;
    }

    public void setIsTapped(boolean isTapped) {
        this.isTapped = isTapped;
    }

    public boolean isBayan() {return isBayan;}

    public void setIsBayan(boolean isBayan){ this.isBayan = isBayan; }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getDateOfLastVisit() {
        return dateOfLastVisit;
    }

    public void setDateOfLastVisit(String dateOfLastVisit) {
        this.dateOfLastVisit = dateOfLastVisit;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getRegisteredAccountName() {
        return registeredAccountName;
    }

    public void setRegisteredAccountName(String registeredAccountName) {
        this.registeredAccountName = registeredAccountName;
    }

    public String getAccountAddress() {
        return accountAddress;
    }

    public void setAccountAddress(String accountAddress) {
        this.accountAddress = accountAddress;
    }

    public String getUnitNumber() {
        return unitNumber;
    }

    public void setUnitNumber(String unitNumber) {
        this.unitNumber = unitNumber;
    }

    public String getDateAssigned() {
        return dateAssigned;
    }

    public void setDateAssigned(String dateAssigned) {
        this.dateAssigned = dateAssigned;
    }

    public String getFloorNumber() {
        return floorNumber;
    }

    public void setFloorNumber(String floorNumber) {
        this.floorNumber = floorNumber;
    }

    public String getLowestFloorOccupied() {
        return lowestFloorOccupied;
    }

    public void setLowestFloorOccupied(String lowestFloorOccupied) {
        this.lowestFloorOccupied = lowestFloorOccupied;
    }

    public String getHighestFloorOccupied() {
        return highestFloorOccupied;
    }

    public void setHighestFloorOccupied(String highestFloorOccupied) {
        this.highestFloorOccupied = highestFloorOccupied;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public int getBarangayIdFk() {
        return barangayIdFk;
    }

    public void setBarangayIdFk(int barangayIdFk) {
        this.barangayIdFk = barangayIdFk;
    }

    public int getCityTownIdFk() {
        return cityTownIdFk;
    }

    public void setCityTownIdFk(int cityTownIdFk) {
        this.cityTownIdFk = cityTownIdFk;
    }

    public int getProvinceIdFk() {
        return provinceIdFk;
    }

    public void setProvinceIdFk(int provinceIdFk) {
        this.provinceIdFk = provinceIdFk;
    }

    public int getRegionIdFk() {
        return regionIdFk;
    }

    public void setRegionIdFk(int regionIdFk) {
        this.regionIdFk = regionIdFk;
    }

    public int getSalesRegionIdFk() {
        return salesRegionIdFk;
    }

    public void setSalesRegionIdFk(int salesRegionIdFk) {
        this.salesRegionIdFk = salesRegionIdFk;
    }

    public String getSecRanking() {
        return secRanking;
    }

    public void setSecRanking(String secRanking) {
        this.secRanking = secRanking;
    }

    public String getGbuTagidFk() {
        return gbuTagidFk;
    }

    public void setGbuTagidFk(String gbuTagidFk) {
        this.gbuTagidFk = gbuTagidFk;
    }

    public int getIndustryTypeIdFk() {
        return industryTypeIdFk;
    }

    public void setIndustryTypeIdFk(int industryTypeIdFk) {
        this.industryTypeIdFk = industryTypeIdFk;
    }

    public String getAccountClassIdFk() {
        return accountClassIdFk;
    }

    public void setAccountClassIdFk(String accountClassIdFk) {
        this.accountClassIdFk = accountClassIdFk;
    }

    public String getTier() {
        return tier;
    }

    public void setTier(String tier) {
        this.tier = tier;
    }

    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getAnniversary() {
        return anniversary;
    }

    public void setAnniversary(String anniversary) {
        this.anniversary = anniversary;
    }

    public String getNumberOfEmployees() {
        return numberOfEmployees;
    }

    public void setNumberOfEmployees(String numberOfEmployees) {
        this.numberOfEmployees = numberOfEmployees;
    }

    public Date getAcCheckoutDate() {
        return acCheckoutDate;
    }

    public void setAcCheckoutDate(Date acCheckoutDate) {
        this.acCheckoutDate = acCheckoutDate;
    }
}
