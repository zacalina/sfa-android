package mobaappworks.com.sgquick;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.InputFilter;
import android.text.method.LinkMovementMethod;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.mobaappworks.R;

import io.fabric.sdk.android.Fabric;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmResults;
import mobaappworks.com.sgquick.accounts.AccountsList;
import mobaappworks.com.sgquick.coverage.Coverage;
import mobaappworks.com.sgquick.entity.EmployeeEntity;

public class LoginActivity extends Activity implements View.OnClickListener {
    private Button loginButton;
    private TextView forgotPassword;
    private TextView tvUsername;
    private TextView tvPassword;
    private ImageView globeLogo;
    private TextView tvAppname;
    private Context context;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private boolean isLoggedIn;
    private EmployeeEntity employee;
    private String newKey;
    private String savedKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        //Set the application fullscreen mode
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //Set the content view
        setContentView(R.layout.activity_login);
        context = this;
        Realm realm = Realm.getDefaultInstance();
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        editor = preferences.edit();

        employee = realm.where(EmployeeEntity.class).findFirst();
        isLoggedIn = preferences.getBoolean(Constant.IS_LOGGED_IN, false);

        if (employee != null && isLoggedIn) {
            lauchHomeScreen();
        }

        //Get Login button and set click action
        loginButton = (Button) findViewById(R.id.login_button);
        loginButton.setOnClickListener(this);

        tvUsername = (TextView) findViewById(R.id.username);
        tvPassword = (TextView) findViewById(R.id.password);
        tvAppname = (TextView) findViewById(R.id.textView4);
//        progressBar = (ProgressBar) findViewById(R.id.progressBar);
//        progressBar.setVisibility(View.INVISIBLE);


        //Set globe logo
//        globeLogo = (ImageView) findViewById(R.id.iv_globe_logo);
//        globeLogo.setImageResource(R.drawable.globe_logo);1
//        forgotPassword = (TextView) findViewById(R.id.forgot_password);
//        forgotPassword.setMovementMethod(LinkMovementMethod.getInstance());
//        forgotPassword.setOnClickListener(this);

        Typeface font = Typeface.createFromAsset(getAssets(), Constant.FONT_FSELLIOT_THIN);
        tvAppname.setTypeface(font);
        tvAppname.setGravity(Gravity.CENTER);
        tvUsername.setTypeface(font);
        tvUsername.setGravity(Gravity.CENTER);
        tvUsername.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        tvPassword.setTypeface(font);
        tvPassword.setGravity(Gravity.CENTER);

//        Typeface font2 = Typeface.createFromAsset(getAssets(), Constant.FONT_FSELLIOT_REG);
        loginButton.setTypeface(font);
        loginButton.setTextSize(20);
//        forgotPassword.setTypeface(font2);
//        forgotPassword.setTextSize(12);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.login_button:
                final String username = tvUsername.getText().toString();
                final String pword = tvPassword.getText().toString();
                final String key = username + ":" + pword;
                savedKey = preferences.getString(Constant.KEY, null);
                newKey = Base64.encodeToString(key.getBytes(),Base64.NO_WRAP);

                loginButton.setEnabled(false);


                    if(employee == null
                            || !employee.getEmail().toUpperCase().equals(username)
                            || !newKey.equals(savedKey)){
                        editor.putString(Constant.KEY, newKey);
                        editor.commit();
                        new LoginActivityAsyncTask().execute(Constant.LOGIN_URL(this) + username + "/");
                    }else{
                        editor.putBoolean(Constant.IS_LOGGED_IN, true);
                        editor.commit();
                        lauchHomeScreen();
                    }

                break;
//            case R.id.forgot_password:
//                Intent intent = new Intent(this,ForgotPassword.class);
//                startActivity(intent);
//                break;
            default:
                Toast.makeText(LoginActivity.this, "No Action Found", Toast.LENGTH_SHORT).show();
        }
    }

    private class LoginActivityAsyncTask extends AsyncTask<String, Integer, String> {
        @Override
        protected void onProgressUpdate(Integer... progress) {
        }
        @Override
        protected void onPostExecute(String result) {

            try {
                if(Util.isNetworkAvailable(context)) {
                    if (result != null) {
                        Realm realm = Realm.getDefaultInstance();

                        realm.beginTransaction();
                        EmployeeEntity employee = realm.createOrUpdateObjectFromJson(EmployeeEntity.class, result);
                        realm.commitTransaction();

                        Intent intent = new Intent(context, LoadingData.class);
                        intent.putExtra(Constant.SYNC, false);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);

                        Util.setUserId(employee.getEmployeeId());
                        Util.setEmail(employee.getEmail());
                        Util.setUsername(employee.getFirstName() + " " + employee.getLastName());

                        editor.putBoolean(Constant.IS_LOGGED_IN, true);
                        editor.putString(Constant.BARANGAY_LAST_SYNC, Constant.SDF_RESYNC.format(Util.getCalendarWithValues(01,1970,01,0,0,0,0).getTime()));
                        editor.commit();

                        startActivity(intent);
                    }
                    else {
                        editor.putString(Constant.KEY, savedKey);
                        editor.commit();
                        Toast.makeText(context, "Username/Password incorrect", Toast.LENGTH_SHORT).show();
                        loginButton.setEnabled(true);
                    }
                }else{
                    editor.putString(Constant.KEY, savedKey);
                    editor.commit();
                    Toast.makeText(context, "No Connection", Toast.LENGTH_SHORT).show();
                    loginButton.setEnabled(true);
                }

            }catch (Exception e){
                e.printStackTrace();
                editor.putString(Constant.KEY, savedKey);
                editor.commit();
                Toast.makeText(context, "Username/Password incorrect", Toast.LENGTH_SHORT).show();
                loginButton.setEnabled(true);
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                return HttpManager.GET(strings[0], context);
            } catch (IOException e) {
                return null;
            }
        }
    }

    public void lauchHomeScreen(){
        Intent intent = new Intent(context, Coverage.class);
        intent.putExtra(Constant.ID, Integer.valueOf(employee.getEmployeeId()));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        Util.setEmail(employee.getEmail());
        Util.setUsername(employee.getFirstName() + " " + employee.getLastName());
        Util.setUserId(employee.getEmployeeId());

        startActivity(intent);
        finish();
    }
}

