package mobaappworks.com.sgquick;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mobaappworks.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmResults;
import mobaappworks.com.sgquick.accounts.AccountsList;
import mobaappworks.com.sgquick.coverage.Coverage;
import mobaappworks.com.sgquick.entity.CoveragePlanEntity;
import mobaappworks.com.sgquick.entity.EmployeeEntity;
import mobaappworks.com.sgquick.entity.SavingStatus;

public class CustomDrawerAdapter extends RecyclerView.Adapter<CustomDrawerAdapter.ViewHolder> {

    public static Context context;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private String mNavTitles[];
    private int mIcons[];
    private String name;
    private String email;
    private Realm realm;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        int Holderid;
        TextView textView;
        ImageView imageView;
        TextView Name;
        TextView email;
        TextView tv;

        public ViewHolder(View itemView,int ViewType, Typeface typeface) {
            super(itemView);


            context = itemView.getContext();

            itemView.setOnClickListener(ViewHolder.this);

            if(ViewType == TYPE_ITEM) {
                textView = (TextView) itemView.findViewById(R.id.rowText);
                textView.setTypeface(typeface);
                imageView = (ImageView) itemView.findViewById(R.id.rowIcon);
                Holderid = 1;
            }
            else{
                Name = (TextView) itemView.findViewById(R.id.name);
                email = (TextView) itemView.findViewById(R.id.email);
//                profile = (ImageView) itemView.findViewById(R.id.circleView);
                Holderid = 0;
            }
        }

        @Override
        public void onClick(View view) {
            //TODO: HIGHLIGHT SELECTED ITEM
            Intent intent;
            try {
                tv = (TextView) view.findViewById(R.id.rowText);
                String tag = (String) tv.getTag();
                switch (tag){
//                    case "Accounts":
//                        intent = new Intent(context,AccountsList.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        context.startActivity(intent);
//                        break;
                    case "Coverage":
                        intent = new Intent(context,Coverage.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.putExtra("syncStatus", "drawerIntent");
                        context.startActivity(intent);
                        break;
                    case "Pipeline":
                        Toast.makeText(context,"Currently Disabled", Toast.LENGTH_SHORT).show();
//                        intent = new Intent(context,Pipeline.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        context.startActivity(intent);
                        break;

                    case "Logout":

                        // TODO: This should just flag the user as logged out
                        preferences = PreferenceManager.getDefaultSharedPreferences(context);
                        editor = preferences.edit();

                        editor.putBoolean(Constant.IS_LOGGED_IN, false);
                        editor.commit();

                        intent = new Intent(context, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        context.startActivity(intent);
                        break;

                    case "Settings":
                        intent = new Intent(context, SettingsActivity.class);
                        context.startActivity(intent);
                        break;

                    case "Sync":
                        intent = new Intent(context, LoadingData.class);
                        intent.putExtra(Constant.SYNC, true);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        context.startActivity(intent);
                        break;

                    default:
                        Log.d("[ONCLICK DEFAULT]", tag);
                        break;
                }
            }catch (NullPointerException e){
                e.printStackTrace();
                Log.d("[1st item was clicked]","");
            }
        }
    }

    public CustomDrawerAdapter(String titles[],int icons[],String name,String email){
        this.mNavTitles = titles;
        this.mIcons = icons;
        this.name = name;
        this.email = email;
    }

    @Override
    public CustomDrawerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Typeface font = Typeface.createFromAsset(parent.getContext().getAssets(), Constant.FONT_FSELLIOT_REG);

        if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.drawer_list_item, parent, false);
            ViewHolder vhItem = new ViewHolder(v, viewType, font);
            return vhItem;

        } else if (viewType == TYPE_HEADER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.header,parent,false);
            ViewHolder vhHeader = new ViewHolder(v, viewType, font);
            return vhHeader;
        }
        return null;

    }

    @Override
    public void onBindViewHolder(CustomDrawerAdapter.ViewHolder holder, int position) {
        if(holder.Holderid ==1) {
            if(position == 3){
                String myGetLastSyncDate = Util.getLastSyncDate(context);
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sssZ");
                Date myDate = null;
                try {
                    myDate = dateFormat.parse(myGetLastSyncDate);

                } catch (ParseException e) {
                    e.printStackTrace();
                }
                SimpleDateFormat timeFormat = new SimpleDateFormat("'as of 'MM/dd/yyyy' 'HH':'mm' 'a");
                String finalDate = timeFormat.format(myDate);

                    holder.textView.setText(mNavTitles[position - 1] + " (" + finalDate + ")");



            }else{
                holder.textView.setText(mNavTitles[position - 1]);
            }

            holder.textView.setTag(mNavTitles[position - 1]);
            holder.imageView.setImageResource(mIcons[position -1]);
        }
        else{
            holder.Name.setText(name);
            holder.email.setText(email);

        }
    }

    @Override
    public int getItemCount() {
        return mNavTitles.length+1;
    }


    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;
        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

}