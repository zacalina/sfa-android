package mobaappworks.com.sgquick.pipeline;

import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TypefaceSpan;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;

import com.mobaappworks.R;

import mobaappworks.com.sgquick.Constant;
import mobaappworks.com.sgquick.CustomDrawerAdapter;
import mobaappworks.com.sgquick.Util;

public class Pipeline extends AppCompatActivity {

    private PipelinePagerAdapter mCoveragePagerAdapter;
    private ViewPager mViewPager;
    private PagerTabStrip pagerTabStrip;
    private ActionBarDrawerToggle mDrawerToggle;
    private String USER;
    private String EMAIL;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private DrawerLayout Drawer;

    private String mTitle = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Hide the title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homescreen);


        CustomDrawerAdapter.context = getApplicationContext();


        // actionbar styling
        SpannableString s = new SpannableString("Pipeline");
        s.setSpan(new TypefaceSpan(Constant.FONT_FSELLIOT_REG), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        USER = Util.getUsername();
        EMAIL = Util.getEmail();

        //TODO: Get title from selected menu
        mTitle = "Pipeline";
        getSupportActionBar().setTitle(s);

        mRecyclerView = (RecyclerView) findViewById(R.id.RecyclerView);
        mRecyclerView.setHasFixedSize(true);

        mAdapter = new CustomDrawerAdapter(Constant.TITLES,Constant.ICONS,USER,EMAIL);
        mRecyclerView.setAdapter(mAdapter);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout);
        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                Drawer,
                R.string.drawer_open,
                R.string.drawer_close) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
                getSupportActionBar().setTitle("");
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
                getSupportActionBar().setTitle(mTitle);
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                getSupportActionBar().setTitle(mTitle);
            }
        };

        Drawer.setDrawerListener(mDrawerToggle);
        mDrawerToggle.setDrawerIndicatorEnabled(true);

        mCoveragePagerAdapter = new PipelinePagerAdapter(getSupportFragmentManager(), this);

        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mCoveragePagerAdapter);

        pagerTabStrip = (PagerTabStrip) findViewById(R.id.pager_title_strip);
        pagerTabStrip.setDrawFullUnderline(true);
        pagerTabStrip.setTabIndicatorColor(Color.rgb(232, 99, 160));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

}