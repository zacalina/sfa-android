package mobaappworks.com.sgquick.pipeline;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.mobaappworks.R;

import mobaappworks.com.sgquick.Constant;
import mobaappworks.com.sgquick.FontChangeCrawler;


public class PipelineFragmentList extends Fragment {

    private Context context;

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        FontChangeCrawler fontChanger = new FontChangeCrawler(this.context.getAssets(), Constant.FONT_FSELLIOT_REG);
        fontChanger.replaceFonts((ViewGroup) this.getView());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_pipeline_list, container, false);
        Bundle args = getArguments();

        TableLayout table = (TableLayout) rootView.findViewById(R.id.tableLayout1);
        table.addView(setHeaders());
        //TODO: ADD ACTUAL DATA

        return rootView;
    }

    public TableRow setHeaders(){

        TableRow headers = new TableRow(getActivity());
        TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,TableRow.LayoutParams.WRAP_CONTENT,1f);
        headers.setLayoutParams(params);

        //STAGE
        TextView stage = new TextView(getActivity());
        stage.setBackgroundResource(R.drawable.cell_shape);
        stage.setGravity(Gravity.CENTER);
        stage.setTextSize(18);
        stage.setText("Stage");
        stage.setPadding(10, 5, 10, 5);
        stage.setLayoutParams(params);

        //ACCOUNTS
        TextView acct = new TextView(getActivity());
        acct.setBackgroundResource(R.drawable.cell_shape);
        acct.setGravity(Gravity.CENTER);
        acct.setTextSize(18);
        acct.setText("Accnt");
        acct.setPadding(10, 5, 10, 5);
        acct.setLayoutParams(params);

        //PROD
        TextView rev = new TextView(getActivity());
        rev.setBackgroundResource(R.drawable.cell_shape);
        rev.setGravity(Gravity.CENTER);
        rev.setTextSize(18);
        rev.setText("Prod");
        rev.setPadding(10, 5, 10, 5);
        rev.setLayoutParams(params);


        //AMNT
        TextView amnt = new TextView(getActivity());
        amnt.setBackgroundResource(R.drawable.cell_shape);
        amnt.setGravity(Gravity.CENTER);
        amnt.setTextSize(18);
        amnt.setText("Amnt");
        amnt.setPadding(10, 5, 10, 5);
        amnt.setLayoutParams(params);

        headers.addView(stage);
        headers.addView(acct);
        headers.addView(rev);
        headers.addView(amnt);

        return headers;

    }
}