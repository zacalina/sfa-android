package mobaappworks.com.sgquick.pipeline;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class PipelinePagerAdapter extends FragmentPagerAdapter {
    Context mContext;
    public PipelinePagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        Bundle args = new Bundle();

        switch (position){
            case 0:
                fragment = new PipelineFragmentList();
                return fragment;
            case 1:
                fragment = new PipelineFragmentChart();
                return fragment;
        }

        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        switch (position){
            case 0:
                return "List";
            case 1:
                return "Chart";
            default:
                return "Error";
        }
    }
}