package mobaappworks.com.sgquick.pipeline;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.mobaappworks.R;

import java.util.ArrayList;
import java.util.List;

import mobaappworks.com.sgquick.Constant;
import mobaappworks.com.sgquick.FontChangeCrawler;

public class PipelineFragmentChart extends Fragment {

    private Context context;

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        FontChangeCrawler fontChanger = new FontChangeCrawler(this.context.getAssets(), Constant.FONT_FSELLIOT_REG);
        fontChanger.replaceFonts((ViewGroup) this.getView());
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_pipeline_chart, container, false);
        Bundle args = getArguments();


        PieChart pc = (PieChart) rootView.findViewById(R.id.pie_chart);
        PieData pd = new PieData(getXAxisValues(),getDataSet());
        pc.setData(pd);

        return rootView;
    }


    private PieDataSet getDataSet() {
        ArrayList<Entry> entries = new ArrayList<>();
        entries.add(new Entry((float) 53.0, 0));
        entries.add(new Entry((float) 47.0, 1));
        PieDataSet dataSet = new PieDataSet(entries,"");
        dataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        dataSet.setSliceSpace(3f);
        return dataSet;
    }

    private ArrayList<String> getXAxisValues() {
        ArrayList<String> xAxis = new ArrayList<>();
        xAxis.add("Data");
        xAxis.add("Solutions");
        return xAxis;
    }


}