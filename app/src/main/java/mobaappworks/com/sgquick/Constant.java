package mobaappworks.com.sgquick;

import android.content.Context;

import com.mobaappworks.R;

import java.text.SimpleDateFormat;

/**
 * Created by ldeguzman on 8/18/15.
 */
public class Constant {

    //SIDE DRAWER STUFF
    public static final String TITLES[] = {
 //           "Accounts",
            "Coverage",
            "Settings",
            "Sync",
            "Logout"
    };
    //SIDE DRAWER STUFF
    public static final int ICONS[] = {
 //           R.drawable.accounts,
            R.drawable.coverage,
            R.drawable.entity,
            R.drawable.sync,
            R.drawable.logout

    };

    //INTENT KEY STUFF
    public static final String ACCOUNT_LIST_TYPE = "ACCOUNT_LIST_TYPE";
    public static final String IS_ACCOUNT_LIST = "IS_ACCOUNT_LIST";
    public static final String ID = "ID";
    public static final String SYNC = "SYNC";
    public static final String USERNAME = "USERNAME";
    public static final String EMAIL = "EMAIL";
    public static final String ACCOUNT_NAME = "ACCOUNT_NAME";
    public static final String IS_CHECKED_IN = "IS_CHECKED_IN";
    public static final String IS_NEW = "IS_NEW";
    public static final String PIPELINE_ID = "PIPELINE_ID";
    public static final String RESEND = "RESEND";

    //PK STUFF
    public static final String SPK_PIPELINE = "ph.com.globe.pipeline.key";
    public static final String CPU_CHECKIN = "ph.com.globe.check.in";
    public static final String CPU_CHECKOUT = "ph.com.globe.check.out";

    //PREFERENCES
    public static final String INITIALIZE_DATA = "INIT_DATA";
    public static final String INITIALIZE_DATA_DUMMY = "INIT_DATA_DUMMY";
    public static final String COVERAGE_LOADED = "COVERAGE_LOADED";
    public static final String LAST_SYNC_DATE = "LAST_SYNC_DATE";
    public static final String MY_LAST_SYNC_DATE = "MY_LAST_SYNC_DATE";

    public static final String BASE_URL_PREF = "BASE_URL_PREF";
    public static final String IS_LOGGED_IN = "IS_LOGGED_IN";
    public static final String KEY = "KEY";
    public static final String BARANGAY_LAST_SYNC = "BARANGAY_LAST_SYNC";

    //REST STUFF
    //WITHOUT AUTH
//    public static String DEFAULT_BASE_URL = "http://ec2-52-27-162-238.us-west-2.compute.amazonaws.com:8080/cerebro-1.0-SNAPSHOT";
    //WITH AUTH
//    public static final String DEFAULT_BASE_URL = "http://ec2-52-89-202-252.us-west-2.compute.amazonaws.com:8080/cerebro-1.0-SNAPSHOT";
//    public static final String DEFAULT_BASE_URL = "http://ec2-52-76-108-246.ap-southeast-1.compute.amazonaws.com:8080/cerebro-1.0-SNAPSHOT";
//    public static final String DEFAULT_BASE_URL = "http://ec2-52-76-76-241.ap-southeast-1.compute.amazonaws.com:8080/cerebro-1.0-SNAPSHOT";
// for SFA production
//    public static final String DEFAULT_BASE_URL = "http://ec2-52-74-1-51.tap-southeast-1.compute.amazonaws.com:8080/cerebro-1.0-SNAPSHOT";
// for SFA ACQUI
//public static final String DEFAULT_BASE_URL = "http://ec2-54-251-157-134.ap-southeast-1.compute.amazonaws.com:8080/cerebro-1.0-SNAPSHOT";

    //test bed
    public static final String DEFAULT_BASE_URL = "http://ec2-52-77-224-156.ap-southeast-1.compute.amazonaws.com:8080/cerebro-1.0-SNAPSHOT";


    // for SFA iOS
//   public static final String DEFAULT_BASE_URL = "http://ec2-52-74-19-249.ap-southeast-1.compute.amazonaws.com:8080/cerebro-1.0-SNAPSHOT";

    //SFA ultimate production api.cerebro.sg.globe.com.ph
//    public static final String DEFAULT_BASE_URL = "http://api.cerebro.sg.globe.com.ph:8080/cerebro-1.0-SNAPSHOT";
    // this is for TRAINING
//    public static final String DEFAULT_BASE_URL =  "http://ec2-52-77-208-236.ap-southeast-1.compute.amazonaws.com:8080/cerebro-1.0-SNAPSHOT";

    public static final String PRODUCT_TYPE_URL(Context context){
        return Util.getBaseUrl(context) + "/lookup/productType/getProductTypes";
    }
    public static final String REGION_URL(Context context){
        return Util.getBaseUrl(context) + "/lookup/region/getRegions";
    }
    public static final String INDUSTRY_URL(Context context){
        return Util.getBaseUrl(context) + "/lookup/industryType/getAllIndustryTypes";
    }
    public static final String ROLE_URL(Context context){
        return Util.getBaseUrl(context) + "/lookup/getAccountContactRoles";
    }
    public static final String LOGIN_URL(Context context){
        return Util.getBaseUrl(context) + "/employee/login/";
    }
    public static final String ADD_CHECK_IN_OUT_URL(Context context){
        return Util.getBaseUrl(context) + "/account/addCheckInOut";
    }
    public static final String ACCOUNTS_URL(Context context){
        return Util.getBaseUrl(context) + "/account/getAccounts";
    }

    public static final String CITY_URL(Context context){
        return Util.getBaseUrl(context) + "/lookup/citiestowns";
    }

    public static final String COUNT_ALL_CITY_URL(Context context){
        return Util.getBaseUrl(context) + "/lookup/citiestowns/size";
    }

    public static final String PROVINCE_URL(Context context){
        return Util.getBaseUrl(context) + "/lookup/province/getAllProvinces";
    }

    public static final String COUNT_ALL_BARANGAY_URL(Context context){
        return Util.getBaseUrl(context) + "/lookup/barangay/size";
    }

    public static final String BARANGAY_URL(Context context){
//        return Util.getBaseUrl(context) + "/lookup/barangay/getAllBarangays";
        return Util.getBaseUrl(context) + "/lookup/barangay";
    }
    public static final String COVERAGE_MONTH(Context context){
        return Util.getBaseUrl(context) + "/employee/getCoveragePlan";
    }
    public static final String ADD_DEVICE_HEALTH(Context context){
        return Util.getBaseUrl(context) + "/util/addDeviceHealth";
    }
    public static final String GET_PROJECT_URL(Context context){
        return Util.getBaseUrl(context) + "/lookup/project/getProjects";
    }
    public static final String GET_REMARKS_URL(Context context){
        return Util.getBaseUrl(context) + "/lookup/pipelineRemarks/getPipelineRemarks";
    }
    public static final String GET_PIPELINE(Context context){
        return Util.getBaseUrl(context) + "/account/getAccountPipelines";
    }
    public static final String GET_CURRENT_PIPELINE(Context context){
        return Util.getBaseUrl(context) + "/account/getCurrentAccountPipeline";
    }
    public static final String SAVE_PIPELINE_URL(Context context){
        return Util.getBaseUrl(context) + "/account/savePipeline";
    }

    public static final String UPDATE_ACCOUNT_PIPELINE_URL(Context context){
        return Util.getBaseUrl(context) + "/account/updateAccountPipeline";
    }


    public static final String ADD_ACCOUNT_PIPELINE_URL(Context context){
        return Util.getBaseUrl(context) + "/account/saveAccountPipeline";
    }

    public static final String MY_ADD_ACCOUNT_PIPELINE_URL(Context context){
        return Util.getBaseUrl(context) + "/account/updateAccountPipeline";
    }


    public static final String UPDATE_SCHEDULE(Context context){
        return Util.getBaseUrl(context) + "/employee/updateCoveragePlan";
    }

    public static final String UPDATE_ACCOUNT(Context context){
        return Util.getBaseUrl(context) + "/account/updateAccount";
    }

    public static final String GET_ACCOUNT_COUNT(Context context){
        return Util.getBaseUrl(context) + "/account/countAllAccounts";
    }

    public static final String GET_ACCOUNT_BY_EMPLOYEE_COUNT(Context context){
        return Util.getBaseUrl(context) + "/account/countAllAccounts";
    }

    public static final String ACCOUNTS_BY_EMPLOYEE_URL(Context context){
        return Util.getBaseUrl(context) + "/account/getAccounts";
    }


    //DESIGN
    public static final String FONT_FSELLIOT_REG = "FSElliotPro-Regular.otf";
    public static final String FONT_FSELLIOT_BOLD = "FSElliotPro-Bold.otf";
    public static final String FONT_FSELLIOT_HEAVY = "FSElliotPro-Heavy.otf";
    public static final String FONT_FSELLIOT_LIGHT = "FSElliotPro-Light.otf";
    public static final String FONT_FSELLIOT_THIN = "FSElliotPro-Thin.otf";

    public static final String FONT_HELVETICA_TTF = "Helvetica.ttf";
    public static final String FONT_HELVETICA_NEUE_ULTRALIGHT_OBL = "HelveticaNeue-UltraLigExtObl.otf";
    public static final String FONT_HELVETICA_NEUE_ULTRALIGHT_ITAL = "HelveticaNeue-UltraLightItal.otf";
    public static final String FONT_HELVETICA_NEUE_LIGHT = "HelveticaNeueCE-Light.otf";
    public static final String FONT_HELVETICA_NEUE_MEDIUM = "HelveticaNeueCE-Medium.otf";
    public static final String FONT_HELVETICA_NEUE_ROMAN = "HelveticaNeueCE-Roman.otf";

    // COLORS
    public static final String COLOR_BLUE = "005ab0";
    public static final String COLOR_DARKER_BLUE = "134891";
    public static final String COLOR_GREEN = "a4b419";
    public static final String COLOR_VIOLET = "8d2e97";
    public static final String COLOR_PINK = "e863a0";

    //SDFs
    public static final SimpleDateFormat SDF_API = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sssZ");
    public static final SimpleDateFormat SDF_RESYNC = new SimpleDateFormat("yyyy-MM-dd");
    //public static final SimpleDateFormat SDF_SYNC = new SimpleDateFormat("'as of 'MM/dd/yyyy' 'HH':'mm' 'a");
    public static final SimpleDateFormat DRAWER_SDF_SYNC = new SimpleDateFormat("'as of 'MM/dd/yyyy' 'HH':'mm' 'a");
    public static final SimpleDateFormat SDF_SYNC = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sssZ");
    public static final SimpleDateFormat SDF_DATE_TIME = new SimpleDateFormat("MM/dd/yyyy' 'HH':'mm");
    public static final SimpleDateFormat SDF_DATE = new SimpleDateFormat("MM/dd/yyyy");
    public static final SimpleDateFormat SDF_MONTH = new SimpleDateFormat("MMMM', 'yyyy");

}

