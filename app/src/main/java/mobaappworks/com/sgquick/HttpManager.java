package mobaappworks.com.sgquick;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;

import io.realm.Realm;
import mobaappworks.com.sgquick.entity.SavingStatus;

/**
 * Created by ldeguzman on 9/12/15.
 */
public class HttpManager {

    private static String DEBUG_TAG = "HTTP MANAGER";
    private static Realm realm;
    private static SharedPreferences preferences;
    private static SharedPreferences.Editor editor;

    public static String GET(String stringUrl, Context context) throws IOException {
        Log.d(DEBUG_TAG, "The url is: " + stringUrl);

        if(preferences == null){
            preferences = PreferenceManager.getDefaultSharedPreferences(context);
        }
        if(editor == null){
            editor = preferences.edit();
        }


        InputStream is = null;
        try {
            URL url = new URL(stringUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setRequestProperty("Authorization", "Basic " + preferences.getString(Constant.KEY, ""));
            conn.connect();
            Integer response = conn.getResponseCode();
            Log.d(DEBUG_TAG, "The response is: " + response);

            is = conn.getInputStream();
            String contentAsString = convertInputStreamToString(is);
            Log.d(DEBUG_TAG, "The body is: " + contentAsString);

            if(response != 200 || "".equals(contentAsString))
                return null;

            return contentAsString;

        } finally {
            if (is != null) {
                is.close();
            }
        }
    }

    public static String POST(String stringUrl, String body, Context context){
        return POST(stringUrl, body, false, context);
    }

    public static String POST(String stringUrl, String body, Boolean isResending, Context context) {
        Log.d("[HTTP POST]","URL: " + stringUrl);
        Log.d("[HTTP POST]","BODY: " + body);

        if(preferences == null){
            preferences = PreferenceManager.getDefaultSharedPreferences(context);
        }
        if(editor == null){
            editor = preferences.edit();
        }

        URL url;
        String response = "";
        try {
            url = new URL(stringUrl);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            conn.setRequestProperty("Authorization", "Basic " + preferences.getString(Constant.KEY, ""));
            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(body);

            writer.flush();
            writer.close();
            os.close();
            int responseCode=conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line=br.readLine()) != null) {
                    response+=line;
                }
            } else {
                if(!isResending) {
                    realm = Realm.getDefaultInstance();
                    realm.beginTransaction();
                    SavingStatus savingStatus = new SavingStatus();
                    savingStatus.setId(Util.getNextKey(SavingStatus.class).intValue());
                    savingStatus.setMethod("POST");
                    savingStatus.setBody(body);
                    savingStatus.setUrl(stringUrl);
                    savingStatus.setTimestamp(new Date());
                    savingStatus.setType(stringUrl);
                    realm.copyToRealmOrUpdate(savingStatus);
                    realm.commitTransaction();
                }
                response=stringUrl;
                throw new HttpException(responseCode+"");
            }
        } catch (Exception e) {
            if(!isResending) {
                realm = Realm.getDefaultInstance();
                realm.beginTransaction();
                SavingStatus savingStatus = new SavingStatus();
                savingStatus.setId(Util.getNextKey(SavingStatus.class).intValue());
                savingStatus.setMethod("POST");
                savingStatus.setBody(body);
                savingStatus.setUrl(stringUrl);
                savingStatus.setTimestamp(new Date());
                savingStatus.setType(stringUrl);
                realm.copyToRealmOrUpdate(savingStatus);
                realm.commitTransaction();
            }
            e.printStackTrace();
        }

        return response;
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException{
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;
        inputStream.close();
        return result;
    }

    public static String PUT(String stringUrl, String body, Context context){
        return PUT(stringUrl, body, false, context);
    }

    public static String PUT(String stringUrl, String body, Boolean isResending, Context context) {
        Log.d("[HTTP POST]","URL: " + stringUrl);
        Log.d("[HTTP POST]","BODY: " + body);

        if(preferences == null){
            preferences = PreferenceManager.getDefaultSharedPreferences(context);
        }
        if(editor == null){
            editor = preferences.edit();
        }

        URL url;
        String response = "";
        try {
            url = new URL(stringUrl);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("PUT");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            conn.setRequestProperty("Authorization", "Basic " + preferences.getString(Constant.KEY, ""));
            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(body);

            writer.flush();
            writer.close();
            os.close();
            int responseCode=conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK || responseCode == 204) {
                String line;
                BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line=br.readLine()) != null) {
                    response+=line;
                }
            } else {
                if(!isResending) {
                    realm = Realm.getDefaultInstance();
                    realm.beginTransaction();
                    SavingStatus savingStatus = new SavingStatus();
                    savingStatus.setId(Util.getNextKey(SavingStatus.class).intValue());
                    savingStatus.setMethod("PUT");
                    savingStatus.setBody(body);
                    savingStatus.setUrl(stringUrl);
                    savingStatus.setTimestamp(new Date());
                    savingStatus.setType(stringUrl);
                    realm.copyToRealmOrUpdate(savingStatus);
                    realm.commitTransaction();
                }
                response=stringUrl;
                throw new HttpException(responseCode+"");
            }
        } catch (Exception e) {
            if(!isResending) {
                realm = Realm.getDefaultInstance();
                realm.beginTransaction();
                SavingStatus savingStatus = new SavingStatus();
                savingStatus.setId(Util.getNextKey(SavingStatus.class).intValue());
                savingStatus.setMethod("PUT");
                savingStatus.setBody(body);
                savingStatus.setUrl(stringUrl);
                savingStatus.setTimestamp(new Date());
                savingStatus.setType(stringUrl);
                realm.copyToRealmOrUpdate(savingStatus);
                realm.commitTransaction();
            }
            e.printStackTrace();
        }

        return response;
    }



}
