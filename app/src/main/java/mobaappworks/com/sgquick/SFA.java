package mobaappworks.com.sgquick;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class SFA extends Application {
  @Override
  public void onCreate() {
    super.onCreate();

    System.out.println("jcgofredo");
    FontsOverride.setDefaultFont(this,  "DEFAULT", Constant.FONT_HELVETICA_NEUE_MEDIUM);

    RealmConfiguration config = new RealmConfiguration.Builder(this).build();
    Realm.setDefaultConfiguration(config);
  }
}