package mobaappworks.com.sgquick;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by J. Gofredo on 10/2/2015.
 */
public class FontChangeCrawler
{
    private Typeface typeface;

    public FontChangeCrawler(Typeface typeface)
    {
        this.typeface = typeface;
    }

    public FontChangeCrawler(AssetManager assets, String assetsFontFileName)
    {
        typeface = Typeface.createFromAsset(assets, assetsFontFileName);
    }

    public void replaceFonts(ViewGroup viewTree)
    {
        View child;
        for(int i = 0; i < viewTree.getChildCount(); ++i)
        {
            child = viewTree.getChildAt(i);
            if(child instanceof ViewGroup)
            {
                // recursive call
                replaceFonts((ViewGroup)child);
            }
            else if(child instanceof TextView)
            {
                // base case
                ((TextView) child).setTypeface(typeface);
            }
        }
    }
}


//public class FontUtil {
//
//    private static Typeface mTitleFont;
//    private static Typeface mContentFont;
//
//    public static enum FontType {
//
//        TITLE_FONT {
//            public String toString() {
//                return Constant.FONT_FSELLIOT_REG;
//            }
//        },
//
//        CONTENT_FONT {
//            public String toString() {
//                return Constant.FONT_FSELLIOT_LIGHT;
//            }
//        }
//    }
//
//    /**
//     * @return Typeface Instance with the font passed as parameter
//     */
//    public static Typeface getTypeface(Context context, String typefaceName) {
//        Typeface typeFace = null;
//
//        try {
//
//            if (typefaceName.equals(FontType.TITLE_FONT.toString())) {
//
//                if (mTitleFont == null) {
//                    mTitleFont = Typeface.createFromAsset(
//                            context.getAssets(), "fonts/" + typefaceName);
//                }
//
//                typeFace = mTitleFont;
//
//            } else if (typefaceName.equals(FontType.CONTENT_FONT.toString())) {
//
//                if (mContentFont == null) {
//                    mContentFont = Typeface.createFromAsset(
//                            context.getAssets(), "fonts/" + typefaceName);
//                }
//
//                typeFace = mContentFont;
//
//            }
//
//        } catch (Exception ex) {
//            typeFace = Typeface.DEFAULT;
//        }
//
//        return typeFace;
//    }
//
//    /**
//     * @return Typeface Instance with the font passed as parameter
//     */
//    public static Typeface getTypeface(Context context, FontType typefaceName) {
//        return getTypeface(context, typefaceName.toString());
//    }
//}