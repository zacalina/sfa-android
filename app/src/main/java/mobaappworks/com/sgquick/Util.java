package mobaappworks.com.sgquick;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.preference.PreferenceManager;
import android.telephony.CellInfo;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import mobaappworks.com.sgquick.entity.AccountEntity;
import mobaappworks.com.sgquick.entity.CoveragePlanEntity;
import mobaappworks.com.sgquick.entity.PipelineEntity;
import mobaappworks.com.sgquick.entity.ProductTypeEntity;

/**
 * Created by Migs on 9/16/2015.
 */
public class Util {

    private static Realm realm;
    private static String displaySignal;
    private static String userId;
    private static String username;
    private static String email;
    private static SharedPreferences preferences;
    private static SharedPreferences.Editor editor;

    public static String getUsername() {
        return username;
    }

    public static void setUsername(String username) {
        Util.username = username;
    }

    public static String getEmail() {
        return email;
    }

    public static void setEmail(String email) {
        Util.email = email;
    }

    public static String getUserId() {
        return userId;
    }

    public static void setUserId(String userId) {
        Util.userId = userId;
    }

    public static Long getNextKey(Class<? extends RealmObject> clazz) {
        realm = Realm.getDefaultInstance();
        try{
            Long id = realm.where(clazz).maximumInt("id") + 1;
            if(id < 0){
                id = 1L;
            }
            return id;
        }catch (NullPointerException e){
            return 1L;
        }
    }

    public static String getProductName(int id){
        if(id == 0){
            return "Product Name";
        }
        else {
            realm = Realm.getDefaultInstance();
            ProductTypeEntity result = realm.where(ProductTypeEntity.class).equalTo("id", id).findFirst();
            return result.getProductTypeName();

//            if(id == 1){
//                return "BROADBANDNOMADIC";
//            }
//            else if(id == 2){
//                return "DATA";
//            }
//            else if(id == 3){
//                return "FIXED BROADBAND";
//            }
//            else if(id == 4){
//                return "MT POSTPAID";
//            }
//            else if(id == 5){
//                return "PREPAID";
//            }
//            else if(id == 6){
//                return "SOLUTIONS";
//            }
//            else if(id == 7){
//                return "VOICE";
//            }
//            else{
//                return result.getProductTypeName();
//            }


        }
    }

    public static PipelineEntity getPipeline(Long id){
        realm = Realm.getDefaultInstance();
        PipelineEntity result = realm.where(PipelineEntity.class).equalTo("id", id).findFirst();
        return result;
    }

    public static Double getBatteryLife(Context context){
        Intent batteryIntent = context.getApplicationContext().registerReceiver(null,
                new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int rawlevel = batteryIntent.getIntExtra("level", -1);
        double scale = batteryIntent.getIntExtra("scale", -1);
        double level = -1;
        if (rawlevel >= 0 && scale > 0) {
            level = rawlevel / scale;
        }

        return level;
    }

    public static String getCarrierName(Context context){
        TelephonyManager manager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        String carrierName = manager.getNetworkOperatorName();
        List<CellInfo> allCellInfo = manager.getAllCellInfo();
        return carrierName;
    }

    public static String getIMEI(Context context){
        TelephonyManager manager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        String imei = manager.getDeviceId();
        return imei;
    }


    public static void setDisplaySignal(Integer signal){
        Integer dBm = (2 * signal) - 113;
        displaySignal = dBm.toString();
    }

    public static String getDisplaySignal(){
        return displaySignal;
    }

    public static String getAvailableInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSizeLong();
        long availableBlocks = stat.getAvailableBlocksLong();
        return formatSize(availableBlocks * blockSize);
    }

    public static String getTotalInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSizeLong();
        long totalBlocks = stat.getBlockCountLong();
        return formatSize(totalBlocks * blockSize);
    }


    public static String formatSize(long size) {
        String suffix = null;

        if (size >= 1024) {
            suffix = "KB";
            size /= 1024;
            if (size >= 1024) {
                suffix = "MB";
                size /= 1024;
            }
        }

        StringBuilder resultBuffer = new StringBuilder(Long.toString(size));

        int commaOffset = resultBuffer.length() - 3;
        while (commaOffset > 0) {
            resultBuffer.insert(commaOffset, ',');
            commaOffset -= 3;
        }

        if (suffix != null) resultBuffer.append(suffix);
        return resultBuffer.toString();
    }


    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }

    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    public static String myGetLastSyncDate(Context context){
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String myLastSyncDate = preferences.getString(Constant.LAST_SYNC_DATE, Constant.SDF_SYNC.format(new Date()));
        return myLastSyncDate;
    }

    public static String getLastSyncDate(Context context){
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String lastSyncDate = preferences.getString(Constant.LAST_SYNC_DATE, Constant.SDF_SYNC.format(new Date()));
        return lastSyncDate;
    }

    public static String drawerGetLastSyncDate(Context context){
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String lastSyncDate = preferences.getString(Constant.LAST_SYNC_DATE, Constant.SDF_SYNC.format(new Date()));
        return lastSyncDate;
    }



    public static void setLastSyncDate(){
        editor = preferences.edit();
        editor.putString(Constant.LAST_SYNC_DATE, Constant.SDF_SYNC.format(new Date()));
        editor.commit();
    }

    public static RealmResults<CoveragePlanEntity> getCoverageMonth(long from,long to){
        realm = Realm.getDefaultInstance();
        RealmQuery<CoveragePlanEntity> query = realm.where(CoveragePlanEntity.class)
                .between("scheduledDateTime",from,to);
        return query.findAll();
    }

    public static Calendar getCalendarWithValues(int month, int year, int day, int hour, int minute, int second, int millisecond){
        Calendar today = Calendar.getInstance();
        if (month != -1)
            today.set(Calendar.MONTH, month);

        if (year != -1)
            today.set(Calendar.YEAR, year);

        if (day != -1)
            today.set(Calendar.DAY_OF_MONTH, day);


        today.set(Calendar.HOUR_OF_DAY,hour);
        today.set(Calendar.MINUTE, minute);
        today.set(Calendar.SECOND, second);
        today.set(Calendar.MILLISECOND, millisecond);
        return today;
    }

    public static List<String> getAvailableTelco(Context context){
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfo
                = connectivityManager.getAllNetworkInfo();
        List<String> listNetworkInfo = new ArrayList<>();
        for(int i=0; i<networkInfo.length; i++){
            listNetworkInfo.add(networkInfo[i].toString());
        }
        return listNetworkInfo;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static boolean isMyNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) {
            try {
                URL url = new URL("http://www.google.com/");
                HttpURLConnection urlc = (HttpURLConnection)url.openConnection();
                urlc.setRequestProperty("User-Agent", "test");
                urlc.setRequestProperty("Connection", "close");
                urlc.setConnectTimeout(1000); // mTimeout is in seconds
                urlc.connect();
                if (urlc.getResponseCode() == 200) {
                    return true;
                } else {
                    return false;
                }
            } catch (IOException e) {
                Log.i("warning", "Error checking internet connection", e);
                return false;
            }
        }

        return false;

    }



    public static String getBaseUrl(Context context){
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String baseUrl = preferences.getString(Constant.BASE_URL_PREF, Constant.DEFAULT_BASE_URL);
        if("".equals(baseUrl))
            return Constant.DEFAULT_BASE_URL;
        else
            return baseUrl;
    }

    public static AccountEntity getAccountsById(Integer id){
        realm = Realm.getDefaultInstance();
        RealmQuery<AccountEntity> query = realm.where(AccountEntity.class).equalTo("accountId", id);
        return query.findFirst();
    }


}

