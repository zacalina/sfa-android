package mobaappworks.com.sgquick;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

import com.mobaappworks.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpResponseException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import mobaappworks.com.sgquick.coverage.Coverage;
import mobaappworks.com.sgquick.entity.AccountEntity;
import mobaappworks.com.sgquick.entity.BarangayEntity;
import mobaappworks.com.sgquick.entity.CheckInOutEntity;
import mobaappworks.com.sgquick.entity.CityTownEntity;
import mobaappworks.com.sgquick.entity.CompanyContactRoleEntity;
import mobaappworks.com.sgquick.entity.CoveragePlanEntity;
import mobaappworks.com.sgquick.entity.EmployeeEntity;
import mobaappworks.com.sgquick.entity.IndustryTypeEntity;
import mobaappworks.com.sgquick.entity.PipelineEntity;
import mobaappworks.com.sgquick.entity.ProductTypeEntity;
import mobaappworks.com.sgquick.entity.ProjectEntity;
import mobaappworks.com.sgquick.entity.ProvinceEntity;
import mobaappworks.com.sgquick.entity.RegionEntity;
import mobaappworks.com.sgquick.entity.RemarksEntity;
import mobaappworks.com.sgquick.entity.SavingStatus;

/**
 * Created by Migs on 9/28/2015.
 */
public class LoadingData extends Activity {

    private Integer pullCount;
    private Context context;
    private boolean willSync;
    private boolean hasErrors;
    private boolean isGettingAccounts;
    private int totalCount;
    private int currentPage;
    private int totalBrgyCount = 0;
    private int totalCityCount = 0;
    private int currentBrgyPage;
    private int currentCityPage;
    private PipelineEntity ple;
    private EmployeeEntity eme;
    SharedPreferences preferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        context = this;
        hasErrors = false;
        isGettingAccounts = false;
        currentPage = 0;
        willSync = getIntent().getBooleanExtra(Constant.SYNC, false);

        if(Util.isNetworkAvailable(this)) {
            //Util.setLastSyncDate();
            getData(willSync);
        }else{
            Toast.makeText(this, "No Connection", Toast.LENGTH_SHORT).show();
            //msgSyncComplete();
            finish();
        }

    }

    private class LoadingDataAsyncTask extends AsyncTask<String, Integer, String> {
        @Override
        protected void onProgressUpdate(Integer... progress) {
        }
        @Override
        protected void onPostExecute(String result) {

            //Handling for add pipeline cascading call
            if ("ADD_PIPELINE".equals(result)) {
                pullCount++;
                new LoadingDataAsyncTask().execute(Constant.ADD_ACCOUNT_PIPELINE_URL(context));
                new LoadingDataAsyncTask().execute(Constant.SAVE_PIPELINE_URL(context));
            }

            pullCount--;
            Log.d("KAMOTE", "kamote pullcount " + pullCount);
            String lastUpdate = preferences.getString(Constant.BARANGAY_LAST_SYNC, Constant.SDF_RESYNC.format(new Date()));

            //Getting all Cities
//            Log.d("MIGS", "current" + currentCityPage);
            if(totalCityCount !=0 && currentCityPage != totalCityCount){
                String cityUrl = Constant.CITY_URL(context) + "/" + currentCityPage + "/100/" + lastUpdate;
//                Log.d("MIGS", cityUrl);
                if(totalCityCount >= currentCityPage){
                    new LoadingDataAsyncTask().execute(cityUrl);
                    pullCount++;
                    currentCityPage++;
                }
                else{
                    pullCount--;
                }
            }

            //Getting all Barangay
//            Log.d("MIGS", "current" + currentBrgyPage);
//
//            if(totalBrgyCount != 0 && currentBrgyPage != totalBrgyCount){
//                String url = Constant.BARANGAY_URL(context) + "/" + currentBrgyPage + "/100/" + lastUpdate;
//                Log.d("MIGS",url);
//                if(totalBrgyCount >= currentBrgyPage) {
//                    new LoadingDataAsyncTask().execute(url);
//                    pullCount++;
//                    currentBrgyPage++;
//                }else{
//                    pullCount--;
//                }
//            }

            //Getting the accounts
//            Log.d("MIGS-ACCOUNT", "currentAccount" + currentBrgyPage);
            if (pullCount == 3) {
                String url = Constant.ACCOUNTS_URL(context) + "/" + currentPage + "/100/" + Util.getUserId();
                Log.d("MIGS-ACCOUNT",url);
                if(totalCount >= currentPage) {
                    if(Util.getUserId().toString().isEmpty() || Util.getUserId().toString().trim().equals("") || Util.getUserId().toString().length() < 1){
                        Log.d("Employee ID Error :", "Employee ID Lost");
                    }
                    else {
                        new LoadingDataAsyncTask().execute(url);
                        pullCount++;
                        currentPage++;
                        Log.d("PULL ", result);
                    }
                }else{
                    pullCount--;
                }
            }

            //Finishing up the download of data
            if(pullCount == 1){
                Intent intent = new Intent(context, Coverage.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);


                if(hasErrors){
                    intent.putExtra("syncStatus", "syncErrors");
                    startActivity(intent);
//                        for(int i=0; i < 3; i++) {
//                            Toast.makeText(context, "Could not complete request, please check the url defined in the settings!", Toast.LENGTH_SHORT).show();
//                        }
                }else {
                    preferences.edit().remove(Constant.BARANGAY_LAST_SYNC).commit();
                    if (willSync) {
                        Util.setLastSyncDate();
                        intent.putExtra("syncStatus", "syncComplete");
                        startActivity(intent);


                    }
                    else {
                        Util.setLastSyncDate();
                        intent.putExtra("syncStatus", "loadComplete");
                        startActivity(intent);
//                        for (int i=0; i < 3; i++) {
//                            Toast.makeText(context, "Load Complete", Toast.LENGTH_LONG).show();
//                        }
                    }
                }
                finish();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            Realm asyncRealm = Realm.getDefaultInstance();
            asyncRealm.beginTransaction();

//            if("PUT".equals(strings[2])){
//                return HttpManager.PUT(strings[0], strings[1], context);
//            }
            try {
                String lastUpdate = preferences.getString(Constant.BARANGAY_LAST_SYNC, Constant.SDF_RESYNC.format(new Date()));
                String result = "";
                if (strings[0].equals(Constant.ADD_ACCOUNT_PIPELINE_URL(context))
                        || strings[0].equals(Constant.SAVE_PIPELINE_URL(context))) {
                    result = HttpManager.POST(strings[0], strings[1], true, context);
                }
                else if(strings[0].equals(Constant.SAVE_PIPELINE_URL(context))) {
                    result = HttpManager.POST(strings[0], strings[1], true, context);
                }
                else if(strings[0].equals(Constant.UPDATE_ACCOUNT(context))){
                    result = HttpManager.PUT(strings[0], strings[1], context);
                }
                else if(strings[0].equals(Constant.ADD_CHECK_IN_OUT_URL(context))){
                    result = HttpManager.PUT(strings[0], strings[1], true, context);
                }
                else if (!strings[0].equals(Constant.RESEND)) {
                    result = HttpManager.GET(strings[0], context);
                } else if (strings[0].equals(Constant.UPDATE_ACCOUNT(context))){
                   // result = HttpManager.PUT(strings[])
                    result = HttpManager.PUT(strings[0], strings[1], true, context);
                }
//                else if(strings[0].equals(Constant.ADD_CHECK_IN_OUT_URL(context))){
//                    result = HttpManager.PUT(strings[0], strings[1], true, context);
//                }
                if(strings[0].equals(Constant.RESEND)){
                    List<SavingStatus> savingStatusesForDeletion = new ArrayList<>();
                    RealmResults<SavingStatus> savingStatuses = asyncRealm.where(SavingStatus.class).findAll();
                    for (SavingStatus ss : savingStatuses) {
                        Realm realm = Realm.getDefaultInstance();
                        JSONObject pleJsonBody = new JSONObject(ss.getBody());
                        JSONObject aceJsonBody = new JSONObject(ss.getBody());
                        String response;
                        if("POST".equals(ss.getType())) {
                            response = HttpManager.POST(ss.getUrl(), ss.getBody(), true, context);
                        }else if("PUT".equals(ss.getType())){
                            response = HttpManager.PUT(ss.getUrl(), ss.getBody(), true, context);
                        }else{
                            response = HttpManager.POST(ss.getUrl(), ss.getBody(), true, context);
                            //throw new HttpResponseException(500,"Method not supported.");
                        }
                        try {
//                            if (ss.getUrl().equals(Constant.ADD_ACCOUNT_PIPELINE_URL(context))) {
//                                PipelineEntity ple = realm.where(PipelineEntity.class).equalTo("id", pleJsonBody.getLong("id")).findFirst();
////                                //sendSavePipeline(ple, Long.valueOf(response));
////                                //if (response != null && !"".equals(response)) {
////                                    sendSavePipeline();
////                                //} else {
//////                                    asyncRealm.beginTransaction();
//////                                    asyncRealm.copyToRealmOrUpdate(ple);
//////                                    asyncRealm.commitTransaction();
////                                //}
//
//                            }
                            if (ss.getUrl().equals(Constant.UPDATE_ACCOUNT(context))){
                                AccountEntity ace = realm.where(AccountEntity.class).equalTo("id", aceJsonBody.getInt("id")).findFirst();
                                sendSaveAccountInfo(ace, Integer.valueOf(response));
                            }
                            if (!"".equals(response))
                                savingStatusesForDeletion.add(ss);

                        } catch (NumberFormatException e){
                            //DO NOTHING
                        }
                    }
                    for (SavingStatus ss : savingStatusesForDeletion) {
                        ss.removeFromRealm();
                    }
                }else if(strings[0].contains(Constant.COVERAGE_MONTH(context))) {
                    asyncRealm.createOrUpdateAllFromJson(CoveragePlanEntity.class, result);
                }else if(strings[0].equals(Constant.PRODUCT_TYPE_URL(context))){
                    asyncRealm.createOrUpdateAllFromJson(ProductTypeEntity.class, result);
                }else if(strings[0].equals(Constant.REGION_URL(context))){
                    asyncRealm.createOrUpdateAllFromJson(RegionEntity.class, result);
                }else if(strings[0].equals(Constant.PROVINCE_URL(context))){
                    asyncRealm.createOrUpdateAllFromJson(ProvinceEntity.class, result);
                }else if(strings[0].equals(Constant.COUNT_ALL_CITY_URL(context) + "/" + lastUpdate)){
                    JSONObject jsonObject = new JSONObject(result);
                    Integer cityCount = jsonObject.getInt("count");
                    totalCityCount = cityCount / 100;
                }else if(strings[0].contains(Constant.CITY_URL(context))){
                    Log.d("MIGS", result);
                    JSONObject jsonCityObject = new JSONObject(result);
                    JSONArray cityContent = (JSONArray) jsonCityObject.get("content");
                    asyncRealm.createOrUpdateAllFromJson(CityTownEntity.class, cityContent);
                }else if(strings[0].equals(Constant.INDUSTRY_URL(context))){
                    asyncRealm.createOrUpdateAllFromJson(IndustryTypeEntity.class, result);
                }else if(strings[0].equals(Constant.ROLE_URL(context))){
                    asyncRealm.createOrUpdateAllFromJson(CompanyContactRoleEntity.class, result);
                }else if(strings[0].contains(Constant.ACCOUNTS_URL(context))){
//                    JSONObject jsonObject = new JSONObject(result);
//                    JSONArray content = (JSONArray) jsonObject.get("content");
                    asyncRealm.createOrUpdateAllFromJson(AccountEntity.class, result);
                    RealmResults<AccountEntity> aes = asyncRealm.where(AccountEntity.class).findAll();

                    for (AccountEntity ae : aes) {
                        //GET PIPELINE PER ACCOUNT
                        //TODO:Throttle maybe?
                        String accountPipeline = HttpManager.GET(Constant.GET_PIPELINE(context) + "/" + ae.getAccountId() + "/0/100", context);
                        JSONArray pipelineArray = new JSONArray((accountPipeline));
                        for(int i = 0; i < pipelineArray.length(); i++){
                            JSONObject pipeline = new JSONObject(pipelineArray.get(i).toString());
                            PipelineEntity pipelineEntity = new PipelineEntity();
                            pipelineEntity.setId(pipeline.getLong("accountPipelineId"));
                            pipelineEntity.setProductTypeId(pipeline.getInt("productTypeIdFk"));
                            pipelineEntity.setStartDate(pipeline.getString("startDate"));
                            pipelineEntity.setAccountId(pipeline.getInt("accountIdFk"));
                            //GET CURRENT PIPELINE PER ACCOUNT
                            String currentPipeline = HttpManager.GET(Constant.GET_CURRENT_PIPELINE(context) + "/" + pipeline.getString("accountPipelineId"), context);
                            if(currentPipeline != null){
                                JSONObject current = new JSONObject(currentPipeline);
                                pipelineEntity.setStage(current.getString("currentStage"));
                                pipelineEntity.setRevenue(current.getInt("currentPotentialRev"));
                                pipelineEntity.setRemarks(current.getInt("currentRemarksIdFk"));
                                pipelineEntity.setNotes(current.getString("currentNotes"));
                                pipelineEntity.setProductTypeId(current.getInt("productTypeIdFk"));
                                pipelineEntity.setProjectId(current.getInt("projectIdFk"));
                            }
                            asyncRealm.copyToRealmOrUpdate(pipelineEntity);
                        }
                    }
                    //currentPage++;
                }else if(strings[0].contains(Constant.GET_PROJECT_URL(context))){
                    asyncRealm.createOrUpdateAllFromJson(ProjectEntity.class, result);
                }else if(strings[0].contains(Constant.GET_REMARKS_URL(context))) {
                    asyncRealm.createOrUpdateAllFromJson(RemarksEntity.class, result);
                }else if(strings[0].contains(Constant.GET_ACCOUNT_COUNT(context))){
                    JSONObject jsonObject = new JSONObject(result);
                    Integer accountCount = jsonObject.getInt("count");

                    totalCount = accountCount / 100;

//                    if(accountCount % 100 > 0){
//                        totalCount++;
//                    }
                }
//                else if(strings[0].equals(Constant.COUNT_ALL_BARANGAY_URL(context) + "/" + lastUpdate)){
//                    JSONObject jsonObject = new JSONObject(result);
//                    Integer brgyCount = jsonObject.getInt("count");
//                    totalBrgyCount = brgyCount / 100;
//                }

//                else if(strings[0].contains(Constant.BARANGAY_URL(context))){
//                    Log.d("MIGS", result);
//                    JSONObject jsonBrgyObject = new JSONObject(result);
//                    JSONArray brgyContent = (JSONArray) jsonBrgyObject.get("content");
//                    asyncRealm.createOrUpdateAllFromJson(BarangayEntity.class, brgyContent);
//                }

            } catch (IOException e) {
                if(!strings[0].equals(Constant.RESEND)){
                    hasErrors = true;
                }
                e.printStackTrace();
                Log.d("MIGS",e.getMessage());
            } catch (JSONException e) {
                hasErrors = true;
                e.printStackTrace();
                Log.d("MIGS",e.getMessage());
            } catch (Exception e){
                if(!strings[0].equals(Constant.RESEND)){
                    hasErrors = true;
                }
                e.printStackTrace();
                Log.d("MIGS", e.getMessage());
            }
            asyncRealm.commitTransaction();
            return "";
        }
    }

    public void getData(Boolean willSync){
        pullCount = 12;

        if(willSync){
            pullCount += 1;
            //RESEND ALL FAILED POST TRANSACTIONS



            //end all info

            new LoadingDataAsyncTask().execute(Constant.RESEND);
        }

        //start upload all info
        Realm asyncRealm = Realm.getDefaultInstance();
        //start upload all info

        String myLastSyncDate  = Util.myGetLastSyncDate(context);
        SimpleDateFormat kamote_sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sssZ");

        //pipeline entity

        try {
            Date mypld = kamote_sdf.parse(myLastSyncDate);
            RealmResults<PipelineEntity> ple = asyncRealm.where(PipelineEntity.class).greaterThan("plCheckoutDate", mypld).findAll();

            for (PipelineEntity pl : ple) {
                JSONObject pipelineDetailsJson = new JSONObject();
                try {
                    pipelineDetailsJson.put("id", pl.getId());
                    pipelineDetailsJson.put("accountPipelineId", pl.getId());
                    pipelineDetailsJson.put("accountIdFk", pl.getAccountId());
                    pipelineDetailsJson.put("productTypeIdFk", pl.getProductTypeId());
                    pipelineDetailsJson.put("startDate", pl.getStartDate());
                    pipelineDetailsJson.put("endDate", "null");
                    pullCount++;
                    new LoadingDataAsyncTask().execute(Constant.ADD_ACCOUNT_PIPELINE_URL(context), pipelineDetailsJson.toString(), "POST");

                    JSONObject savePipeline = new JSONObject();
                    savePipeline.put("id", pl.getId());
                    savePipeline.put("accountPipelineIdFk", pl.getId());
                    savePipeline.put("stage", pl.getStage());
                    savePipeline.put("potentialRevenue", pl.getRevenue());
                    savePipeline.put("notes", pl.getNotes());
                    savePipeline.put("updateDate", pl.getUpdateDate());
                    savePipeline.put("pipelineRemarksIdFk", pl.getRemarks());
                    savePipeline.put("projectIdFk", pl.getProjectId());
                    new LoadingDataAsyncTask().execute(Constant.SAVE_PIPELINE_URL(context), savePipeline.toString(), "POST");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }catch (ParseException ex) {
            Log.d("Error : ","error date check in out");
        }

        try {
            Date mypld = kamote_sdf.parse(myLastSyncDate);
        RealmResults<AccountEntity> aes = asyncRealm.where(AccountEntity.class).greaterThan("acCheckoutDate", mypld).findAll();

        for (AccountEntity ae : aes) {
            JSONObject accountDetailsJson = new JSONObject();
            try {
                accountDetailsJson.put("id", ae.getId());
                accountDetailsJson.put("accountId", ae.getAccountId());
                accountDetailsJson.put("accountName", ae.getAccountName());
                accountDetailsJson.put("registeredAccountName", ae.getRegisteredAccountName());
                accountDetailsJson.put("employeeId", ae.getEmployeeId());
                accountDetailsJson.put("accountAddress", ae.getAccountAddress());
                accountDetailsJson.put("unitNumber", ae.getUnitNumber());
                accountDetailsJson.put("floorNumber", ae.getFloorNumber());
                accountDetailsJson.put("lowestFloorOccupied", ae.getLowestFloorOccupied());
                accountDetailsJson.put("highestFloorOccupied", ae.getHighestFloorOccupied());
                accountDetailsJson.put("buildingName", ae.getBuildingName());
                accountDetailsJson.put("streetNumber", ae.getStreetNumber());
                accountDetailsJson.put("streetName", ae.getStreetName());
                accountDetailsJson.put("barangayIdFk", ae.getBarangayIdFk());
                accountDetailsJson.put("cityTownIdFk", ae.getCityTownIdFk());
                accountDetailsJson.put("provinceIdFk", ae.getProvinceIdFk());
                accountDetailsJson.put("regionIdFk", ae.getRegionIdFk());
                accountDetailsJson.put("salesRegionIdFk", ae.getSalesRegionIdFk());
                accountDetailsJson.put("secRanking", ae.getSecRanking());
                accountDetailsJson.put("tier", ae.getTier());
                accountDetailsJson.put("gbuTagidFk", ae.getGbuTagidFk());
                accountDetailsJson.put("industryTypeIdFk", ae.getIndustryTypeIdFk());
                accountDetailsJson.put("accountClassIdFk", ae.getAccountClassIdFk());
                accountDetailsJson.put("numberOfEmployees", ae.getNumberOfEmployees());
                accountDetailsJson.put("segmentIdFk", ae.getSegmentIdFk());
                accountDetailsJson.put("anniversary", ae.getAnniversary());
                accountDetailsJson.put("originalName", ae.getOriginalName());
                accountDetailsJson.put("cleanedName", ae.getCleanedName());
                accountDetailsJson.put("longitude", ae.getLongitude());
                accountDetailsJson.put("latitude", ae.getLatitude());
                accountDetailsJson.put("isTapped", ae.isTapped());
                accountDetailsJson.put("contactNumber", ae.getContactNumber());
                accountDetailsJson.put("zipCode", ae.getZipCode());

                new LoadingDataAsyncTask().execute(Constant.UPDATE_ACCOUNT(context), accountDetailsJson.toString(), "PUT");
                pullCount++;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        }catch (ParseException ex) {
            Log.d("Error : ","error date check in out");
        }

        //Toast.makeText(getApplicationContext(), myLastSyncDate, Toast.LENGTH_SHORT).show();

        try {
            //Date myd = sdf.parse("20130526160000");
            Date myd = kamote_sdf.parse(myLastSyncDate);

            RealmResults<CheckInOutEntity> chk = asyncRealm.where(CheckInOutEntity.class).greaterThan("checkoutDate", myd).findAll();
            for (CheckInOutEntity ck : chk) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("id", "0");
                    jsonObject.put("accountIdFk", ck.getAccountId());
                    jsonObject.put("longitude", ck.getCheckinLng());
                    jsonObject.put("latitude", ck.getCheckinLat());
                    jsonObject.put("checkInTime", Constant.SDF_API.format(ck.getCheckinDate()));
                    jsonObject.put("checkOutTime", Constant.SDF_API.format(ck.getCheckoutDate()));
                    jsonObject.put("remarks", ck.getCheckinNote() + ck.getCheckoutNote());

                    new LoadingDataAsyncTask().execute(Constant.ADD_CHECK_IN_OUT_URL(context), jsonObject.toString(), "PUT");
                    pullCount++;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
     catch (ParseException ex) {
        Log.d("Error : ","error date check in out");
    }

        String lastUpdate = preferences.getString(Constant.BARANGAY_LAST_SYNC, Constant.SDF_RESYNC.format(new Date()));
        //new LoadingDataAsyncTask().execute(Constant.RESEND);
        if(Util.getUserId().toString().isEmpty() || Util.getUserId().toString().trim().equals("") || Util.getUserId().toString().length() < 1){
            Log.d("Employee ID Error :", "Employee ID Lost");
        }
        else {
            new LoadingDataAsyncTask().execute(Constant.COVERAGE_MONTH(context) + "/" + Util.getUserId());
        }
        new LoadingDataAsyncTask().execute(Constant.PRODUCT_TYPE_URL(context));
        new LoadingDataAsyncTask().execute(Constant.REGION_URL(context));
        new LoadingDataAsyncTask().execute(Constant.PROVINCE_URL(context));
        //new LoadingDataAsyncTask().execute(Constant.CITY_URL(context));
//        new LoadingDataAsyncTask().execute(Constant.COUNT_ALL_BARANGAY_URL(context) + "/" + lastUpdate);
        new LoadingDataAsyncTask().execute(Constant.COUNT_ALL_CITY_URL(context) + "/" + lastUpdate);
        new LoadingDataAsyncTask().execute(Constant.INDUSTRY_URL(context));
        new LoadingDataAsyncTask().execute(Constant.ROLE_URL(context));

        new LoadingDataAsyncTask().execute(Constant.GET_PROJECT_URL(context));
        new LoadingDataAsyncTask().execute(Constant.GET_REMARKS_URL(context));
        if(Util.getUserId().toString().isEmpty() || Util.getUserId().toString().trim().equals("") || Util.getUserId().toString().length() < 1){
            Log.d("Employee ID Error :", "Employee ID Lost");
        }
        else {
            new LoadingDataAsyncTask().execute(Constant.GET_ACCOUNT_BY_EMPLOYEE_COUNT(context) + "/" + Util.getUserId());
        }

    }

//    Realm myRealm = Realm.getDefaultInstance();
//    public void sendSavePipeline(PipelineEntity ple2, Long id) throws JSONException {
//        ple2.setId(id);
//        sendSavePipeline(ple2);
//
//    }

    public void sendSaveAccountInfo(AccountEntity ace2, int id) throws JSONException {
        ace2.setId(id);
        sendSaveAccountInfo(ace2);
    }


//    public void sendSavePipeline(PipelineEntity ple) throws JSONException {
//        JSONObject savePipeline = new JSONObject();
//        savePipeline.put("id", 0);
//        savePipeline.put("accountPipelineIdFk", ple.getId());
//        savePipeline.put("stage", ple.getStage());
//        savePipeline.put("potentialRevenue", ple.getRevenue());
//        savePipeline.put("notes", ple.getNotes());
//        savePipeline.put("updateDate", ple.getUpdateDate());
//        savePipeline.put("pipelineRemarksIdFk", ple.getRemarks());
//        savePipeline.put("projectIdFk", ple.getProjectId());
//        pullCount++;
//        new LoadingDataAsyncTask().execute(Constant.SAVE_PIPELINE_URL(context), savePipeline.toString());
//    }

    public void sendSaveAccountInfo(AccountEntity ace) throws JSONException{
        JSONObject saveAccountInfo = new JSONObject();
        saveAccountInfo.put("id", 0);
        saveAccountInfo.put("accountId", ace.getAccountId());
        saveAccountInfo.put("accountName", ace.getAccountName());
        saveAccountInfo.put("registeredAccountName", ace.getRegisteredAccountName());
        saveAccountInfo.put("employeeId", ace.getEmployeeId());
        saveAccountInfo.put("accountAddress", ace.getAccountAddress());
        saveAccountInfo.put("unitNumber", ace.getUnitNumber());
        saveAccountInfo.put("floorNumber", ace.getFloorNumber());
        saveAccountInfo.put("lowestFloorOccupied", ace.getLowestFloorOccupied());
        saveAccountInfo.put("highestFloorOccupied", ace.getHighestFloorOccupied());
        saveAccountInfo.put("buildingName", ace.getBuildingName());
        saveAccountInfo.put("streetNumber", ace.getStreetNumber());
        saveAccountInfo.put("streetName", ace.getStreetName());
        saveAccountInfo.put("barangayIdFk", ace.getBarangayIdFk());
        saveAccountInfo.put("cityTownIdFk", ace.getCityTownIdFk());
        saveAccountInfo.put("provinceIdFk", ace.getProvinceIdFk());
        saveAccountInfo.put("regionIdFk", ace.getRegionIdFk());
        saveAccountInfo.put("salesRegionIdFk", ace.getSalesRegionIdFk());
        saveAccountInfo.put("secRanking", ace.getSecRanking());
        saveAccountInfo.put("tier", ace.getTier());
        saveAccountInfo.put("gbuTagidFk", ace.getGbuTagidFk());
        saveAccountInfo.put("industryTypeIdFk", ace.getIndustryTypeIdFk());
        saveAccountInfo.put("accountClassIdFk", ace.getAccountClassIdFk());
        saveAccountInfo.put("numberOfEmployees", ace.getNumberOfEmployees());
        saveAccountInfo.put("segmentIdFk", ace.getSegmentIdFk());
        saveAccountInfo.put("anniversary", ace.getAnniversary());
        saveAccountInfo.put("originalName", ace.getOriginalName());
        saveAccountInfo.put("cleanedName", ace.getCleanedName());
        saveAccountInfo.put("longitude", ace.getLongitude());
        saveAccountInfo.put("latitude", ace.getLatitude());
        saveAccountInfo.put("isTapped", ace.isTapped());
        saveAccountInfo.put("contactNumber", ace.getContactNumber());
        saveAccountInfo.put("zipCode", ace.getZipCode());
        pullCount++;
        new LoadingDataAsyncTask().execute(Constant.UPDATE_ACCOUNT(context), saveAccountInfo.toString());
    }


    @Override
    public void onBackPressed() {
        //Do Nothing
    }

    public void msgSyncComplete(){
        //Toast.makeText(getApplicationContext(), "Amount Cannot be NULL or ZERO", Toast.LENGTH_SHORT).show();
        //start dialog
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        //alertDialog.setTitle("Alert");
        alertDialog.setMessage("Sync Complete");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();


    }


}
