package mobaappworks.com.sgquick.accounts;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.mobaappworks.R;

import java.util.Date;

import io.realm.Realm;
import mobaappworks.com.sgquick.Constant;
import mobaappworks.com.sgquick.FontChangeCrawler;
import mobaappworks.com.sgquick.entity.AccountEntity;
import mobaappworks.com.sgquick.entity.CheckOutEntity;

/**
 * Created by ldeguzman on 9/9/15.
 */
public class AccountsCheckOutPopUp extends Activity implements View.OnClickListener{

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private Realm realm;
    private int id;
    private TextView tv;
    private Context context;
    private Button btn_checkout;

    @Override
    public void setContentView(View view)
    {
        super.setContentView(view);
        FontChangeCrawler fontChanger = new FontChangeCrawler(this.context.getAssets(), Constant.FONT_FSELLIOT_REG);
        fontChanger.replaceFonts((ViewGroup)this.findViewById(android.R.id.content));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accounts_info_checkout);
        realm = Realm.getDefaultInstance();

        tv = (TextView) findViewById(R.id.tv_close);
        tv.setOnClickListener(this);
        btn_checkout = (Button) findViewById(R.id.btn_check_out);
        btn_checkout.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.tv_close:
                finish();
                break;

            case R.id.btn_check_out:
                Float pid = preferences.getFloat(Constant.CPU_CHECKOUT, 0);
                Log.d("[AAP]", pid.toString());
                editor.putFloat(Constant.CPU_CHECKOUT, pid + 1);
                editor.apply();

                //TODO: Get actual lng lat of the device
                CheckOutEntity checkOutEntitye = new CheckOutEntity(pid.intValue(),getAccount(id),1L,1L,new Date().toString());
                realm.beginTransaction();
                realm.copyToRealmOrUpdate(checkOutEntitye);
                realm.commitTransaction();
                finish();
                break;

        }
    }

    public AccountEntity getAccount(int id){

        AccountEntity result = realm.where(AccountEntity.class)
                .equalTo("id", id)
                .findFirst();

        return result;
    }
}
