package mobaappworks.com.sgquick.accounts;

/**
 * Created by ldeguzman on 9/8/15.
 */
public class AccountsPipelineDTO {

    private String stage;
    private Integer amount;
    private String product;

    public AccountsPipelineDTO(String stage, Integer amount, String product) {
        this.stage = stage;
        this.amount = amount;
        this.product = product;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }
}
