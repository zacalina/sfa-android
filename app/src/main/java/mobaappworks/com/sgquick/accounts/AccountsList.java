package mobaappworks.com.sgquick.accounts;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TypefaceSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mobaappworks.R;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import mobaappworks.com.sgquick.Constant;
import mobaappworks.com.sgquick.CustomDrawerAdapter;
import mobaappworks.com.sgquick.HttpManager;
import mobaappworks.com.sgquick.Util;
import mobaappworks.com.sgquick.entity.AccountEntity;
import mobaappworks.com.sgquick.entity.CityTownEntity;
import mobaappworks.com.sgquick.entity.CompanyContactEntity;
import mobaappworks.com.sgquick.entity.CompanyContactRoleEntity;
import mobaappworks.com.sgquick.entity.IndustryTypeEntity;
import mobaappworks.com.sgquick.entity.ProductTypeEntity;
import mobaappworks.com.sgquick.entity.ProvinceEntity;
import mobaappworks.com.sgquick.entity.RegionEntity;

public class AccountsList extends AppCompatActivity {

    private ActionBarDrawerToggle mDrawerToggle;
    private String USER;
    private String EMAIL;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private DrawerLayout Drawer;
    private String mTitle = "";
    private Realm realm;
    private Context context;
    private RealmResults<AccountEntity> accountEntities;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private ListView listView;
    private ProgressBar pgAclist;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accounts_list);

        context = this;
        realm = Realm.getDefaultInstance();
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = preferences.edit();

        // actionbar styling
        SpannableString s = new SpannableString("Account List");
        s.setSpan(new TypefaceSpan(Constant.FONT_FSELLIOT_REG), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        //TODO: add logged in username and email from login
        USER = Util.getUsername();
        EMAIL = Util.getEmail();

        //TODO: Get title from selected menu
        mTitle = "ACCOUNT LIST";
        getSupportActionBar().setTitle(mTitle);


        mRecyclerView = (RecyclerView) findViewById(R.id.RecyclerView);
        mRecyclerView.setHasFixedSize(true);

        mAdapter = new CustomDrawerAdapter(Constant.TITLES, Constant.ICONS, USER, EMAIL);
        mRecyclerView.setAdapter(mAdapter);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        listView = (ListView) findViewById(R.id.listview);
        listView.setOnItemClickListener(onClickListener());
        Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout);
        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                Drawer,
                R.string.drawer_open,
                R.string.drawer_close) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(drawerView.getApplicationWindowToken(), 0);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(drawerView.getApplicationWindowToken(), 0);
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(drawerView.getApplicationWindowToken(), 0);
            }
        };

        Drawer.setDrawerListener(mDrawerToggle);
        mDrawerToggle.setDrawerIndicatorEnabled(true);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        handleIntent(getIntent());
        listView.setAdapter(new AccountsListAdapter(context, accountEntities, true));

        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    public AdapterView.OnItemClickListener onClickListener(){
        return new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Integer tag = (Integer) view.getTag();
                TextView tv = (TextView) view.findViewById(R.id.firstLine);
                String name = tv.getText().toString();
                Log.d("[Account on Click]", tag.toString());

                Intent intent = new Intent(context,Accounts.class);
                intent.putExtra(Constant.ID, tag);
                intent.putExtra(Constant.ACCOUNT_NAME, name);
                intent.putExtra(Constant.IS_ACCOUNT_LIST, true);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(intent);
            }

        };
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public RealmResults<AccountEntity> getAccounts(){
        return realm.allObjects(AccountEntity.class);
    }

    public RealmResults<AccountEntity> getAccounts(String name){
        RealmQuery<AccountEntity> query = realm.where(AccountEntity.class)
                .contains("accountName", name, RealmQuery.CASE_INSENSITIVE);
        return query.findAll();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        // Inflate menu to add items to action bar if it is present.
        inflater.inflate(R.menu.common_menu, menu);
        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.menu_search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            accountEntities = getAccounts(query);
        }else{
            accountEntities = getAccounts();
        }
        accountEntities.sort("accountName");
    }

}
