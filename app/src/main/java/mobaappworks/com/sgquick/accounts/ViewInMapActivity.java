package mobaappworks.com.sgquick.accounts;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mobaappworks.R;

import java.text.SimpleDateFormat;
import java.util.Map;

import mobaappworks.com.sgquick.Constant;
import mobaappworks.com.sgquick.Util;
import mobaappworks.com.sgquick.entity.AccountEntity;

/**
 * Created by Migs on 10/17/2015.
 */


public class ViewInMapActivity extends FragmentActivity implements View.OnClickListener, LocationListener {
    private GoogleMap mMap;
    private Button btnNavigate;
    private int id;
    private AccountEntity account;
    private Location location;
    private LocationManager locationManager;
    private Double latitude;
    private Double longitude;
    private Marker marker;
    private Context context;
    private Boolean isGPSEnabled;
    private Boolean isNetworkEnabled;
    private Map<String, Integer> ids;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accounts_view_in_map);
        id = getIntent().getIntExtra(Constant.ID, -1);
        account = Util.getAccountsById(id);
        context = this;

        // navigation button styling
        btnNavigate = (Button) findViewById(R.id.navigate);
        Typeface font = Typeface.createFromAsset(getAssets(), Constant.FONT_FSELLIOT_REG);
        btnNavigate.setTypeface(font);
        btnNavigate.setBackgroundColor(Color.parseColor("#005ab0"));
        btnNavigate.setTextColor(Color.parseColor("#ffffff"));

        btnNavigate.setOnClickListener(this);
        getLocation();

        setUpMapIfNeeded();
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            SupportMapFragment sfm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_view);
            mMap = sfm.getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {

        try {
            LatLng location = new LatLng(Double.valueOf(account.getLatitude()), Double.valueOf(account.getLongitude()));

            mMap.setOnMyLocationChangeListener(myLocationChangeListener);

            mMap.addMarker(new MarkerOptions()
                    .position(location)
                    .title(account.getAccountName())
                    .snippet(account.getAccountAddress()));

            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 16.0f));

            marker = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(latitude, longitude))
//                .snippet("Lat:" + location.getLatitude() + "Lng:"+ location.getLongitude())
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
//                    .title("ME")
            );
            mMap.setOnInfoWindowClickListener(
                    new GoogleMap.OnInfoWindowClickListener() {
                        @Override
                        public void onInfoWindowClick(Marker marker) {
                            Intent nextScreen = new Intent(context, AccountsAddPipeline.class);
                            nextScreen.putExtra(Constant.ID, ids.get(marker.getTitle()));
//                        nextScreen.putExtra("eventId", "" + eventId);
                            startActivity(nextScreen);
                        }
                    }
            );

        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(ViewInMapActivity.this, "Unable to view in map. No location available.", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.navigate:
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr="
                                + latitude
                                + ","
                                + longitude
                                + "&daddr="
                                + account.getLatitude()
                                + ","
                                + account.getLongitude()));

                startActivity(intent);
                break;

        }

    }
    private GoogleMap.OnMyLocationChangeListener myLocationChangeListener = new GoogleMap.OnMyLocationChangeListener() {
        @Override
        public void onMyLocationChange(Location location) {
            LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());
            marker = mMap.addMarker(new MarkerOptions().position(loc));
        }
    };

    public Location getLocation() {
        try {
            if (latitude == null) {
                latitude = 0D;
            }

            if (longitude == null) {
                longitude = 0D;
            }

            locationManager = (LocationManager) context
                    .getSystemService(LOCATION_SERVICE);

            // getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
            } else {
//                this.canGetLocation = true;
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            5000,
                            10,
                            this);
                    Log.d("Network", "Network Enabled");
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                5000,
                                10,
                                this);
                        Log.d("GPS", "GPS Enabled");
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return location;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        locationManager.removeUpdates(this);
    }

    @Override
    public void onLocationChanged(Location location) {
        String lng = "Longitude: " + location.getLongitude();
        longitude = location.getLongitude();
        Log.v("MAP VIEW", lng);
        String lat = "Latitude: " + location.getLatitude();
        latitude = location.getLatitude();
        Log.v("MAP VIEW", lat);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
