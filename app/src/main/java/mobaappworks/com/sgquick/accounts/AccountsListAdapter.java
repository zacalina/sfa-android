package mobaappworks.com.sgquick.accounts;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobaappworks.R;

import java.util.Calendar;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmBaseAdapter;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import mobaappworks.com.sgquick.Constant;
import mobaappworks.com.sgquick.Util;
import mobaappworks.com.sgquick.entity.AccountEntity;
import mobaappworks.com.sgquick.entity.CoveragePlanEntity;

/**
 * Created by ldeguzman on 9/5/15.
 */
public class AccountsListAdapter extends RealmBaseAdapter<AccountEntity> {

    Realm realm;
    boolean isAccountList;

    public AccountsListAdapter(Context context, RealmResults<AccountEntity> results, boolean isAccountList) {
        super(context, results, true);
        this.isAccountList = isAccountList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyViewHolder viewHolder = new MyViewHolder();
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.activity_accounts_list_view, parent, false);
        AccountEntity accountEntity = realmResults.get(position);

        // styling
        Typeface font_item = Typeface.createFromAsset(this.context.getAssets(), Constant.FONT_FSELLIOT_REG);
        viewHolder.name = (TextView) rowView.findViewById(R.id.firstLine);
        viewHolder.name.setTypeface(font_item);
        viewHolder.name.setText(accountEntity.getAccountName());

        String lastVisit = "";

        if(!"".equals(accountEntity.getDateOfLastVisit())){
            lastVisit = Constant.SDF_DATE.format(new Date(Long.valueOf(accountEntity.getDateOfLastVisit())));
        }

        Calendar today = Calendar.getInstance();
        String todayFormatted = Constant.SDF_DATE.format(today.getTime());

        if (!"".equals(lastVisit) && todayFormatted.equals(lastVisit)){
            viewHolder.name.setTextColor(Color.parseColor("#d9534f"));
        }

        Typeface font_desc = Typeface.createFromAsset(this.context.getAssets(), Constant.FONT_FSELLIOT_LIGHT);
        viewHolder.description = (TextView) rowView.findViewById(R.id.secondLine);
        viewHolder.description.setTypeface(font_desc);
        viewHolder.description.setText(accountEntity.getAccountAddress());

        realm = Realm.getDefaultInstance();
        viewHolder.appointment = (TextView) rowView.findViewById(R.id.thirdLine);
        viewHolder.appointment.setTypeface(font_desc);

        Calendar calendarWithValues = Util.getCalendarWithValues(
                today.get(Calendar.MONTH),
                today.get(Calendar.YEAR),
                today.get(Calendar.DAY_OF_MONTH),
                0,
                0,
                0,
                0);


        CoveragePlanEntity coveragePlanEntity;

        //1447002508433
        CoveragePlanEntity coveragePlanToday = realm.where(CoveragePlanEntity.class)
                .equalTo("accountIdFk", String.valueOf(accountEntity.getAccountId()))
                .equalTo("scheduledDateTime", calendarWithValues.getTimeInMillis())
                .findFirst();

        if(coveragePlanToday == null){
            RealmResults<CoveragePlanEntity> coveragePlanNearFuture = realm.where(CoveragePlanEntity.class)
                    .equalTo("accountIdFk", String.valueOf(accountEntity.getAccountId()))
                    .greaterThan("scheduledDateTime", calendarWithValues.getTimeInMillis())
                    .findAllSorted("scheduledDateTime", true);


            if(coveragePlanNearFuture.size() == 0){
                RealmResults<CoveragePlanEntity> coveragePlanPast = realm.where(CoveragePlanEntity.class)
                        .equalTo("accountIdFk", String.valueOf(accountEntity.getAccountId()))
                        .lessThan("scheduledDateTime", calendarWithValues.getTimeInMillis())
                        .findAllSorted("scheduledDateTime", false);

                if(coveragePlanPast.size() == 0){
                    coveragePlanEntity = null;
                }else{
                    coveragePlanEntity = coveragePlanPast.get(0);
                }

            }else{
                coveragePlanEntity = coveragePlanNearFuture.get(0);
            }
        }else{
            coveragePlanEntity = coveragePlanToday;
        }

        if(isAccountList) {
            viewHolder.appointment.setVisibility(View.GONE);
        }else if(coveragePlanEntity != null){
            viewHolder.appointment.setText(Constant.SDF_DATE.format(new Date(coveragePlanEntity.getScheduledDateTime())));
        }else{
            viewHolder.appointment.setText("No Appointment Set.");
        }

        rowView.setTag(accountEntity.getAccountId());

//        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
//        // change the icon for Windows and iPhone
//        String s = values[position];
//        if (s.startsWith("iPhone")) {
//            imageView.setImageResource(R.drawable.no);
//        } else {
//            imageView.setImageResource(R.drawable.ok);
//        }
        return rowView;
    }

    public class MyViewHolder{
        TextView name;
        TextView description;
        TextView appointment;
        ImageView imageView;
    }
}
