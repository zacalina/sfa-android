package mobaappworks.com.sgquick.accounts;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;

import com.mobaappworks.R;

import java.text.SimpleDateFormat;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import mobaappworks.com.sgquick.Constant;
import mobaappworks.com.sgquick.FontChangeCrawler;
import mobaappworks.com.sgquick.entity.CheckInOutEntity;
import mobaappworks.com.sgquick.entity.PipelineEntity;

public class AccountsFragmentPipeline extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener {
    private View rootView;
    private ImageButton btnAddPipeline;
    private ImageButton btnCheckIn;
    private Context context;
    private ListView lv_pipeline;
    private RealmResults<PipelineEntity> pipelineEntities;
    private Realm realm;
    private int id;
    private Boolean isAccountList;

    private Uri checkin = Uri.parse("android.resource://acqui.mobaappworks.com.sgquick/" + R.drawable.fragment_checkin);
    private Uri checkout = Uri.parse("android.resource://acqui.mobaappworks.com.sgquick/" + R.drawable.fragment_checkout);
    private Uri addPipeline = Uri.parse("android.resource://acqui.mobaappworks.com.sgquick/" + R.drawable.fragment_addpipe);

//    private Uri checkin = Uri.parse("android.resource://training.mobaappworks.com.sgquick/" + R.drawable.fragment_checkin);
//    private Uri checkout = Uri.parse("android.resource://training.mobaappworks.com.sgquick/" + R.drawable.fragment_checkout);
//    private Uri addPipeline = Uri.parse("android.resource://training.mobaappworks.com.sgquick/" + R.drawable.fragment_addpipe);

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        FontChangeCrawler fontChanger = new FontChangeCrawler(this.context.getAssets(), Constant.FONT_FSELLIOT_REG);
        fontChanger.replaceFonts((ViewGroup) this.getView());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_accounts_pipeline, container, false);
        Bundle args = getArguments();
        id = args.getInt(Constant.ID);
        isAccountList = args.getBoolean(Constant.IS_ACCOUNT_LIST);


        realm = Realm.getDefaultInstance();
        context = rootView.getContext();

        btnAddPipeline = (ImageButton) rootView.findViewById(R.id.btn_add_pipeline);
        btnAddPipeline.setBackgroundColor(Color.parseColor("#333333"));
        btnAddPipeline.setOnClickListener(this);
        btnAddPipeline.setImageURI(addPipeline);

        btnCheckIn = (ImageButton) rootView.findViewById(R.id.btn_check_in);
        btnCheckIn.setBackgroundColor(Color.parseColor("#333333"));
        btnCheckIn.setImageURI(checkin);
        btnCheckIn.setOnClickListener(this);

//        if(isCheckedIn())
//            btnCheckIn.setText("Check-Out");
//        else
//            btnCheckIn.setText("Check-In");

        if(isCheckedIn())
            btnCheckIn.setImageURI(checkout);
        else
            btnCheckIn.setImageURI(checkin);


        pipelineEntities = getPipeline(id);

        lv_pipeline = (ListView) rootView.findViewById(R.id.pipeline_list);
        lv_pipeline.setOnItemClickListener(this);
        lv_pipeline.setAdapter(new AccountsFragmentPipelineAdapter(context, pipelineEntities));

        if (isAccountList){
            accountListElementControls();
        }

        Log.d("[ADD PIPE LINE]", "onCreate");
        return rootView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_add_pipeline:
                Intent intent = new Intent(context, AccountsAddPipeline.class);
                intent.putExtra(Constant.ID, id);
                startActivity(intent);
                break;

            case R.id.btn_check_in:
                Intent checkinIntent = new Intent(context, AccountsCheckInPopUp.class);
                checkinIntent.putExtra(Constant.ID, id);
                startActivity(checkinIntent);
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("ON RESUME", "PIPELINE");

        pipelineEntities = getPipeline(id);
        lv_pipeline.setAdapter(new AccountsFragmentPipelineAdapter(context, pipelineEntities));
//
//        if(isCheckedIn())
//            btnCheckIn.setText("Check-Out");
//        else
//            btnCheckIn.setText("Check-In");

        if(isCheckedIn())
            btnCheckIn.setImageURI(checkout);
        else
            btnCheckIn.setImageURI(checkin);

    }

    public RealmResults<PipelineEntity> getPipeline(int id){
        RealmQuery<PipelineEntity> query = realm.where(PipelineEntity.class)
                .equalTo("accountId", id);
        return query.findAll();
    }

    public boolean isCheckedIn(){
        RealmResults<CheckInOutEntity> cioes = realm.where(CheckInOutEntity.class)
                .equalTo("accountId", String.valueOf(id))
                .equalTo("checkoutLng", "")
                .equalTo("checkoutLat", "")
                .findAll();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String today = sdf.format(new Date());
        for (CheckInOutEntity  checkInOutEntity: cioes) {
            String checkin = sdf.format(checkInOutEntity.getCheckinDate());
            if (checkin.equals(today))
                return true;
        }
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        Long tag = (Long) view.getTag();
        Intent intent = new Intent(context, AccountsAddPipeline.class);
        intent.putExtra(Constant.ID, id);
        intent.putExtra(Constant.PIPELINE_ID, tag);
        startActivity(intent);

    }

    private void accountListElementControls(){
        btnCheckIn.setVisibility(View.VISIBLE);
        btnAddPipeline.setVisibility(View.VISIBLE);
    }
}