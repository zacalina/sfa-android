package mobaappworks.com.sgquick.accounts;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mobaappworks.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import io.realm.Realm;
import io.realm.RealmResults;
import mobaappworks.com.sgquick.Constant;
import mobaappworks.com.sgquick.FontChangeCrawler;
import mobaappworks.com.sgquick.HttpManager;
import mobaappworks.com.sgquick.Util;
import mobaappworks.com.sgquick.entity.PipelineEntity;
import mobaappworks.com.sgquick.entity.ProductTypeEntity;
import mobaappworks.com.sgquick.entity.ProjectEntity;
import mobaappworks.com.sgquick.entity.RemarksEntity;

public class AccountsAddPipeline extends Activity implements View.OnClickListener {

    private Button btnSave;
    private Button btnCancel;
    private TextView title;
    private TextView tvProduct;
    private TextView tvStage;
    private TextView tvProjects;
    private TextView tvRemarks;
    private TextView tvAmount;
    private EditText etAmount;
    private EditText etNotes;
    private Spinner spStage;
    private Spinner spProjects;
    private Spinner spRemarks;
    private Spinner spProducts;
    private Realm realm;
    private Context context;
    private Integer id;
    private Long pipelineId;
    private Map<String, ProductTypeEntity> productMap;
    private Map<String, ProjectEntity> projectMap;
    private Map<String, RemarksEntity> remarkMap;
    private PipelineEntity ple;
    private RemarksEntity rme;
    private int prodCtr;
    private int projCtr;
    private int remCtr;
    private boolean forUpdate;
    private int count;

    private List<ProjectEntity> projObjList = new ArrayList<ProjectEntity>();
    private List<RemarksEntity> remObjList = new ArrayList<RemarksEntity>();

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
        FontChangeCrawler fontChanger = new FontChangeCrawler(this.getAssets(), Constant.FONT_FSELLIOT_REG);
        fontChanger.replaceFonts((ViewGroup) this.findViewById(android.R.id.content));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accounts_add_pipeline);

        Typeface font = Typeface.createFromAsset(getAssets(), Constant.FONT_FSELLIOT_REG);

        id = getIntent().getIntExtra(Constant.ID, 0);
        pipelineId = getIntent().getLongExtra(Constant.PIPELINE_ID, 0);

        spStage = (Spinner) findViewById(R.id.et_stage);
        spProjects = (Spinner) findViewById(R.id.sp_projects);
        spRemarks = (Spinner) findViewById(R.id.sp_remarks);


        if (pipelineId == 0) {
            ple = new PipelineEntity();
            ple.setId(Util.getNextKey(PipelineEntity.class));
            forUpdate = false;
        } else {
            ple = Util.getPipeline(pipelineId);
            int selection;

            if (!"".equals(ple.getStage())) {
                if(ple.getStage().equals("1")){
                    selection = 1;
                }
                else if(ple.getStage().equals("2")){
                    selection = 2;
                }
                else if(ple.getStage().equals("3A")){
                    selection = 3;
                }
                else if(ple.getStage().equals("3B")){
                    selection = 4;
                }
                else if(ple.getStage().equals("4A")){
                    selection = 5;
                }
                else if(ple.getStage().equals("4B")){
                    selection = 6;
                }
                else if(ple.getStage().equals("5A")){
                    selection = 7;
                }
                else if(ple.getStage().equals("5B")){
                    selection = 8;
                }
                else{
                    selection = 0;
                }
            } else {
                selection = 0;
            }

            spStage.setSelection(selection);

            forUpdate = true;
        }

        context = this;
        realm = Realm.getDefaultInstance();
        productMap = new HashMap<>();
        projectMap = new HashMap<>();
        remarkMap = new HashMap<>();

        btnSave = (Button) findViewById(R.id.btn_save);
        btnSave.setTypeface(font);
        btnSave.setOnClickListener(this);

        btnCancel = (Button) findViewById(R.id.btn_cancel);
        btnCancel.setTypeface(font);
        btnCancel.setOnClickListener(this);

        etAmount = (EditText) findViewById(R.id.et_amount);

        etAmount.setText(String.valueOf(ple.getRevenue()));

        if(etAmount.getText().toString().equals(" ") || etAmount.getText().toString().isEmpty() || etAmount.getText().toString().equals("0")){
            etAmount.setText("");
            //etAmount.setHint("0");
        }
        else{
            etAmount.setText(String.valueOf(ple.getRevenue()));
        }

        etNotes = (EditText) findViewById(R.id.et_notes);
        etNotes.setTypeface(font);
        etNotes.setText(ple.getNotes() == null ? "" : ple.getNotes());
        if(etNotes.getText().toString().equals(" ") || etNotes.getText().toString().isEmpty() || etNotes.getText().toString().equals("") || etNotes.getText().toString().equals(null)){
            etNotes.setHint("NOTES");
        }

        spStage = (Spinner) findViewById(R.id.et_stage);

        spProducts = (Spinner) findViewById(R.id.sp_product);

        spRemarks = (Spinner) findViewById(R.id.sp_remarks);

        setSpinnerItems();

        //spProducts.setSelection(prodCtr);
        int s = projObjList.size();
        int indexSelected = 0;
        for (int x = 0 ; x < s ; x++){
         ProjectEntity projectEntity = projObjList.get(x);
            if(projectEntity != null){
                int id = projectEntity.getId();
                if (id == ple.getProjectId() ){
                    indexSelected = x;
                }
            }
        }
        spProjects.setSelection(indexSelected);


        int t = remObjList.size();
        int remIndexSelected = 0;
        for (int y = 0 ; y < t ; y++){
            RemarksEntity remarksEntity = remObjList.get(y);
            if(remarksEntity != null){
                int id = remarksEntity.getId();
                if (id == ple.getRemarks() ){
                    remIndexSelected = y;
                }
            }
        }
        spRemarks.setSelection(remIndexSelected);

        if(ple.getProductTypeId()==1){
            spProducts.setSelection(1);
        }
        else if(ple.getProductTypeId()==2){
            spProducts.setSelection(2);
        }
        else if(ple.getProductTypeId()==3){
            spProducts.setSelection(3);
        }
        else if(ple.getProductTypeId()==4){
            spProducts.setSelection(4);
        }
        else if(ple.getProductTypeId()==5){
            spProducts.setSelection(5);
        }
        else if(ple.getProductTypeId()==6){
            spProducts.setSelection(6);
        }
        else if(ple.getProductTypeId()==7){
            spProducts.setSelection(7);
        }
        else{
            spProducts.setSelection(0);
        }

        if(!" ".equals(spProducts.getSelectedItem().toString())){
                spProducts.setEnabled(false);
        }
        else{
                spProducts.setEnabled(true);
        }

        //if(ple.get)
//        if(ple.getProjectId()==1){
//            spProjects.setSelection(2);
//        }
//        else if(ple.getProjectId()==2){
//            spProjects.setSelection(4);
//        }
////        else if(ple.getProjectId()==3){
////            spProjects.setSelection(2);
////        }
//        else if(ple.getProjectId()==4){
//            spProjects.setSelection(3);
//        }
//        else if(ple.getProjectId()==5){
//            spProjects.setSelection(1);
//        }
//        else{
//            spProjects.setSelection(0);
//        }

//        if(ple.getRemarks() == 2){
//            spRemarks.setSelection(1);
//        }
//        else if(ple.getRemarks() == 3){
//            spRemarks.setSelection(7);
//        }
//        else if(ple.getRemarks() == 4){
//            spRemarks.setSelection(8);
//        }
//        else if(ple.getRemarks() == 5){
//            spRemarks.setSelection(13);
//        }
//        else if(ple.getRemarks() == 6){
//            spRemarks.setSelection(11);
//        }
//        else if(ple.getRemarks() == 7){
//            spRemarks.setSelection(23);
//        }
//        else if(ple.getRemarks() == 8){
//            spRemarks.setSelection(12);
//        }
//        else if(ple.getRemarks() == 9){
//            spRemarks.setSelection(9);
//        }
//        else if(ple.getRemarks() == 10){
//            spRemarks.setSelection(3);
//        }
//        else if(ple.getRemarks() == 11){
//            spRemarks.setSelection(6);
//        }
//        else if(ple.getRemarks() == 12){
//            spRemarks.setSelection(14);
//        }
//        else if(ple.getRemarks() == 13){
//            spRemarks.setSelection(16);
//        }
//        else if(ple.getRemarks() == 14){
//            spRemarks.setSelection(19);
//        }
//        else if(ple.getRemarks() == 15){
//            spRemarks.setSelection(17);
//        }
//        else if(ple.getRemarks() == 16){
//            spRemarks.setSelection(18);
//        }
//        else if(ple.getRemarks() == 17){
//            spRemarks.setSelection(24);
//        }
//        else if(ple.getRemarks() == 18){
//            spRemarks.setSelection(5);
//        }
//        else if(ple.getRemarks() == 19){
//            spRemarks.setSelection(10);
//        }
//        else if(ple.getRemarks() == 20){
//            spRemarks.setSelection(20);
//        }
//        else if(ple.getRemarks() == 21){
//            spRemarks.setSelection(21);
//        }
//        else if(ple.getRemarks() == 22){
//            spRemarks.setSelection(2);
//        }
//        else if(ple.getRemarks() == 23){
//            spRemarks.setSelection(4);
//        }
//        else if(ple.getRemarks() == 24){
//            spRemarks.setSelection(15);
//        }
//        else if(ple.getRemarks() == 25){
//            spRemarks.setSelection(22);
//        }
//        else{
//            spRemarks.setSelection(0);
//        }



        title = (TextView) findViewById(R.id.textView);
        title.setTypeface(font);

        tvProduct = (TextView) findViewById(R.id.lbl_add_pipeline_product);
        tvProduct.setTypeface(font);

        tvStage = (TextView) findViewById(R.id.lbl_add_pipeline_stage);
        tvStage.setTypeface(font);

        tvProjects = (TextView) findViewById(R.id.lbl_add_pipeline_projects);
        tvProjects.setTypeface(font);

        tvRemarks = (TextView) findViewById(R.id.lbl_add_pipeline_remarks);
        tvRemarks.setTypeface(font);

        tvAmount = (TextView) findViewById(R.id.lbl_add_pipeline_amount);
        tvAmount.setTypeface(font);

        etAmount.addTextChangedListener(mTextEditorWatcher);
        etNotes.addTextChangedListener(mTextEditorWatcher);

    }

    boolean enableChangeListener = true;

    private final TextWatcher mTextEditorWatcher = new TextWatcher() {

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // When No Password Entered

        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        public void afterTextChanged(Editable s) {
            enableChangeListener = false;
        }

    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_accounts_add_pipeline, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void setSpinnerItems() {
        final List<String> prodList = getProducts();
        prodList.add(" ");
        Collections.sort(prodList);
        final ArrayAdapter<String> prodDataAdapter = new ArrayAdapter<>(this,
                R.layout.spinner_item, prodList);

        spProducts.setAdapter(prodDataAdapter);
        final int myProd, myProj, myRek ;
        final String myStage;
        myStage = ple.getStage();
        myProd = ple.getProductTypeId();
        myProj = ple.getProjectId();
        myRek = ple.getRemarks();
        spProducts.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (myProd - 1 != spProducts.getSelectedItemId())
                    enableChangeListener = false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        //john
        projObjList = getProjetsObj();

        Collections.sort(projObjList,new Comparator<ProjectEntity>(){
            @Override
            public int compare(ProjectEntity lhs, ProjectEntity rhs) {
                return lhs.getName().compareToIgnoreCase(rhs.getName());
            }
                });

        List<String> projList = new ArrayList<String>();
        //projList.add(" ");
        int size = projObjList.size();
        for (int x=0; x<size; x++){
            ProjectEntity projectEntity = projObjList.get(x);
            projList.add(projectEntity.getName());
        }


        //------------------------------------


//        List<String> projList = getProjets();
//        projList.add(" ");
//        Collections.sort(projList);
        ArrayAdapter<String> projDataAdapter = new ArrayAdapter<>(this,
                R.layout.spinner_item, projList);
        spProjects.setAdapter(projDataAdapter);
        spProjects.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (myProj - 1 != spProjects.getSelectedItemId())
                    enableChangeListener = false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        //john remarks
        remObjList = getRemarksObj();

        Collections.sort(remObjList,new Comparator<RemarksEntity>(){
            @Override
            public int compare(RemarksEntity mhs, RemarksEntity nhs) {
                return mhs.getName().compareToIgnoreCase(mhs.getName());
            }
        });

        List<String> remarksList = new ArrayList<String>();
        //projList.add(" ");
        int remsize = remObjList.size();
        for (int y=0; y<remsize; y++){
            RemarksEntity remarksEntity = remObjList.get(y);
            remarksList.add(remarksEntity.getName());
        }


        //---------------------------------------------------
//        List<String> remList = getRemarks();
//        remList.add(" ");
//        Collections.sort(remList);
        ArrayAdapter<String> remDataAdapter = new ArrayAdapter<>(this,
                R.layout.spinner_item, remarksList);
        spRemarks.setAdapter(remDataAdapter);
        spRemarks.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (myRek - 2 != spRemarks.getSelectedItemId())
                    enableChangeListener = false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spStage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(null != myStage) {
                    if (!myStage.toString().equals(spStage.getSelectedItem())) {
                        enableChangeListener = false;
                    }
                    else {

                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save:
                disableButtons();
                count = 1;

                String prodItem = (String) spProducts.getSelectedItem();
                String stageItem = (String) spStage.getSelectedItem();
                String projItem = (String) spProjects.getSelectedItem();
                String remarkItem = (String) spRemarks.getSelectedItem();
                String amountItem = etAmount.getText().toString();

                if((stageItem.equals(" ") || stageItem.isEmpty()) ){
                    stageNotBlank();
                    btnSave.setEnabled(true);
                }
                else if((stageItem.equals("1") && amountItem.equals("0")) || (stageItem.equals("1") && amountItem.equals("")) ){
                    msgNotZero();
                    btnSave.setEnabled(true);
                }
                else if((stageItem.equals("2") && amountItem.equals("0")) || (stageItem.equals("2") && amountItem.equals("")) ){
                    msgNotZero();
                    btnSave.setEnabled(true);
                }
                else if((stageItem.equals("3A") && amountItem.equals("0")) || (stageItem.equals("3A") && amountItem.equals("")) ){
                    msgNotZero();
                    btnSave.setEnabled(true);
                }
                else if (stageItem.equals("3B") && amountItem.equals("0") || stageItem.equals("3B") && amountItem.equals("")){
                    msgNotZero();
                    btnSave.setEnabled(true);
                }
                else if (stageItem.equals("4A") && amountItem.equals("0") || stageItem.equals("4A") && amountItem.equals("")){
                    msgNotZero();
                    btnSave.setEnabled(true);
                }
                else if (stageItem.equals("4B") && amountItem.equals("0") || stageItem.equals("4B") && amountItem.equals("")){
                    msgNotZero();
                    btnSave.setEnabled(true);
                }
                else if (stageItem.equals("5A") && amountItem.equals("0") || stageItem.equals("5A") && amountItem.equals("")){
                    msgNotZero();
                    btnSave.setEnabled(true);
                }
                else if (stageItem.equals("5B") && amountItem.equals("0") || stageItem.equals("5B") && amountItem.equals("")){
                    msgNotZero();
                    btnSave.setEnabled(true);
                }
                else if (spProducts.getSelectedItem().toString().equals(" ")){
                    msgProductNotBlank();
                    btnSave.setEnabled(true);
                }
                else if(etAmount.getText().toString().isEmpty() || etAmount.getText().toString().equals(" ")){
                    msgNotZero();
                    btnSave.setEnabled(true);
                }
//                else if(spProjects.getSelectedItem().toString().equals(" ")){
//                    projNotBlank();
//                    btnSave.setEnabled(true);
//                }
                else{
                    realm.beginTransaction();
                    String today = Constant.SDF_API.format(new Date());
                   // SimpleDateFormat kamote_sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sssZ");
                    SimpleDateFormat kamote_sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sssZ");
                    try {
                        Date mypld = kamote_sdf.parse(today);
                    if(etAmount.getText().toString().isEmpty() || etAmount.getText().toString().equals(" ") || etAmount.getText().toString().equals("")){
                        ple.setRevenue(0);
                    }else {
                        ple.setRevenue(Integer.valueOf("".equals(etAmount.getText().toString()) ? etAmount.getHint().toString().trim() : etAmount.getText().toString().trim()));
                    }
                    ple.setStage(stageItem);
                    ple.setAccountId(id);
                    if(spRemarks.getSelectedItem().toString().equals(" ")){
                        ple.setRemarks(0);
                    }
                    else{
                        int selected = spRemarks.getSelectedItemPosition();
                        RemarksEntity remarksEntity = (RemarksEntity)remObjList.get(selected);
                        if (remarksEntity != null) {
                            ple.setRemarks(remarksEntity.getId());
                        }
                        //ple.setRemarks(remarkMap.get(remarkItem).getId());
                    }

                    if(spProjects.getSelectedItem().toString().equals(" ") || spProjects.getSelectedItem().toString().equals(null) || ((String) spProjects.getSelectedItem()).isEmpty()){
                        ple.setProjectId(0);
                    }
                    else {
                        int selected = spProjects.getSelectedItemPosition();
                        ProjectEntity projEntity = (ProjectEntity)projObjList.get(selected);
                        if (projEntity != null) {
                            ple.setProjectId(projEntity.getId());
                        }
//                        ple.setProjectId(projectMap.get(projItem).getId());
                    }

                    if(etNotes.getText().toString().equals(" ") || etNotes.getText().toString().isEmpty() || etNotes.getText().toString().equals("")){
                        ple.setNotes("");
                    }
                    else {
                        ple.setNotes("".equals(etNotes.getText().toString()) ? etNotes.getHint().toString() : etNotes.getText().toString());
                    }
                    ple.setUpdateDate(today);
                    ple.setProductTypeId(productMap.get(prodItem).getId());
                    ple.setStartDate(today);

                    realm.commitTransaction();

                        JSONObject jsonObject = new JSONObject();
                        try {

                            if (!forUpdate) {
                                //FOR NEW

                                jsonObject.put("id", 0);
                                jsonObject.put("accountPipelineId", ple.getId());
                                jsonObject.put("accountIdFk", ple.getAccountId());
                                jsonObject.put("productTypeIdFk", ple.getProductTypeId());
                                jsonObject.put("startDate", today);
                                jsonObject.put("endDate", "null");
                                jsonObject.put("plCheckoutDate", mypld);
                                count++;
                                new AddPipelineAsyncTask().execute(Constant.ADD_ACCOUNT_PIPELINE_URL(context), jsonObject.toString());

                                JSONObject savePipeline = new JSONObject();
                                savePipeline.put("id", 0);
                                savePipeline.put("accountPipelineIdFk", ple.getId());
                                savePipeline.put("stage", ple.getStage());
                                savePipeline.put("potentialRevenue", ple.getRevenue());
                                savePipeline.put("notes", ple.getNotes());
                                savePipeline.put("updateDate", ple.getUpdateDate());
                                savePipeline.put("pipelineRemarksIdFk", ple.getRemarks());
                                savePipeline.put("projectIdFk", ple.getProjectId());
                                savePipeline.put("plCheckoutDate", mypld);
                                count++;
                                new AddPipelineAsyncTask().execute(Constant.SAVE_PIPELINE_URL(context), savePipeline.toString());


                            } else {
                                //For Update
//                                JSONObject savePipeline = new JSONObject();
//                                savePipeline.put("id", accountPipelineID);
//                                savePipeline.put("accountPipelineIdFk", ple.getId());
//                                savePipeline.put("stage", ple.getStage());
//                                savePipeline.put("potentialRevenue", ple.getRevenue());
//                                savePipeline.put("notes", ple.getNotes());
//                                savePipeline.put("updateDate", ple.getUpdateDate());
//                                savePipeline.put("pipelineRemarksIdFk", ple.getRemarks());
//                                savePipeline.put("projectIdFk", ple.getProjectId());
//                                savePipeline.put("plCheckoutDate", mypld);
//                                count++;
//                                new AddPipelineAsyncTask().execute(Constant.SAVE_PIPELINE_URL(context), savePipeline.toString());
                                sendSavePipeline(ple);
                            }
                            getAlertDialog(false).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }catch (ParseException ex) {
                        Log.d("Error : ", "error date check in out");
                    }




                }
                //case

                break;

            case R.id.btn_cancel:
                caseButton();
                break;
        }
    }


    public void mesgChange(){
        new AlertDialog.Builder(context)
                .setTitle("")
                .setMessage("Discard changes made?")
                .setPositiveButton("No", null)
                .setNegativeButton("Yes", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface arg0, int arg1) {
                                AccountsAddPipeline.this.finish();
                            }
                        }
                ).create().show();
    }

    public void msgNotZero(){
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setMessage("Please indicate correct amount on this stage");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public void projNotBlank(){
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setMessage("Project cannot be blank");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public void stageNotBlank(){
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setMessage("Stage cannot be blank");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public void msgProductNotBlank(){
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setMessage("Product cannot be blank");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public void caseButton() {
        int myAmnt = ple.getRevenue();
        String myStage = ple.getStage();
        String etnoytes = ple.getNotes();
        int myProd, myProj, myRek ;
        myProd = ple.getProductTypeId();
        myProj = ple.getProjectId();
        myRek = ple.getRemarks();
        if (myProd - 1 != spProducts.getSelectedItemId())
            mesgChange();
        else if (myProj - 1 != spProjects.getSelectedItemId()){
            mesgChange();
        }
        else if (myRek - 2  != spRemarks.getSelectedItemId()){
            mesgChange();
        }
        else if (!etnoytes.toString().equals(etNotes.getText().toString())) {
            mesgChange();
        }
        else if (myAmnt != Integer.parseInt(etAmount.getText().toString())) {
            mesgChange();
        }
        else if (!myStage.toString().equals(spStage)) {
            mesgChange();
        }
        else {finish();}
    }




    public void disableButtons() {
        btnSave.setEnabled(false);
    }

    public List<String> getProducts() {

        int ctrinner = 0;
        prodCtr = 0;
        List<String> prods = new ArrayList<>();

        RealmResults<ProductTypeEntity> result = realm.where(ProductTypeEntity.class).findAll();
        for (ProductTypeEntity pte : result) {
            prods.add(pte.getProductTypeName());
            productMap.put(pte.getProductTypeName(), pte);
            if (pte.getId() == ple.getProductTypeId())
                setProdSelected(ctrinner);
            ctrinner++;
        }
        //Collections.sort(prods);
        return prods;
    }

    public List<String> getProjets() {
        int ctrinner = 0;
        projCtr = 0;
        List<String> proj = new ArrayList<>();

        RealmResults<ProjectEntity> result = realm.where(ProjectEntity.class).findAll();

        for (ProjectEntity res : result) {
            proj.add(res.getName());
            projectMap.put(res.getName(), res);
            if (res.getId() == ple.getProjectId())
                setProjSelected(ctrinner); //projectEntity
            ctrinner++;
        }

        return proj;
    }

    //john
    public List<ProjectEntity> getProjetsObj() {
        int ctrinner = 0;
        projCtr = 0;
        List<ProjectEntity> proj = new ArrayList<>();

        RealmResults<ProjectEntity> result = realm.where(ProjectEntity.class).findAll();
        int size = result.size() + 1;
        for (int x = 0; x < size; x++){
            ProjectEntity projectEntity = new ProjectEntity();
            if(x != 0) {
                projectEntity = (ProjectEntity) result.get(x - 1);
            }
            else
            {
                projectEntity.setId(0);
                projectEntity.setName("");
                projectEntity.setDescription("");
            }
            proj.add(projectEntity);
//            if (projectEntity.getId() == ple.getProjectId())
//                setProjSelected(x);
        }

        return proj;
    }

    public List<String> getRemarks() {
        int ctrinner = 0;
        remCtr = 0;
        List<String> rem = new ArrayList<>();
        RealmResults<RemarksEntity> result = realm.where(RemarksEntity.class).findAll();
        for (RemarksEntity res : result) {
            rem.add(res.getName());
            remarkMap.put(res.getName(), res);
            if (res.getId() == ple.getRemarks())
                setRemSelected(ctrinner);
            ctrinner++;
        }

        return rem;
    }

    //john
    public List<RemarksEntity> getRemarksObj() {
        int ctrinner = 0;
        remCtr = 0;
        List<RemarksEntity> rems = new ArrayList<>();

        RealmResults<RemarksEntity> result = realm.where(RemarksEntity.class).findAll();
        int size = result.size() + 1;
        for (int x = 0; x < size; x++){
            RemarksEntity remarksEntity = new RemarksEntity();
            if(x != 0) {
                remarksEntity = (RemarksEntity) result.get(x - 1);
            }
            else
            {
                remarksEntity.setId(0);
                remarksEntity.setName("");
                remarksEntity.setDescription("");
            }
            rems.add(remarksEntity);
//            if (projectEntity.getId() == ple.getProjectId())
//                setProjSelected(x);
        }

        return rems;
    }

    public void setProdSelected(int i) {
        prodCtr = i;
    }

    public void setProjSelected(int i) {
        projCtr = i;
    }

    public void setRemSelected(int i) {
        remCtr = i;
    }


    private class AddPipelineAsyncTask extends AsyncTask<String, Integer, String> {
        @Override
        protected void onProgressUpdate(Integer... progress) {
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                if (result != null && !"".equals(result)) {
                    sendSavePipeline(ple, Long.valueOf(result));
                } else {
                    realm.beginTransaction();
                    realm.copyToRealmOrUpdate(ple);
                    realm.commitTransaction();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NumberFormatException e) {
                realm.beginTransaction();
                realm.copyToRealmOrUpdate(ple);
                realm.commitTransaction();
            }
        }

        @Override
        protected String doInBackground(String... strings) {

            String resp = HttpManager.POST(strings[0], strings[1], context);
            if (strings[0].contains(Constant.ADD_ACCOUNT_PIPELINE_URL(context))) {
                return resp;
            } else {
                return null;
            }
        }
    }


    public void sendSavePipeline(PipelineEntity ple2, Long id) throws JSONException {
        ple2.setId(id);
        realm.beginTransaction();
        ple.setId(id);

        realm.copyToRealmOrUpdate(ple);
        realm.commitTransaction();
        sendSavePipeline(ple2);
    }

    public void sendSavePipeline(PipelineEntity ple) throws JSONException {
        SimpleDateFormat kamote_sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sssZ");
        String today = Constant.SDF_API.format(new Date());
        try {
            Date mypld = kamote_sdf.parse(today);
            JSONObject savePipeline = new JSONObject();
            savePipeline.put("id", 0);
            savePipeline.put("accountPipelineIdFk", ple.getId());
            savePipeline.put("stage", ple.getStage());
            savePipeline.put("potentialRevenue", ple.getRevenue());
            savePipeline.put("notes", ple.getNotes());
            savePipeline.put("updateDate", ple.getUpdateDate());
            savePipeline.put("pipelineRemarksIdFk", ple.getRemarks());
            savePipeline.put("projectIdFk", ple.getProjectId());
            savePipeline.put("plCheckoutDate", mypld);
            new AddPipelineAsyncTask().execute(Constant.SAVE_PIPELINE_URL(context), savePipeline.toString());
        } catch (ParseException ex) {
        Log.d("Error : ", "error date check in out");
    }

    }

    public AlertDialog getAlertDialog(boolean hasError) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        if (hasError) {
            alertDialogBuilder.setTitle("");

            // set dialog message
            alertDialogBuilder
                    .setMessage("An error occured while saving the pipeline, please try again by clicking the Sync Button on the side menu.")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                            dialog.cancel();
                        }
                    });
        } else {
            alertDialogBuilder.setTitle("");
            alertDialogBuilder
                    .setMessage("Pipeline was successfully saved!")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                            dialog.cancel();
                        }
                    });
        }

        AlertDialog alertDialog = alertDialogBuilder.create();
        return alertDialog;
    }


    @Override
    public void onBackPressed() {
        if (!enableChangeListener) {
            new AlertDialog.Builder(this)
                    .setTitle("")
                    .setMessage("Do you want to save changes?")
                    .setPositiveButton(android.R.string.yes, null)
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface arg0, int arg1) {
                                    AccountsAddPipeline.this.finish();
                                }
                            }
                    ).create().show();
        }
    }

}

