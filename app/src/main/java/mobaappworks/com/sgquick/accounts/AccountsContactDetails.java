package mobaappworks.com.sgquick.accounts;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.mobaappworks.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmResults;
import mobaappworks.com.sgquick.Constant;
import mobaappworks.com.sgquick.entity.AccountEntity;
import mobaappworks.com.sgquick.entity.CompanyContactEntity;
import mobaappworks.com.sgquick.entity.CompanyContactRoleEntity;
import mobaappworks.com.sgquick.entity.ProductTypeEntity;

/**
 * Created by ldeguzman on 9/12/15.
 */
public class AccountsContactDetails extends Activity implements View.OnClickListener, TextWatcher, AdapterView.OnItemSelectedListener{
    private EditText etName;
    private EditText etNumber;
    private EditText etEmail;
    private EditText etBirthDate;
    private EditText etInterest;
    private EditText etPreferences;
    private Spinner spRole;
    private Realm realm;
    private Button btnSave;
    private Button btnCancel;

    private boolean isNew;
    private int id;
    private int ctr = 0;
    private Map<String,CompanyContactRoleEntity> roleMap;

    private CompanyContactEntity companyContactEntity;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accounts_contact_details);
        realm = Realm.getDefaultInstance();
        id = getIntent().getIntExtra(Constant.ID, 0);
        isNew = getIntent().getBooleanExtra(Constant.IS_NEW, false);

        if(isNew) {
            companyContactEntity = new CompanyContactEntity();
        }else{
            companyContactEntity = getContact(id);
        }

        roleMap = new HashMap<>();

        spRole = (Spinner) findViewById(R.id.et_role);
        spRole.setOnItemSelectedListener(this);

        etName = (EditText) findViewById(R.id.et_name);
        etName.addTextChangedListener(this);
        etNumber = (EditText) findViewById(R.id.et_number);
        etNumber.addTextChangedListener(this);
        etEmail = (EditText) findViewById(R.id.et_email);
        etEmail.addTextChangedListener(this);
        etBirthDate = (EditText) findViewById(R.id.et_birthday);
        etBirthDate.addTextChangedListener(this);
        etInterest = (EditText) findViewById(R.id.et_interest);
        etInterest.addTextChangedListener(this);
        etPreferences = (EditText) findViewById(R.id.et_pref);
        etPreferences.addTextChangedListener(this);

        btnSave = (Button) findViewById(R.id.btn_save);
        btnSave.setOnClickListener(this);
        btnCancel = (Button) findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(this);

        etName.setHint(companyContactEntity.getName());
        etNumber.setHint(companyContactEntity.getContactNumber());
        etEmail.setHint(companyContactEntity.getEmail());
        etBirthDate.setHint(companyContactEntity.getBirthDate());
        etInterest.setHint(companyContactEntity.getInterests());
        etPreferences.setHint(companyContactEntity.getPreference());

        setSpinnerItems();
    }

    public void setSpinnerItems() {
        List<String> roleList = getRoles();
        ArrayAdapter<String> roleDataAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, roleList);
        spRole.setAdapter(roleDataAdapter);
        spRole.setSelection(ctr);
    }
    public List<String> getRoles(){
        int ctrinner = 0;
        List<String> roles = new ArrayList<>();
        RealmResults<CompanyContactRoleEntity> result = realm.where(CompanyContactRoleEntity.class).findAll();
        for (CompanyContactRoleEntity ccre : result) {
            roles.add(ccre.getName());
            roleMap.put(ccre.getName(), ccre);
            if(companyContactEntity.getCompanyContactRoleId() != null &&
                    ccre.getName().equals(companyContactEntity.getCompanyContactRoleId().getName())){
                setSelected(ctrinner);
            }
            ctrinner++;
        }
        return roles;
    }

    public void setSelected(int i){
        ctr = i;
    }



    public CompanyContactEntity getContact(int id){
        return realm.where(CompanyContactEntity.class).equalTo("id",id).findFirst();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_save:

                realm.beginTransaction();

                companyContactEntity.setName("".equals(etName.getText().toString()) ? etName.getHint().toString() : etName.getText().toString());
                companyContactEntity.setContactNumber("".equals(etNumber.getText().toString()) ? etNumber.getHint().toString() : etNumber.getText().toString());
                companyContactEntity.setEmail("".equals(etEmail.getText().toString()) ? etEmail.getHint().toString() : etEmail.getText().toString());
                companyContactEntity.setBirthDate("".equals(etBirthDate.getText().toString()) ? etBirthDate.getHint().toString() : etBirthDate.getText().toString());
                companyContactEntity.setInterests("".equals(etInterest.getText().toString()) ? etInterest.getHint().toString() : etInterest.getText().toString());
                companyContactEntity.setPreference("".equals(etPreferences.getText().toString()) ? etPreferences.getHint().toString() : etPreferences.getText().toString());
                companyContactEntity.setCompanyContactRoleId(roleMap.get(spRole.getSelectedItem()));

                if(isNew) {
                    companyContactEntity.setAccountId(getAccountDetails(id));
                    realm.copyToRealmOrUpdate(companyContactEntity);
                }
                realm.commitTransaction();

                finish();
                break;
            case R.id.btn_cancel:
                finish();
        }
    }

    public AccountEntity getAccountDetails(int id){
        RealmResults<AccountEntity> result = realm.where(AccountEntity.class)
                .equalTo("id", id)
                .findAll();
        if(result.size() == 1) {
            for (AccountEntity ae : result) {
                return ae;
            }
        }else{
            Log.d("[Account Fragment Info]", "Id not found");
        }
        return null;
    }
    public Date stringToDate(String toDate){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        try {
            return format.parse(toDate);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return new Date();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        btnSave.setEnabled(true);
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        btnSave.setEnabled(true);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
