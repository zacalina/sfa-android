package mobaappworks.com.sgquick.accounts;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;

import com.mobaappworks.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import mobaappworks.com.sgquick.Constant;
import mobaappworks.com.sgquick.FontChangeCrawler;
import mobaappworks.com.sgquick.entity.CheckInOutEntity;
import mobaappworks.com.sgquick.entity.CrossSellUpSellEntity;
import mobaappworks.com.sgquick.entity.PipelineEntity;

public class AccountsFragmentCSUS extends Fragment implements View.OnClickListener {
    private View rootView;
    private Context context;
    private ImageButton btnCheckIn;
    private ImageButton btnAddPipeline;
    private Realm realm;
    private int id;
    private ListView lvCsus;

    private Uri checkin = Uri.parse("android.resource://mobaappworks.com.sgquick/" + R.drawable.fragment_checkin);
    private Uri checkout = Uri.parse("android.resource://mobaappworks.com.sgquick/" + R.drawable.fragment_checkout);
    private Uri addToPipeline = Uri.parse("android.resource://mobaappworks.com.sgquick/" + R.drawable.fragment_addpipe);

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        FontChangeCrawler fontChanger = new FontChangeCrawler(this.context.getAssets(), Constant.FONT_FSELLIOT_LIGHT);
        fontChanger.replaceFonts((ViewGroup) this.getView());
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_accounts_csus, container, false);
        Bundle args = getArguments();
        id = args.getInt(Constant.ID);
        realm = Realm.getDefaultInstance();
        context = rootView.getContext();

        btnCheckIn = (ImageButton) rootView.findViewById(R.id.btn_check_in);
        btnCheckIn.setImageURI(checkin);
        btnCheckIn.setBackgroundColor(Color.parseColor("#333333"));
        btnCheckIn.setOnClickListener(this);

        btnAddPipeline = (ImageButton) rootView.findViewById(R.id.btn_add_pipeline);
        btnAddPipeline.setImageURI(addToPipeline);
        btnAddPipeline.setBackgroundColor(Color.parseColor("#333333"));
        btnAddPipeline.setOnClickListener(this);


        RealmResults<CrossSellUpSellEntity> csus = getcsus(id);
        lvCsus = (ListView) rootView.findViewById(R.id.csus_list);
        lvCsus.setAdapter(new AccountsFragmentCsusAdapter(context,csus));

        if(isCheckedIn())
            btnCheckIn.setImageURI(checkout);
        else
            btnCheckIn.setImageURI(checkin);


        return rootView;
    }

    public RealmResults<CrossSellUpSellEntity> getcsus(int id){
        RealmQuery<CrossSellUpSellEntity> query = realm.where(CrossSellUpSellEntity.class)
                .equalTo("accountId.id", id);
        return query.findAll();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_check_in:
                Intent checkinIntent = new Intent(context, AccountsCheckInPopUp.class);
                checkinIntent.putExtra(Constant.ID, id);
                startActivity(checkinIntent);
                break;
            case R.id.btn_add_pipeline:
                Intent aap = new Intent(context, AccountsAddPipeline.class);
                aap.putExtra(Constant.ID, id);
                startActivity(aap);
                break;
        }
    }
    public boolean isCheckedIn(){
        RealmResults<CheckInOutEntity> cioes = realm.where(CheckInOutEntity.class)
                .equalTo("accountId", String.valueOf(id))
                .equalTo("checkoutLng", "")
                .equalTo("checkoutLat", "")
                .findAll();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String today = sdf.format(new Date());
        for (CheckInOutEntity  checkInOutEntity: cioes) {
            String checkin = sdf.format(checkInOutEntity.getCheckinDate());
            if (checkin.equals(today))
                return true;
        }
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("ON RESUME", "CSUS");

        if(isCheckedIn())
            btnCheckIn.setImageURI(checkout);
        else
            btnCheckIn.setImageURI(checkin);
    }
}