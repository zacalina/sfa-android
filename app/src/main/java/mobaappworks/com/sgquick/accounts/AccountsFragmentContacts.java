package mobaappworks.com.sgquick.accounts;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;

import com.mobaappworks.R;

import java.text.SimpleDateFormat;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmResults;
import mobaappworks.com.sgquick.Constant;
import mobaappworks.com.sgquick.FontChangeCrawler;
import mobaappworks.com.sgquick.entity.CheckInOutEntity;
import mobaappworks.com.sgquick.entity.CompanyContactEntity;

public class AccountsFragmentContacts extends Fragment implements View.OnClickListener {

    private ImageButton btnCheckIn;
    private ImageButton btnAddContact;
    private ImageButton btnAddPipeline;
    private View rootView;
    private Context context;
    private ListView lvContacts;
    private int id;
    private Realm realm;

    private Uri checkin = Uri.parse("android.resource://mobaappworks.com.sgquick/" + R.drawable.fragment_checkin);
    private Uri checkout = Uri.parse("android.resource://mobaappworks.com.sgquick/" + R.drawable.fragment_checkout);
    private Uri addToPipeline = Uri.parse("android.resource://mobaappworks.com.sgquick/" + R.drawable.fragment_addpipe);

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        FontChangeCrawler fontChanger = new FontChangeCrawler(this.context.getAssets(), Constant.FONT_FSELLIOT_LIGHT);
        fontChanger.replaceFonts((ViewGroup) this.getView());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_accounts_contacts, container, false);
        Bundle args = getArguments();

        id = args.getInt(Constant.ID);
        context = rootView.getContext();
        realm = Realm.getDefaultInstance();

        btnCheckIn = (ImageButton) rootView.findViewById(R.id.btn_check_in);
        btnCheckIn.setImageURI(checkin);
        btnCheckIn.setBackgroundColor(Color.parseColor("#333333"));
        btnCheckIn.setOnClickListener(this);

        btnAddPipeline = (ImageButton) rootView.findViewById(R.id.btn_add_pipeline);
        btnAddPipeline.setImageURI(addToPipeline);
        btnAddPipeline.setBackgroundColor(Color.parseColor("#333333"));
        btnAddPipeline.setOnClickListener(this);

        btnAddContact = (ImageButton) rootView.findViewById(R.id.btn_add_contact);
        //DISABLE ADD CONTACT
        btnAddContact.setVisibility(View.GONE);
        btnAddContact.setOnClickListener(this);

        if(isCheckedIn())
            btnCheckIn.setImageURI(checkout);
        else
            btnCheckIn.setImageURI(checkin);

        lvContacts = (ListView) rootView.findViewById(R.id.lv_contact);
        lvContacts.setAdapter(new ContactListAdapter(context, getContacts(id)));
        lvContacts.setOnItemClickListener(onClickListener());

        return rootView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_check_in:
                Intent checkinIntent = new Intent(context, AccountsCheckInPopUp.class);
                checkinIntent.putExtra(Constant.ID, id);
                startActivity(checkinIntent);
                break;
            case R.id.btn_add_contact:
                Intent addContactIntent = new Intent(context, AccountsContactDetails.class);
                addContactIntent.putExtra(Constant.ID, id);
                addContactIntent.putExtra(Constant.IS_NEW, true);
                startActivity(addContactIntent);
                break;
            case R.id.btn_add_pipeline:
                Intent aap = new Intent(context, AccountsAddPipeline.class);
                aap.putExtra(Constant.ID, id);
                startActivity(aap);
                break;

        }
    }
    public AdapterView.OnItemClickListener onClickListener(){
        return new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Integer tag = (Integer) view.getTag();
                Intent intent = new Intent(context, AccountsContactDetails.class);
                intent.putExtra(Constant.ID, tag);
                context.startActivity(intent);
            }

        };
    }

    public RealmResults<CompanyContactEntity> getContacts(int id){
        return realm.where(CompanyContactEntity.class).equalTo("accountId.id",id).findAll();
    }

    public boolean isCheckedIn(){
        RealmResults<CheckInOutEntity> cioes = realm.where(CheckInOutEntity.class)
                .equalTo("accountId", String.valueOf(id))
                .equalTo("checkoutLng", "")
                .equalTo("checkoutLat", "")
                .findAll();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String today = sdf.format(new Date());
        for (CheckInOutEntity  cioe: cioes) {
            String checkin = sdf.format(cioe.getCheckinDate());
            if (checkin.equals(today))
                return true;
        }
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("ON RESUME", "CONTACTS");

        if(isCheckedIn())
            btnCheckIn.setImageURI(checkout);
        else
            btnCheckIn.setImageURI(checkin);
    }
}