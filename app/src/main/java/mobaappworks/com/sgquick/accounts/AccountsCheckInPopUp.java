package mobaappworks.com.sgquick.accounts;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mobaappworks.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import mobaappworks.com.sgquick.Constant;
import mobaappworks.com.sgquick.FontChangeCrawler;
import mobaappworks.com.sgquick.HttpManager;
import mobaappworks.com.sgquick.Util;
import mobaappworks.com.sgquick.entity.AccountEntity;
import mobaappworks.com.sgquick.entity.CheckInOutEntity;
import mobaappworks.com.sgquick.entity.DeviceHealth;

/**
 * Created by ldeguzman on 9/9/15.
 */
public class AccountsCheckInPopUp extends Activity implements View.OnClickListener, LocationListener{
    private Realm realm;
    private int id;
    private Button btnCancel;
    private TextView tvTitleBar;
    private Button btnCheckin;
    private Context context;
    private EditText etNotes;
    private CheckInOutEntity checkInOutEntity;
    private Location location;
    private LocationManager locationManager;
    private Double latitude;
    private Double longitude;
    private Double batteryLife;
    private Boolean isGPSEnabled;
    private Boolean isNetworkEnabled;
    private Integer hasInternet;
    private String carrierName;
    private String signalStrength;
    private String storageLevel;
    private String imei;
    private String deviceName;
    private String osVersion;
    private int count;
    private String message;

    private String TAG = this.getClass().getCanonicalName();

    @Override
    public void setContentView(View view)
    {
        super.setContentView(view);
        FontChangeCrawler fontChanger = new FontChangeCrawler(this.getApplicationContext().getAssets(), Constant.FONT_FSELLIOT_REG);
        fontChanger.replaceFonts((ViewGroup)this.findViewById(android.R.id.content));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_accounts_info_checkin);
//
        Typeface font_item = Typeface.createFromAsset(getAssets(), Constant.FONT_FSELLIOT_REG);
//        tvTitleBar.setTypeface(font_item);
//        btnCheckin.setTypeface(font_item);

        id = getIntent().getIntExtra(Constant.ID, 0);
        Log.d("PASSED ID ", String.valueOf(id));
        realm = Realm.getDefaultInstance();
        context = this;

        btnCancel = (Button) findViewById(R.id.btn_cancel);
        tvTitleBar = (TextView) findViewById(R.id.tv_title_bar);
        etNotes = (EditText) findViewById(R.id.et_notes);
        btnCheckin = (Button) findViewById(R.id.btn_check_in);

        tvTitleBar.setTypeface(font_item);
        etNotes.setTypeface(font_item);
        btnCheckin.setTypeface(font_item);

        btnCheckin.setOnClickListener(this);
        btnCancel.setOnClickListener(this);


        if(isCheckedIn()) {
            tvTitleBar.setText("CHECK-OUT");
            btnCheckin.setText("CHECK-OUT");
            message = "CHECK-OUT";
        }else {
            btnCheckin.setText("CHECK-IN");
            tvTitleBar.setText("CHECK-IN");
            message = "CHECK-IN";
        }

        getLocation();

        batteryLife = Util.getBatteryLife(this);
//        carrierName = Util.getCarrierName(this);
        signalStrength = Util.getDisplaySignal();
        hasInternet = Util.isNetworkAvailable(this)? 1:0;
        storageLevel = Util.getAvailableInternalMemorySize() + " free of "
                + Util.getTotalInternalMemorySize() + " Total";
        imei = Util.getIMEI(this);
        osVersion  = Build.VERSION.RELEASE;
        deviceName = Util.getDeviceName();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_cancel:
                finish();
                break;
            case R.id.btn_check_in:
                count = 1;
                btnCheckin.setEnabled(false);
                try {
                    //GET DEVICE INFO
                    JSONObject deviceHealth = new JSONObject();
                    deviceHealth.put("id", "0");
                    deviceHealth.put("signalLevel",signalStrength);
                    deviceHealth.put("batteryLevel",batteryLife.toString());
                    deviceHealth.put("hasInternet", hasInternet);
                    deviceHealth.put("storageLevel", storageLevel);
                    deviceHealth.put("capturedTime", Constant.SDF_API.format(new Date()));
                    deviceHealth.put("employeeIdFk", Util.getUserId());
                    deviceHealth.put("imei", imei);
                    deviceHealth.put("osVersion", osVersion);
                    deviceHealth.put("deviceModel", deviceName);

                    if(isCheckedIn()){
                        //FOR CHECK OUT
                        String note = etNotes.getText().toString();
                        realm.beginTransaction();
                        checkInOutEntity.setCheckoutLat(latitude.toString());
                        checkInOutEntity.setCheckoutLng(longitude.toString());
                        checkInOutEntity.setCheckoutDate(new Date());
                        checkInOutEntity.setCheckoutNote(note);

                        AccountEntity accountsById = Util.getAccountsById(Integer.valueOf(checkInOutEntity.getAccountId()));
                        accountsById.setDateOfLastVisit(String.valueOf(new Date().getTime()));

                        realm.commitTransaction();

                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("id", "0");
                        jsonObject.put("accountIdFk", checkInOutEntity.getAccountId());
                        jsonObject.put("longitude", checkInOutEntity.getCheckinLng());
                        jsonObject.put("latitude", checkInOutEntity.getCheckinLat());
                        jsonObject.put("checkInTime", Constant.SDF_API.format(checkInOutEntity.getCheckinDate()));
                        jsonObject.put("checkOutTime", Constant.SDF_API.format(checkInOutEntity.getCheckoutDate()));
                        jsonObject.put("remarks", checkInOutEntity.getCheckinNote() + checkInOutEntity.getCheckoutNote());

                        count++;
                        new CheckInOutAsyncTask().execute(Constant.ADD_CHECK_IN_OUT_URL(context), jsonObject.toString());
                        deviceHealth.put("transactionType", "check_out");

                    }else {
                        //FOR CHEKIN
                        Long nextId = Util.getNextKey(CheckInOutEntity.class);
                        String note = etNotes.getText().toString();

                        CheckInOutEntity cie = new CheckInOutEntity(
                                nextId.intValue(),
                                String.valueOf(id),
                                longitude.toString(),
                                latitude.toString(),
                                null,
                                null,
                                new Date(),
                                null,
                                note,
                                null);

                        realm.beginTransaction();
                        realm.copyToRealmOrUpdate(cie);
                        realm.commitTransaction();

                        deviceHealth.put("transactionType", "check_in");
                    }

                    new CheckInOutAsyncTask().execute(Constant.ADD_DEVICE_HEALTH(context), deviceHealth.toString());
                    realm.beginTransaction();
                    deviceHealth.put("id", Util.getNextKey(DeviceHealth.class));
                    realm.createObjectFromJson(DeviceHealth.class, deviceHealth.toString());


                    AccountEntity accountsById = Util.getAccountsById(id);
                    accountsById.setDateOfLastVisit(String.valueOf(new Date().getTime()));
                    realm.commitTransaction();
                    getAlertDialog(false).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                break;
        }
    }

    public Location getLocation() {
        try {
            if(latitude == null){
                latitude = 0D;
            }

            if(longitude == null){
                longitude = 0D;
            }

            locationManager = (LocationManager) context
                    .getSystemService(LOCATION_SERVICE);

            // getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
            } else {
//                this.canGetLocation = true;
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            5000,
                            10,
                            this);
                    Log.d("Network", "Network Enabled");
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                5000,
                                10,
                                this);
                        Log.d("GPS", "GPS Enabled");
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return location;
    }
    public boolean isCheckedIn(){
        RealmResults<CheckInOutEntity> cioes = realm.where(CheckInOutEntity.class)
                .equalTo("accountId", String.valueOf(id))
                .equalTo("checkoutLng", "")
                .equalTo("checkoutLat", "")
                .findAll();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String today = sdf.format(new Date());
        for (CheckInOutEntity  cioe: cioes) {
            String checkin = sdf.format(cioe.getCheckinDate());
            if (checkin.equals(today))
                checkInOutEntity = cioe;
            return true;
        }
        return false;
    }
    @Override
    public void onLocationChanged(Location location) {
        String lng = "Longitude: " + location.getLongitude();
        longitude = location.getLongitude();
        Log.v(TAG, lng);
        String lat = "Latitude: " + location.getLatitude();
        latitude = location.getLatitude();
        Log.v(TAG, lat);
    }
    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
    }
    @Override
    public void onProviderEnabled(String s) {
    }
    @Override
    public void onProviderDisabled(String s) {
    }

    private class CheckInOutAsyncTask extends AsyncTask<String, Integer, String> {
        @Override
        protected void onProgressUpdate(Integer... progress) {
        }
        @Override
        protected void onPostExecute(String result) {
        }
        @Override
        protected String doInBackground(String... strings) {
            return HttpManager.PUT(strings[0], strings[1], context);
        }
    }

    public AlertDialog getAlertDialog(boolean hasError){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        if(hasError){
            alertDialogBuilder.setTitle("");

            // set dialog message
            alertDialogBuilder
                    .setMessage("An error occured while " + message + ", please try again by clicking the Sync Button on the side menu.")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                            dialog.cancel();
                        }
                    });
        } else {
            alertDialogBuilder.setTitle("");
            alertDialogBuilder
                    .setMessage("Successful " + message + "!")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                            dialog.cancel();
                        }
                    });
        }

        AlertDialog alertDialog = alertDialogBuilder.create();

        return alertDialog;
    }
}
