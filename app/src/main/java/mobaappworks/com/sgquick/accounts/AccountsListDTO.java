package mobaappworks.com.sgquick.accounts;

/**
 * Created by ldeguzman on 9/5/15.
 */
public class AccountsListDTO {

    private int id;
    private String name;
    private String info;
    private String filename;

    public AccountsListDTO(int id, String name, String info, String filename) {
        this.id = id;
        this.name = name;
        this.info = info;
        this.filename = filename;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
}
