package mobaappworks.com.sgquick.accounts;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mobaappworks.R;

import java.text.SimpleDateFormat;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmResults;
import mobaappworks.com.sgquick.Constant;
import mobaappworks.com.sgquick.FontChangeCrawler;
import mobaappworks.com.sgquick.entity.AccountDetailsEntity;
import mobaappworks.com.sgquick.entity.CheckInOutEntity;

public class AccountsFragmentExistingProd extends Fragment implements View.OnClickListener {
    private View rootView;
    private TextView mtPostpaidLines;
    private TextView mtPostpaidMsf;
    private TextView mtPostpaidRev;
    private TextView bbLine;
    private TextView bbRev;
    private TextView bbMsf;
    private TextView solLine;
    private TextView solRev;
    private TextView solMsf;
    private TextView vLine;
    private TextView vRev;
    private TextView vMsf;
    private TextView nomLine;
    private TextView nomMsf;
    private TextView nomRev;
    private TextView dataLine;
    private TextView dataRev;
    private TextView dataMsf;
    private Context context;
    private ImageButton btnAddPipeline;
    private ImageButton btnCheckIn;
    private int id;

    private Realm realm;

    private Uri checkin = Uri.parse("android.resource://mobaappworks.com.sgquick/" + R.drawable.fragment_checkin);
    private Uri checkout = Uri.parse("android.resource://mobaappworks.com.sgquick/" + R.drawable.fragment_checkout);
    private Uri addToPipeline = Uri.parse("android.resource://mobaappworks.com.sgquick/" + R.drawable.fragment_addpipe);

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        FontChangeCrawler fontChanger = new FontChangeCrawler(this.context.getAssets(), Constant.FONT_FSELLIOT_REG);
        fontChanger.replaceFonts((ViewGroup) this.getView());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_accounts_existing_prod, container, false);
        Bundle args = getArguments();
        realm = Realm.getDefaultInstance();
        context = rootView.getContext();
        id = args.getInt(Constant.ID);

        btnCheckIn = (ImageButton) rootView.findViewById(R.id.btn_check_in);
        btnCheckIn.setImageURI(checkin);
        btnCheckIn.setBackgroundColor(Color.parseColor("#333333"));
        btnCheckIn.setOnClickListener(this);

        btnAddPipeline = (ImageButton) rootView.findViewById(R.id.btn_add_pipeline);
        btnAddPipeline.setImageURI(addToPipeline);
        btnAddPipeline.setBackgroundColor(Color.parseColor("#333333"));
        btnAddPipeline.setOnClickListener(this);

        if(isCheckedIn())
            btnCheckIn.setImageURI(checkout);
        else
            btnCheckIn.setImageURI(checkin);

        AccountDetailsEntity details = getDetails(args.getInt(Constant.ID));

        mtPostpaidLines = (TextView) rootView.findViewById(R.id.mt_pp_line);
        mtPostpaidMsf = (TextView) rootView.findViewById(R.id.mt_pp_msf);
        mtPostpaidRev = (TextView) rootView.findViewById(R.id.mt_pp_rev);
        bbLine = (TextView) rootView.findViewById(R.id.bb_line);
        bbRev = (TextView) rootView.findViewById(R.id.bb_rev);
        bbMsf = (TextView) rootView.findViewById(R.id.bb_msf);
        solLine = (TextView) rootView.findViewById(R.id.sol_line);
        solRev = (TextView) rootView.findViewById(R.id.sol_rev);
        solMsf = (TextView) rootView.findViewById(R.id.sol_msf);
        vLine = (TextView) rootView.findViewById(R.id.v_line);
        vRev = (TextView) rootView.findViewById(R.id.v_rev);
        vMsf = (TextView) rootView.findViewById(R.id.v_msf);
        nomLine = (TextView) rootView.findViewById(R.id.nom_line);
        nomMsf = (TextView) rootView.findViewById(R.id.nom_msf);
        nomRev = (TextView) rootView.findViewById(R.id.nom_rev);
        dataLine = (TextView) rootView.findViewById(R.id.data_line);
        dataRev = (TextView) rootView.findViewById(R.id.data_rev);
        dataMsf = (TextView) rootView.findViewById(R.id.data_msf);


        if(details != null) {
            mtPostpaidLines.setText(details.getDataPostpaidLines());
            mtPostpaidMsf.setText(details.getDataPostpaidMSF());
            mtPostpaidRev.setText(details.getDataPostpaidBilled());
            bbLine.setText(details.getFixedBbPostpaidLines());
            bbRev.setText(details.getFixedBbPostpaidBilled());
            bbMsf.setText(details.getFixedBbPostpaidMSF());
            solLine.setText(details.getSolutionsLines());
            solRev.setText(details.getSolutionsBilled());
            solMsf.setText(details.getSolutionsMSF());
            vLine.setText(details.getAvgMonthlyAcquiLines());
            vRev.setText(details.getAvgMonthlyAcquiBilled());
            vMsf.setText(details.getAvgMonthlyAcquiMSF());
            nomLine.setText(details.getNomadicPostpaidLines());
            nomMsf.setText(details.getNomadicPostpaidMSF());
            nomRev.setText(details.getNomadicPostpaidBilled());
            dataLine.setText(details.getDataPostpaidLines());
            dataRev.setText(details.getDataPostpaidBilled());
            dataMsf.setText(details.getDataPostpaidMSF());

        }
        return rootView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_check_in:
                Intent checkinIntent = new Intent(context, AccountsCheckInPopUp.class);
                checkinIntent.putExtra(Constant.ID, id);
                startActivity(checkinIntent);
                break;
            case R.id.btn_add_pipeline:
                Intent aap = new Intent(context, AccountsAddPipeline.class);
                aap.putExtra(Constant.ID, id);
                startActivity(aap);
                break;
        }
    }

    public AccountDetailsEntity getDetails(int id){
        return realm.where(AccountDetailsEntity.class)
                .equalTo("accountId.id", id)
                .findFirst();
    }

    public boolean isCheckedIn(){
        RealmResults<CheckInOutEntity> cioes = realm.where(CheckInOutEntity.class)
                .equalTo("accountId", String.valueOf(id))
                .equalTo("checkoutLng", "")
                .equalTo("checkoutLat", "")
                .findAll();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String today = sdf.format(new Date());
        for (CheckInOutEntity  checkInOutEntity: cioes) {
            String checkin = sdf.format(checkInOutEntity.getCheckinDate());
            if (checkin.equals(today))
                return true;
        }
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("ON RESUME", "EP");
        if(isCheckedIn())
            btnCheckIn.setImageURI(checkout);
        else
            btnCheckIn.setImageURI(checkin);
    }
}