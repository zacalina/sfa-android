package mobaappworks.com.sgquick.accounts;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.internal.widget.AdapterViewCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mobaappworks.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmResults;
import mobaappworks.com.sgquick.Constant;
import mobaappworks.com.sgquick.FontChangeCrawler;
import mobaappworks.com.sgquick.HttpManager;
import mobaappworks.com.sgquick.Util;
import mobaappworks.com.sgquick.entity.AccountEntity;
import mobaappworks.com.sgquick.entity.BarangayEntity;
import mobaappworks.com.sgquick.entity.CheckInOutEntity;
import mobaappworks.com.sgquick.entity.CityTownEntity;
import mobaappworks.com.sgquick.entity.CoveragePlanEntity;
import mobaappworks.com.sgquick.entity.IndustryTypeEntity;
import mobaappworks.com.sgquick.entity.ProvinceEntity;
import mobaappworks.com.sgquick.entity.RegionEntity;

public class AccountsFragmentInfo extends Fragment implements View.OnClickListener, TextWatcher, AdapterView.OnItemSelectedListener {
    private ImageButton btnShowPhoto;
    private ImageButton btnCheckIn;
    private ImageButton btnSave;
    private ImageButton btnAddPipeline;
    private ImageButton btnAddToCalendar;
    private ImageButton btnViewInMap;
    private View rootView;

    //    private TextView dateAssigned;
    private EditText floor;
    private EditText unit;
    private EditText building;
    private EditText streetNumber;
    private EditText streetName;
    private EditText accountAddress;
    //    private EditText email;


    private Spinner barangay;
    private Spinner cityTown;
    private Spinner province;
    private Spinner region;
    private Spinner IndustryType;
//    private Spinner zipcode;
//    private Spinner salesRegion;
//    private Spinner gbuTag;
//    private Spinner accountClass;
//    private Spinner segment;


    private int industryCtr;
    private int ctrRegion;
    private Integer selRegion;
    private int ctrProvince;
    private Integer selProvince;
    private int ctrCityTown;
    private Integer selCityTown;
    private int ctrBarangay;
    private Integer selBarangay;

    private int ctrInd;

    private EditText schedule;
//    private EditText dateOfLastVisit;
//    private EditText mainBranch;
    private EditText numOfEmployees;
    private EditText tier;
    private EditText secRanking;
//    private EditText numOfBranches;
//    private EditText anniversary;
//    private EditText webSite;
//    private EditText social;
//    private EditText remarks;
//    private EditText registeredAccountName;
//    private EditText lfo;
//    private EditText hfo;

//    private EditText originalName;
//    private EditText longitude;
//    private EditText latitude;
//    private EditText cleanedName;
//
//    private RadioButton untappedYes;
//    private RadioButton untappedNo;
//    private RadioGroup radioGroup;

    private Realm realm;
    private int id;
    private boolean isAccountList;
    private Context context;

    private AccountEntity accountDetails;
    private  CoveragePlanEntity coveragePlan;


    private Uri checkin = Uri.parse("android.resource://acqui.mobaappworks.com.sgquick/" + R.drawable.fragment_checkin);
    private Uri checkout = Uri.parse("android.resource://acqui.mobaappworks.com.sgquick/" + R.drawable.fragment_checkout);
    private Uri save = Uri.parse("android.resource://acqui.mobaappworks.com.sgquick/" + R.drawable.fragment_save);
    private Uri addToPipeline = Uri.parse("android.resource://acqui.mobaappworks.com.sgquick/" + R.drawable.fragment_addpipe);
    private Uri addToCalendar = Uri.parse("android.resource://acqui.mobaappworks.com.sgquick/" + R.drawable.fragment_addcal);
    private Uri map = Uri.parse("android.resource://acqui.mobaappworks.com.sgquick/" + R.drawable.fragment_map);

//    private Uri checkin = Uri.parse("android.resource://training.mobaappworks.com.sgquick/" + R.drawable.fragment_checkin);
//    private Uri checkout = Uri.parse("android.resource://training.mobaappworks.com.sgquick/" + R.drawable.fragment_checkout);
//    private Uri save = Uri.parse("android.resource://training.mobaappworks.com.sgquick/" + R.drawable.fragment_save);
//    private Uri addToPipeline = Uri.parse("android.resource://training.mobaappworks.com.sgquick/" + R.drawable.fragment_addpipe);
//    private Uri addToCalendar = Uri.parse("android.resource://training.mobaappworks.com.sgquick/" + R.drawable.fragment_addcal);
//    private Uri map = Uri.parse("android.resource://training.mobaappworks.com.sgquick/" + R.drawable.fragment_map);


    private Map<String,  IndustryTypeEntity> industryMap;
    private Map<String, Integer> mapRegion;
    private Map<String, Integer> mapProvince;
    private Map<String, Integer> mapCity;
    private Map<String, Integer> mapBarangay;


    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        FontChangeCrawler fontChanger = new FontChangeCrawler(this.context.getAssets(), Constant.FONT_FSELLIOT_REG);
        fontChanger.replaceFonts((ViewGroup) this.getView());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_accounts_info, container, false);
        Bundle args = getArguments();
        id = args.getInt(Constant.ID);
        isAccountList = args.getBoolean(Constant.IS_ACCOUNT_LIST);
        realm = Realm.getDefaultInstance();
        context = rootView.getContext();

        industryMap = new HashMap<>();

        btnCheckIn = (ImageButton) rootView.findViewById(R.id.btn_check_in);
        btnCheckIn.setImageURI(checkin);
        btnCheckIn.setBackgroundColor(Color.parseColor("#333333"));
        btnCheckIn.setOnClickListener(this);

        btnSave = (ImageButton) rootView.findViewById(R.id.btn_save);
        btnSave.setBackgroundColor(Color.parseColor("#333333"));
        btnSave.setImageURI(save);
        btnSave.setOnClickListener(this);

        btnAddPipeline = (ImageButton) rootView.findViewById(R.id.btn_add_pipeline);
        btnAddPipeline.setBackgroundColor(Color.parseColor("#333333"));
        btnAddPipeline.setImageURI(addToPipeline);
        btnAddPipeline.setOnClickListener(this);

        btnAddToCalendar = (ImageButton) rootView.findViewById(R.id.btn_add_to_calendar);
        btnAddToCalendar.setBackgroundColor(Color.parseColor("#333333"));
        btnAddToCalendar.setImageURI(addToCalendar);
        btnAddToCalendar.setOnClickListener(this);

        btnViewInMap = (ImageButton) rootView.findViewById(R.id.btn_view_in_map);
        btnViewInMap.setBackgroundColor(Color.parseColor("#333333"));
        btnViewInMap.setImageURI(map);
        btnViewInMap.setOnClickListener(this);

        if(isCheckedIn())
            btnCheckIn.setImageURI(checkout);
        else
            btnCheckIn.setImageURI(checkin);

        IndustryType = (Spinner) rootView.findViewById(R.id.sp_industry_type);
//
        accountDetails = getAccountDetails(id);
        coveragePlan = realm.where(CoveragePlanEntity.class)
                .equalTo("accountIdFk", String.valueOf(accountDetails.getAccountId())).findFirst();

//        dateAssigned = (TextView) rootView.findViewById(R.id.date_assigned);
//        dateAssigned.setText(accountDetails.getDateAssigned());

        unit = (EditText) rootView.findViewById(R.id.unit);
        floor = (EditText) rootView.findViewById(R.id.floor);
        building = (EditText) rootView.findViewById(R.id.building);
        streetNumber = (EditText) rootView.findViewById(R.id.street_number);
        streetName = (EditText) rootView.findViewById(R.id.street_name);
        barangay = (Spinner) rootView.findViewById(R.id.barangay);
        cityTown = (Spinner) rootView.findViewById(R.id.city_town);
        province = (Spinner) rootView.findViewById(R.id.province);
        region = (Spinner) rootView.findViewById(R.id.region);
        IndustryType = (Spinner) rootView.findViewById(R.id.sp_industry_type);
        accountAddress = (EditText) rootView.findViewById(R.id.account_address);
        schedule = (EditText) rootView.findViewById(R.id.schedule);
        schedule.setOnClickListener(this);
        cityTown.setOnItemSelectedListener(this);
        province.setOnItemSelectedListener(this);
        region.setOnItemSelectedListener(this);


//        zipcode = (Spinner) rootView.findViewById(R.id.zipcode);
//        registeredAccountName = (EditText) rootView.findViewById(R.id.et_registered_name);
//        dateAssigned = (EditText) rootView.findViewById(R.id.et_date_assigned);
//        lfo = (EditText) rootView.findViewById(R.id.et_lfo);
//        hfo = (EditText) rootView.findViewById(R.id.et_hfo);
//        salesRegion = (Spinner) rootView.findViewById(R.id.sp_sales_region);

//        dateOfLastVisit = (EditText) rootView.findViewById(R.id.date_of_last_visit);
        numOfEmployees = (EditText) rootView.findViewById(R.id.et_number_of_employees);
        tier = (EditText) rootView.findViewById(R.id.et_tier);
        secRanking = (EditText) rootView.findViewById(R.id.et_sec_ranking);
//        anniversary = (EditText) rootView.findViewById(R.id.anniversary);
//        registeredAccountName = (EditText) rootView.findViewById(R.id.et_registered_name);
//        lfo = (EditText) rootView.findViewById(R.id.et_lfo);
//        hfo = (EditText) rootView.findViewById(R.id.et_hfo);
//        gbuTag = (Spinner) rootView.findViewById(R.id.sp_gbu_tag);
//        accountClass = (Spinner) rootView.findViewById(R.id.sp_account_class);
//        originalName = (EditText) rootView.findViewById(R.id.et_original_name);
//        longitude = (EditText) rootView.findViewById(R.id.et_long);
//        latitude = (EditText) rootView.findViewById(R.id.et_lat);
//        segment = (Spinner) rootView.findViewById(R.id.sp_segment);
//        cleanedName = (EditText) rootView.findViewById(R.id.et_cleaned_name);

        try {
            schedule.setHint(Constant.SDF_DATE.format(new Date(coveragePlan.getScheduledDateTime())));
        }catch (Exception e){
            e.printStackTrace();
        }

        unit.setText(accountDetails.getUnitNumber());
        floor.setText(accountDetails.getFloorNumber());
        building.setText(accountDetails.getBuildingName());
        streetNumber.setText(accountDetails.getStreetNumber());
        streetName.setText(accountDetails.getStreetName());
        accountAddress.setText(accountDetails.getAccountAddress());

//        zipcode
//        registeredAccountName.setHint(accountDetails.getRegisteredAccountName());
//        dateAssigned.setHint(accountDetails.getDateAssigned());
//        lfo.setHint(accountDetails.getLowestFloorOccupied());
//        hfo.setHint(accountDetails.getHighestFloorOccupied());
//        salesRegion

//        dateOfLastVisit.setHint(accountDetails.getDateOfLastVisit());
        numOfEmployees.setText(String.valueOf(accountDetails.getNumberOfEmployees()));
        numOfEmployees.setEnabled(false);
        tier.setText(String.valueOf(accountDetails.getTier()));
        tier.setEnabled(false);
        secRanking.setText(String.valueOf(accountDetails.getSecRanking()));
        secRanking.setEnabled(false);
//        anniversary.setHint(accountDetails.getAnniversary());
//        registeredAccountName.setHint(accountDetails.getRegisteredAccountName());
////        gbuTag
////        accountClass
//        originalName.setHint(accountDetails.getOriginalName());
//        longitude.setHint(accountDetails.getLongitude());
//        latitude.setHint(accountDetails.getLatitude());
////        segment
//        cleanedName.setHint(accountDetails.getCleanedName());

        unit.addTextChangedListener(this);
        floor.addTextChangedListener(this);
        building.addTextChangedListener(this);
        streetNumber.addTextChangedListener(this);
        streetName.addTextChangedListener(this);

        mapRegion = new HashMap<>();
        mapProvince = new HashMap<>();
        mapCity = new HashMap<>();
        mapBarangay = new HashMap<>();

        setSpinnerItems();


        //IndustryType.setSelection(industryCtr);
        IndustryType.setEnabled(false);
        if(accountDetails.getIndustryTypeIdFk()==1){
            IndustryType.setSelection(1);
        }
        else if(accountDetails.getIndustryTypeIdFk()==2){
            IndustryType.setSelection(2);
        }
        else if(accountDetails.getIndustryTypeIdFk()==3){
            IndustryType.setSelection(3);
        }
        else if(accountDetails.getIndustryTypeIdFk()==4){
            IndustryType.setSelection(4);
        }
        else if(accountDetails.getIndustryTypeIdFk()==5){
            IndustryType.setSelection(5);
        }
        else if(accountDetails.getIndustryTypeIdFk()==6){
            IndustryType.setSelection(6);
        }
        else if(accountDetails.getIndustryTypeIdFk()==7){
            IndustryType.setSelection(7);
        }
        else if(accountDetails.getIndustryTypeIdFk()==8){
            IndustryType.setSelection(8);
        }
        else if(accountDetails.getIndustryTypeIdFk()==9){
            IndustryType.setSelection(9);
        }
        else if(accountDetails.getIndustryTypeIdFk()==10){
            IndustryType.setSelection(10);
        }
        else if(accountDetails.getIndustryTypeIdFk()==11){
            IndustryType.setSelection(11);
        }
        else if(accountDetails.getIndustryTypeIdFk()==12){
            IndustryType.setSelection(12);
        }
        else if(accountDetails.getIndustryTypeIdFk()==13){
            IndustryType.setSelection(13);
        }
        else if(accountDetails.getIndustryTypeIdFk()==14){
            IndustryType.setSelection(14);
        }
        else if(accountDetails.getIndustryTypeIdFk()==15){
            IndustryType.setSelection(15);
        }
        else if(accountDetails.getIndustryTypeIdFk()==16){
            IndustryType.setSelection(16);
        }
        else if(accountDetails.getIndustryTypeIdFk()==17){
            IndustryType.setSelection(17);
        }
        else if(accountDetails.getIndustryTypeIdFk()==18){
            IndustryType.setSelection(18);
        }
        else if(accountDetails.getIndustryTypeIdFk()==19){
            IndustryType.setSelection(19);
        }
        else if(accountDetails.getIndustryTypeIdFk()==20){
            IndustryType.setSelection(20);
        }
        else if(accountDetails.getIndustryTypeIdFk()==21){
            IndustryType.setSelection(21);
        }
        else{
            IndustryType.setSelection(0);
        }

        if (isAccountList){
            accountListElementControls();
        }

        return rootView;
    }

    public void launch(View view){
        System.out.println("Jc Gofredo 123");
    }

    @Override
    public void onClick(View view) {

        Intent intent;
        switch (view.getId()){
//            case R.id.btn_show_photo:
//                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
//                photoPickerIntent.setType("image/*");
//                startActivity(photoPickerIntent);
//                break;

            case R.id.btn_check_in:
                intent = new Intent(context,AccountsCheckInPopUp.class);
                intent.putExtra(Constant.ID, id);
                startActivity(intent);
                break;

            case R.id.btn_add_pipeline:
                intent = new Intent(context, AccountsAddPipeline.class);
                intent.putExtra(Constant.ID, id);
                startActivity(intent);
                break;

            case R.id.btn_add_to_calendar:
                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(coveragePlan.getScheduledDateTime());

                intent = new Intent(Intent.ACTION_EDIT);
                intent.setType("vnd.android.cursor.item/event");
                intent.putExtra("beginTime", cal.getTimeInMillis());
//                intent.putExtra("allDay", true);
//                intent.putExtra("rrule", "FREQ=YEARLY");
                intent.putExtra("endTime", cal.getTimeInMillis()+60*60*1000);
                intent.putExtra("title", accountDetails.getAccountName());
                startActivity(intent);
                break;

            case R.id.btn_view_in_map:
                intent = new Intent(context, ViewInMapActivity.class);
                intent.putExtra(Constant.ID, accountDetails.getAccountId());
                startActivity(intent);
                break;

            case R.id.schedule:
                showDialog(0).show();
                break;

            case R.id.btn_save:

                realm.beginTransaction();
                String today = Constant.SDF_API.format(new Date());
//                accountDetails.setDateAssigned("".equals(dateAssigned.getText().toString()) ? dateAssigned.getHint().toString() : dateAssigned.getText().toString());

                if(region.getCount()==0){
                    accountDetails.setRegionIdFk(0);
                }
                else{
                    accountDetails.setRegionIdFk(getRegion(region.getSelectedItem().toString()));
                }
                if(province.getCount()==0){
                    accountDetails.setProvinceIdFk(0);
                }
                else{
                    accountDetails.setProvinceIdFk(getProvince(province.getSelectedItem().toString()));
                }
                if(cityTown.getCount()==0){
                    accountDetails.setCityTownIdFk(0);
                }
                else{
                    accountDetails.setCityTownIdFk(getCity(cityTown.getSelectedItem().toString()));
                }
                if(barangay.getCount()==0){
                    accountDetails.setBarangayIdFk(0);
                }
                else{
                    accountDetails.setBarangayIdFk(getBarangay(barangay.getSelectedItem().toString()));
                }


                String prodItem = (String) IndustryType.getSelectedItem();


                if(prodItem.equals(" ") || prodItem.equals(0) || prodItem.isEmpty()){
                    accountDetails.setIndustryTypeIdFk(0);
                }
                else{
                    accountDetails.setIndustryTypeIdFk(industryMap.get(prodItem).getId());
                }
//
//                accountDetails.setRegionIdFk(getRegion(region.getSelectedItem().toString()));
//                accountDetails.setProvinceIdFk(getProvince(province.getSelectedItem().toString()));
//                accountDetails.setCityTownIdFk(getCity(cityTown.getSelectedItem().toString()));
//                accountDetails.setBarangayIdFk(getBarangay(barangay.getSelectedItem().toString()));
                if(unit.getText().toString().trim().equals("")){
                    accountDetails.setUnitNumber(" ");
                }
                else {
                    accountDetails.setUnitNumber("".equals(unit.getText().toString()) ? unit.getHint().toString() : unit.getText().toString());
                }

                if(floor.getText().toString().trim().equals("")){
                    accountDetails.setFloorNumber(" ");
                }
                else {
                    accountDetails.setFloorNumber("".equals(floor.getText().toString()) ? floor.getHint().toString() : floor.getText().toString());
                }

                if(building.getText().toString().trim().equals("")){
                    accountDetails.setBuildingName(" ");
                }
                else {
                    accountDetails.setBuildingName("".equals(building.getText().toString()) ? building.getHint().toString() : building.getText().toString());
                }

                if(streetNumber.getText().toString().trim().equals("")){
                    accountDetails.setStreetNumber(" ");
                }
                else {
                    accountDetails.setStreetNumber("".equals(streetNumber.getText().toString()) ? streetNumber.getHint().toString() : streetNumber.getText().toString());
                }

                if(streetNumber.getText().toString().trim().equals("")){
                    accountDetails.setStreetNumber(" ");
                }
                else {
                    accountDetails.setStreetNumber("".equals(streetNumber.getText().toString()) ? streetNumber.getHint().toString() : streetNumber.getText().toString());
                }

                if(streetName.getText().toString().trim().equals("")){
                    accountDetails.setStreetName(" ");
                }
                else {
                    accountDetails.setStreetName("".equals(streetName.getText().toString()) ? streetName.getHint().toString() : streetName.getText().toString());
                }

                if(numOfEmployees.getText().toString().trim().equals("")){
                    accountDetails.setNumberOfEmployees(" ");
                }
                else {
                    accountDetails.setNumberOfEmployees("".equals(numOfEmployees.getText().toString()) ? numOfEmployees.getHint().toString() : numOfEmployees.getText().toString());
                }

                if(tier.getText().toString().trim().equals("")){
                    accountDetails.setTier(" ");
                }
                else {
                    accountDetails.setTier("".equals(tier.getText().toString()) ? tier.getHint().toString() : tier.getText().toString());
                }

                if(secRanking.getText().toString().trim().equals("")){
                    accountDetails.setSecRanking(" ");
                }
                else {
                    accountDetails.setSecRanking("".equals(secRanking.getText().toString()) ? secRanking.getHint().toString() : secRanking.getText().toString());
                }

                if(accountAddress.getText().toString().trim().equals("")){
                    accountDetails.setAccountAddress(" ");
                }
                else {
                    accountDetails.setAccountAddress("".equals(accountAddress.getText().toString()) ? accountAddress.getHint().toString() : accountAddress.getText().toString());
                }
                //accountDetails.setUnitNumber("".equals(unit.getText().toString()) ? unit.getHint().toString() : unit.getText().toString());
                //accountDetails.setFloorNumber("".equals(floor.getText().toString()) ? floor.getHint().toString() : floor.getText().toString());
               // accountDetails.setBuildingName("".equals(building.getText().toString()) ? building.getHint().toString() : building.getText().toString());
               // accountDetails.setStreetNumber("".equals(streetNumber.getText().toString()) ? streetNumber.getHint().toString() : streetNumber.getText().toString());
               // accountDetails.setStreetName("".equals(streetName.getText().toString()) ? streetName.getHint().toString() : streetName.getText().toString());

//                accountDetails.setEmailAddress("".equals(email.getText().toString()) ? email.getHint().toString() : email.getText().toString());
//                accountDetails.setCityTownId();city.getText().toString();
//                province.getText().toString();
//                region.getText().toString();
//                accountDetails.setDateOfLastVisit("".equals(dateOfLastVisit.getText().toString()) ? dateOfLastVisit.getHint().toString() : dateOfLastVisit.getText().toString());
//                accountDetails.setIsInTheMainBranch("".equals(mainBranch.getText().toString()) ? mainBranch.getHint().toString() : mainBranch.getText().toString());
//                accountDetails.setNumberOfEmployees(Integer.valueOf("".equals(numOfEmployees.getText().toString()) ? numOfEmployees.getHint().toString() : numOfEmployees.getText().toString()));
//                accountDetails.setNumberOfEmployees("".equals(numOfEmployees.getText().toString()) ? numOfEmployees.getHint().toString() : numOfEmployees.getText().toString());
//                accountDetails.setTier("".equals(tier.getText().toString()) ? tier.getHint().toString() : tier.getText().toString());
//                accountDetails.setSecRanking("".equals(secRanking.getText().toString()) ? secRanking.getHint().toString() : secRanking.getText().toString());
//                accountDetails.setNumberOfBranches(Integer.valueOf("".equals(numOfBranches.getText().toString()) ? numOfBranches.getHint().toString() : numOfBranches.getText().toString()));
//                accountDetails.setAnniversary("".equals(anniversary.getText().toString()) ? anniversary.getHint().toString() : anniversary.getText().toString());
//                accountDetails.setWebsite("".equals(webSite.getText().toString()) ? webSite.getHint().toString() : webSite.getText().toString());
//                accountDetails.setFacebookPage("".equals(social.getText().toString()) ? social.getHint().toString() : social.getText().toString());
//                accountDetails.setRemarks("".equals(remarks.getText().toString()) ? remarks.getHint().toString() : remarks.getText().toString());
                realm.commitTransaction();
                //Toast.makeText(context, "Info Saved", Toast.LENGTH_SHORT).show();
                msgInfoSaved();

                SimpleDateFormat kamote_sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sssZ");
                try {
                    Date mypld = kamote_sdf.parse(today);
                JSONObject accountDetailsJson = new JSONObject();
                try {
                    accountDetailsJson.put("id", accountDetails.getId());
                    accountDetailsJson.put("accountId", accountDetails.getAccountId());
                    accountDetailsJson.put("accountName", accountDetails.getAccountName());
                    accountDetailsJson.put("registeredAccountName", accountDetails.getRegisteredAccountName());
                    accountDetailsJson.put("employeeId", accountDetails.getEmployeeId());
                    accountDetailsJson.put("accountAddress", accountDetails.getAccountAddress());
                    accountDetailsJson.put("unitNumber", accountDetails.getUnitNumber());
                    accountDetailsJson.put("floorNumber", accountDetails.getFloorNumber());
                    accountDetailsJson.put("lowestFloorOccupied", accountDetails.getLowestFloorOccupied());
                    accountDetailsJson.put("highestFloorOccupied", accountDetails.getHighestFloorOccupied());
                    accountDetailsJson.put("buildingName", accountDetails.getBuildingName());
                    accountDetailsJson.put("streetNumber", accountDetails.getStreetNumber());
                    accountDetailsJson.put("streetName", accountDetails.getStreetName());
                    accountDetailsJson.put("barangayIdFk", accountDetails.getBarangayIdFk());
                    accountDetailsJson.put("cityTownIdFk", accountDetails.getCityTownIdFk());
                    accountDetailsJson.put("provinceIdFk", accountDetails.getProvinceIdFk());
                    accountDetailsJson.put("regionIdFk", accountDetails.getRegionIdFk());
                    accountDetailsJson.put("salesRegionIdFk", accountDetails.getSalesRegionIdFk());
                    accountDetailsJson.put("secRanking", accountDetails.getSecRanking());
                    accountDetailsJson.put("tier", accountDetails.getTier());
                    accountDetailsJson.put("gbuTagidFk", accountDetails.getGbuTagidFk());
                    accountDetailsJson.put("industryTypeIdFk", accountDetails.getIndustryTypeIdFk());
                    accountDetailsJson.put("accountClassIdFk", accountDetails.getAccountClassIdFk());
                    accountDetailsJson.put("numberOfEmployees", accountDetails.getNumberOfEmployees());
                    accountDetailsJson.put("segmentIdFk", accountDetails.getSegmentIdFk());
                    accountDetailsJson.put("anniversary", accountDetails.getAnniversary());
                    accountDetailsJson.put("originalName", accountDetails.getOriginalName());
                    accountDetailsJson.put("cleanedName", accountDetails.getCleanedName());
                    accountDetailsJson.put("longitude", accountDetails.getLongitude());
                    accountDetailsJson.put("latitude", accountDetails.getLatitude());
                    accountDetailsJson.put("isTapped", accountDetails.isTapped());
                    accountDetailsJson.put("contactNumber", accountDetails.getContactNumber());
                    accountDetailsJson.put("zipCode", accountDetails.getZipCode());
                    accountDetailsJson.put("acCheckoutDate", mypld);

                    new AccountsInfoAsyncTask().execute(Constant.UPDATE_ACCOUNT(context), accountDetailsJson.toString(), "PUT");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                }catch (ParseException ex) {
                    Log.d("Error : ", "error date check in out");
                }

                break;
        }
    }

    public void msgInfoSaved(){
        //Toast.makeText(getApplicationContext(), "Amount Cannot be NULL or ZERO", Toast.LENGTH_SHORT).show();
        //start dialog
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        //alertDialog.setTitle("Alert");
        alertDialog.setMessage("Account Info Successfully Saved");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        //alertDialog.setTitle("INFO");
        alertDialog.show();
    }

    private DatePickerDialog.OnDateSetListener mDateSetListener =
            new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker view, int year,
                                      int monthOfYear, int dayOfMonth) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(Calendar.YEAR, year);
                    calendar.set(Calendar.MONTH, monthOfYear);
                    calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    schedule.setText(Constant.SDF_DATE.format(calendar.getTime()));

                    realm.beginTransaction();
                    coveragePlan.setScheduledDateTime(calendar.getTimeInMillis());
                    realm.copyToRealmOrUpdate(coveragePlan);
                    realm.commitTransaction();

                    JSONObject object = new JSONObject();

                    try {
                        object.put("id",coveragePlan.getId());
                        object.put("accountIdFk",accountDetails.getAccountId());
                        object.put("scheduledDateTime", Constant.SDF_API.format(calendar.getTime()));
                        object.put("status", "re-scheduled");
                        object.put("employeeId", Util.getUserId());
                        new AccountsInfoAsyncTask().execute(Constant.UPDATE_SCHEDULE(context), object.toString(), "POST");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            };

    protected Dialog showDialog(int id) {
        switch (id) {
            case 0:
                Calendar calendar = Calendar.getInstance();
                Calendar calFromRange = Calendar.getInstance();
                Calendar calToRange = Calendar.getInstance();

                Date date = new Date(coveragePlan.getScheduledDateTime());
                calendar.setTime(date);

                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        context,
                        3,
                        mDateSetListener,
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH));

                calToRange.set(Calendar.YEAR, calToRange.get(Calendar.YEAR) + 10);

                datePickerDialog.getDatePicker().setMaxDate(calToRange.getTimeInMillis());
                datePickerDialog.getDatePicker().setMinDate(calFromRange.getTimeInMillis());

                return datePickerDialog;
        }
        return null;
    }

//    public List<String> getIndustryType(){
//        List<String> industryTypes = new ArrayList<>();
//        RealmResults<IndustryTypeEntity> result = realm.where(IndustryTypeEntity.class).findAll();
//        for (IndustryTypeEntity ite : result) {
//            industryTypes.add(ite.getName());
//        }
//        return industryTypes;
//    }

    public List<String> getIndustryType(){
        int ctrinner = 0;
        industryCtr = 0;
        List<String> prods = new ArrayList<>();
        prods.add(" ");
        RealmResults<IndustryTypeEntity> result = realm.where(IndustryTypeEntity.class).findAll();
        for (IndustryTypeEntity myite : result) {
            prods.add(myite.getName());
            industryMap.put(myite.getName(), myite);
            if(myite.getId() == accountDetails.getIndustryTypeIdFk() )
                setProdSelected(ctrinner);
            ctrinner++;
        }
        return prods;
    }

    public void setProdSelected(int i){
        industryCtr = i;
    }



    public List<String> getBarangays(Integer ctId){

        if(ctId == -1){
            ctId = accountDetails.getCityTownIdFk();
        }

        ctrBarangay = 0;
        selBarangay = 0;
        List<String> data = new ArrayList<>();
//        RealmResults<BarangayEntity> result = realm.where(BarangayEntity.class).findAll();
        RealmResults<BarangayEntity> result = realm.where(BarangayEntity.class).equalTo("cityTownIdFk", ctId).findAll();
        for (BarangayEntity ite : result) {
            data.add(ite.getName());
            mapBarangay.put(ite.getName(), ite.getId());
            if(accountDetails.getBarangayIdFk() == ite.getId()){
                selBarangay = ctrBarangay;
            }
            ctrBarangay++;
        }
        return data;
    }

    public List<String> getCities(Integer provId){

        if(provId == -1){
            provId = accountDetails.getProvinceIdFk();
        }

        ctrCityTown = 0;
        selCityTown = 0;
        List<String> data = new ArrayList<>();
        data.add(" ");
//        RealmResults<CityTownEntity> result = realm.where(CityTownEntity.class).findAll();
        RealmResults<CityTownEntity> result = realm.where(CityTownEntity.class).equalTo("provinceIdFk", provId).findAll();
        for (CityTownEntity ite : result) {
            data.add(ite.getName());
            mapCity.put(ite.getName(), ite.getId());
            if(accountDetails.getCityTownIdFk() == ite.getId()){
                selCityTown = ctrCityTown + 1;
            }
            ctrCityTown++;
        }
        return data;
    }

    public List<String> getProvices(Integer regId){

        if(regId == -1){
            regId = accountDetails.getRegionIdFk();
        }

        ctrProvince = 0;
        selProvince = 0;
        List<String> data = new ArrayList<>();
        data.add(" "); // added 3/3/2016
//        RealmResults<ProvinceEntity> result = realm.where(ProvinceEntity.class).findAll();
        RealmResults<ProvinceEntity> result = realm.where(ProvinceEntity.class).equalTo("regionIdFk", regId).findAll();
        for (ProvinceEntity ite : result) {
            data.add(ite.getName());
            mapProvince.put(ite.getName(), ite.getId());
            if (accountDetails.getProvinceIdFk() == ite.getId()){
                selProvince = ctrProvince + 1;
            }
            ctrProvince++;
        }
        return data;
    }

    public List<String> getRegion(){
        ctrRegion = 0;
        selRegion = 0;
        List<String> data = new ArrayList<>();
        data.add(" "); //added 3/3/2016
        RealmResults<RegionEntity> result = realm.where(RegionEntity.class).findAll();
        for (RegionEntity ite : result) {
            data.add(ite.getName());
            mapRegion.put(ite.getName(), ite.getId());
            if(accountDetails.getRegionIdFk() == ite.getId()){
                selRegion = ctrRegion + 1;
            }
            ctrRegion++;
        }
        return data;
    }


    public Integer getBarangay(String name){
        BarangayEntity result = realm.where(BarangayEntity.class).equalTo("name", name).findFirst();
        return result.getId();
    }


    public Integer getCity(String name){
            if(name.equals(" ") || name.isEmpty()){
                return 0;
            }
            else {
                CityTownEntity result = realm.where(CityTownEntity.class).equalTo("name", name).findFirst();
                return result.getId();
            }
    }

    public Integer getProvince(String name){
            if(name.equals(" ") || name.isEmpty()){
                return 0;
            }
            else {
                ProvinceEntity result = realm.where(ProvinceEntity.class).equalTo("name", name).findFirst();
                return result.getId();
            }
    }


    public Integer getRegion(String name){
            if(name.equals(" ") || name.isEmpty()) {
                return 0;
            }
            else {
                RegionEntity result = realm.where(RegionEntity.class).equalTo("name", name).findFirst();
                return result.getId();
            }
    }


    public void setSpinnerItems() {
        List<String> indTypeList = getIndustryType();
        ArrayAdapter<String> industryTypeAdapter = new ArrayAdapter<>(context,
                R.layout.spinner_item, indTypeList);
        IndustryType.setAdapter(industryTypeAdapter);



        //BARANGAY
        List<String> barList = getBarangays(-1);
        ArrayAdapter<String> barAdapter = new ArrayAdapter<>(context,
                R.layout.spinner_item, barList);
        barangay.setAdapter(barAdapter);
        barangay.setSelection(selBarangay, true);

        //CITY/TOWN
        List<String> cityList = getCities(-1);

        ArrayAdapter<String> cityAdapter = new ArrayAdapter<>(context,
                R.layout.spinner_item, cityList);
        cityTown.setAdapter(cityAdapter);
        cityTown.setSelection(selCityTown, true);

        //PROVINCE
        List<String> proviceList = getProvices(-1);

        ArrayAdapter<String> proviceAdapter = new ArrayAdapter<>(context,
                R.layout.spinner_item, proviceList);
        province.setAdapter(proviceAdapter);
        province.setSelection(selProvince, true);

        //REGION
        List<String> regionList = getRegion();

        ArrayAdapter<String> regionAdapter = new ArrayAdapter<>(context,
                R.layout.spinner_item, regionList);
        region.setAdapter(regionAdapter);
        region.setSelection(selRegion, true);

        if(barList.isEmpty() || cityList.isEmpty() || regionList.isEmpty()){
            accountAddress.setText(accountDetails.getAccountAddress());
        }
        else if(barList.equals("0") || cityList.equals("0") || regionList.equals("0")){
            accountAddress.setText(accountDetails.getAccountAddress());
        }
        else if(barList.equals(null) || cityList.equals(null) || regionList.equals(null)){
            accountAddress.setText(accountDetails.getAccountAddress());
        }
        else if(barList.equals(0) || cityList.equals(0) || regionList.equals(0)){
            accountAddress.setText(accountDetails.getAccountAddress());
        }
        else if(barList.isEmpty() || cityList.isEmpty() || regionList.isEmpty()){
            accountAddress.setText(accountDetails.getAccountAddress());
        } else {
            accountAddress.setText(accountDetails.getAccountAddress() + " " + barangay.getSelectedItem().toString() + " " + cityTown.getSelectedItem().toString() + " " + region.getSelectedItem().toString());
        }

    }


    public AccountEntity getAccountDetails(int id){
        AccountEntity result = realm.where(AccountEntity.class)
                .equalTo("accountId", id)
                .findFirst();
        return result;
    }

    public boolean isCheckedIn(){
        RealmResults<CheckInOutEntity> cioes = realm.where(CheckInOutEntity.class)
                .equalTo("accountId", String.valueOf(id))
                .equalTo("checkoutLng", "")
                .equalTo("checkoutLat", "")
                .findAll();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String today = sdf.format(new Date());
        for (CheckInOutEntity  checkInOutEntity: cioes) {
            String checkin = sdf.format(checkInOutEntity.getCheckinDate());
            if (checkin.equals(today))
                return true;
        }
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("ON RESUME", "INFO");
        if(isCheckedIn())
            btnCheckIn.setImageURI(checkout);
        else
            btnCheckIn.setImageURI(checkin);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


    }

    @Override
    public void afterTextChanged(Editable editable) {
        btnSave.setEnabled(true);


        }


    int check = 0;

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        check++;

        if (check > 3) {

            switch (adapterView.getId()) {
                case R.id.region:
                    //PROVINCE
                    List<String> proviceList = getProvices(getRegion(region.getSelectedItem().toString()));
                    ArrayAdapter<String> proviceAdapter = new ArrayAdapter<>(context,
                            R.layout.spinner_item, proviceList);
                    province.setAdapter(proviceAdapter);
                    province.setSelection(0);
                case R.id.province:
                    //CITY/TOWN
                    List<String> cityList = getCities(getProvince(province.getSelectedItem().toString()));
                    ArrayAdapter<String> cityAdapter = new ArrayAdapter<>(context,
                            R.layout.spinner_item, cityList);
                    cityTown.setAdapter(cityAdapter);
                    cityTown.setSelection(0);
                case R.id.city_town:
                    //BARANGAY
                    List<String> barListCtTwn = getBarangays(getCity(cityTown.getSelectedItem().toString()));
                    ArrayAdapter<String> barAdapter = new ArrayAdapter<>(context,
                            R.layout.spinner_item, barListCtTwn);
                    barangay.setAdapter(barAdapter);
                    barangay.setSelection(0);
                    break;
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    private class AccountsInfoAsyncTask extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPostExecute(String s) {
        }

        @Override
        protected String doInBackground(String... strings) {
            if("PUT".equals(strings[2])){
                return HttpManager.PUT(strings[0], strings[1], context);
            }
            return HttpManager.POST(strings[0], strings[1], context);
        }
    }

    private void accountListElementControls(){
        //schedule.setOnClickListener(null);
        region.setEnabled(true);
        province.setEnabled(true);
        cityTown.setEnabled(true);
        barangay.setEnabled(true);
        streetName.setEnabled(true);
        streetNumber.setEnabled(true);
        building.setEnabled(true);
        floor.setEnabled(true);
        unit.setEnabled(true);


        btnCheckIn.setVisibility(View.VISIBLE);
        btnSave.setVisibility(View.VISIBLE);
        btnAddPipeline.setVisibility(View.VISIBLE);
        btnAddToCalendar.setVisibility(View.VISIBLE);
    }
}