package mobaappworks.com.sgquick.accounts;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.mobaappworks.R;

import io.realm.RealmBaseAdapter;
import io.realm.RealmResults;
import mobaappworks.com.sgquick.Constant;
import mobaappworks.com.sgquick.entity.CompanyContactEntity;

/**
 * Created by ldeguzman on 9/12/15.
 */
public class ContactListAdapter extends RealmBaseAdapter<CompanyContactEntity> {

    public ContactListAdapter(Context context, RealmResults<CompanyContactEntity> realmResults) {
        super(context, realmResults, true);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyViewHolder viewHolder = new MyViewHolder();
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.activity_accounts_contact_list_view, parent, false);
        CompanyContactEntity companyContactEntity = realmResults.get(position);

        rowView.setTag(companyContactEntity.getId());

        viewHolder.tvName = (TextView) rowView.findViewById(R.id.firstLine);
        viewHolder.tvNumber = (TextView) rowView.findViewById(R.id.secondLine);
//        viewHolder.etEmail = (EditText) rowView.findViewById(R.id.et_email);
//        viewHolder.etBirthDate = (EditText) rowView.findViewById(R.id.et_birthday);
//        viewHolder.etInterest = (EditText) rowView.findViewById(R.id.et_interest);
//        viewHolder.etPreferences = (EditText) rowView.findViewById(R.id.et_pref);
//        viewHolder.etRole = (EditText) rowView.findViewById(R.id.et_role);
//
        viewHolder.tvName.setText(companyContactEntity.getName());
        viewHolder.tvNumber.setText(companyContactEntity.getContactNumber());
//        viewHolder.etEmail.setHint(companyContactEntity.getEmail());
//        viewHolder.etBirthDate.setHint(companyContactEntity.getBirthDate().toString());
////        viewHolder.etInterest.setHint(companyContactEntity.geti);
////        viewHolder.etPreferences.setHint(companyContactEntity.pref);
//        viewHolder.etRole.setHint(companyContactEntity.getCompanyContactRoleId().getRoleName());
        

        return rowView;
    }


    private class MyViewHolder{
        TextView tvName;
        TextView tvNumber;

//        EditText etName;
//        EditText etNumber;
//        EditText etEmail;
//        EditText etBirthDate;
//        EditText etInterest;
//        EditText etPreferences;
//        EditText etRole;
    }


}
