package mobaappworks.com.sgquick.accounts;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import mobaappworks.com.sgquick.Constant;

public class AccountsPagerAdapter extends FragmentPagerAdapter {

    Context mContext;
    private int id;
    private boolean isAccountList;

    public AccountsPagerAdapter(FragmentManager fm, Context context, int id, boolean isAccountList) {
        super(fm);
        this.mContext = context;
        this.id = id;
        this.isAccountList = isAccountList;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        Bundle args = new Bundle();
        args.putInt(Constant.ID, this.id);
        args.putBoolean(Constant.IS_ACCOUNT_LIST, isAccountList);

        switch (position){
            case 0:
                fragment = new AccountsFragmentInfo();
                fragment.setArguments(args);
                return fragment;
            case 1:
//                fragment = new AccountsFragmentContacts();
//                fragment.setArguments(args);
//                return fragment;
//            case 2:
//                fragment = new AccountsFragmentExistingProd();
//                fragment.setArguments(args);
//                return fragment;
//            case 3:
//                fragment = new AccountsFragmentCSUS();
//                fragment.setArguments(args);
//                return fragment;
//            case 4:
                fragment = new AccountsFragmentPipeline();
                fragment.setArguments(args);
                return fragment;
            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        switch (position){
            case 0:
                return "INFO";
            case 1:
//                return "CONTACT";
//            case 2:
//                return "EXISTING";
//            case 3:
//                return "CrossSell / UpSell";
//            case 4:
                return "PIPELINE";
            default:
                return "ERROR";
        }
    }
}