package mobaappworks.com.sgquick.accounts;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.mobaappworks.R;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmBaseAdapter;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import mobaappworks.com.sgquick.Util;
import mobaappworks.com.sgquick.entity.PipelineEntity;
import mobaappworks.com.sgquick.entity.ProductTypeEntity;

/**
 * Created by ldeguzman on 9/8/15.
 */
public class AccountsFragmentPipelineAdapter extends RealmBaseAdapter<PipelineEntity> {

    private Realm realm;


    public AccountsFragmentPipelineAdapter(Context context, RealmResults<PipelineEntity> accountEntities) {
        super(context, accountEntities, true);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        realm = Realm.getDefaultInstance();

        MyViewHolder viewHolder = new MyViewHolder();

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.activity_accounts_pipeline_list, parent, false);

        PipelineEntity pipelineEntity = realmResults.get(position);

        viewHolder.stage = (TextView) rowView.findViewById(R.id.secondLine);
        viewHolder.stage.setText(pipelineEntity.getStage());

        viewHolder.amount = (TextView) rowView.findViewById(R.id.thirdLine);
        viewHolder.amount.setText(String.valueOf(pipelineEntity.getRevenue()));

        viewHolder.product = (TextView) rowView.findViewById(R.id.firstLine);

        viewHolder.product.setText(Util.getProductName(pipelineEntity.getProductTypeId()));

        if(position%2==0) {
            rowView.setBackgroundColor(Color.parseColor("#e7e7e7"));
            viewHolder.stage.setTextColor(Color.parseColor("#333333"));
            viewHolder.product.setTextColor(Color.parseColor("#333333"));
            viewHolder.amount.setTextColor(Color.parseColor("#333333"));
        }

        rowView.setTag(pipelineEntity.getId());
        return rowView;
    }

    private class MyViewHolder {
        TextView stage;
        TextView amount;
        TextView product;
    }


}
