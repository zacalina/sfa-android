package mobaappworks.com.sgquick.accounts;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mobaappworks.R;

import io.realm.RealmBaseAdapter;
import io.realm.RealmResults;
import mobaappworks.com.sgquick.Constant;
import mobaappworks.com.sgquick.FontChangeCrawler;
import mobaappworks.com.sgquick.entity.CrossSellUpSellEntity;
import mobaappworks.com.sgquick.entity.PipelineEntity;

/**
 * Created by ldeguzman on 9/8/15.
 */
public class AccountsFragmentCsusAdapter extends RealmBaseAdapter<CrossSellUpSellEntity> {

    public AccountsFragmentCsusAdapter(Context context, RealmResults<CrossSellUpSellEntity> accountEntities) {
        super(context, accountEntities, true);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        MyViewHolder viewHolder = new MyViewHolder();

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.activity_accounts_csus_list, parent, false);

        CrossSellUpSellEntity crossSellUpSellEntity = realmResults.get(position);

        viewHolder.product = (TextView) rowView.findViewById(R.id.secondLine);
        viewHolder.product.setText(crossSellUpSellEntity.getProductTypeId().getProductTypeName() == null? "No Product Defined!" : crossSellUpSellEntity.getProductTypeId().getProductTypeName());

        viewHolder.recom = (TextView) rowView.findViewById(R.id.firstLine);
        viewHolder.recom.setText(crossSellUpSellEntity.getRecommendation());

//        if(position%2==0) {
//        rowView.setBackgroundColor(Color.parseColor("#e0e0e0"));
//            viewHolder.stage.setTextColor(Color.WHITE);
//            viewHolder.product.setTextColor(Color.WHITE);
//            viewHolder.amount.setTextColor(Color.WHITE);
//        }

        return rowView;
    }

    private class MyViewHolder {
        TextView product;
        TextView recom;
    }
}
